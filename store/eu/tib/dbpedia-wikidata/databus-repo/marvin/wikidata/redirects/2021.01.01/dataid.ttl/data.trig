<http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/dataid.ttl#Dataset> {
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software, commit: https://github.com/dbpedia/extraction-framework/commit/6ce5e19acd2b2051ee1283a1409bcffadde365f3 . After its releases, data is cleaned and persisted under the DBpedia account. ) Dataset containing redirects between articles in Wikidata."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Redirects"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/redirects> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/redirects/2021.01.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "The extractor responsible for this dataset is:\nhttps://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/RedirectExtractor.scala" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Redirects"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/dataid.ttl#redirects.ttl.bz2> , <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/dataid.ttl#redirects_transitive.ttl.bz2> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/dataid.ttl#redirects.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/redirects/2021.01.01/redirects.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2021-01-12T09:39:09Z\n<http://wikidata.dbpedia.org/resource/Template:Soft_redirect> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Softredirect> .\n<http://wikidata.dbpedia.org/resource/Template:Dot> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:·> .\n<http://wikidata.dbpedia.org/resource/Template:Nobots> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Bots> .\n<http://wikidata.dbpedia.org/resource/Template:Smile> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Smiley> .\n<http://wikidata.dbpedia.org/resource/Template:Clr> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Clear> .\n<http://wikidata.dbpedia.org/resource/Template:Template> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Tl> .\n<http://wikidata.dbpedia.org/resource/Template:Benvenuto> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Welcome/text/it> .\n<http://wikidata.dbpedia.org/resource/Template:-)> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Smiley> .\n<http://wikidata.dbpedia.org/resource/Template:U> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Reply> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "814d865c62e7b7b85cbbef2d074c06e1c143dd9d26b102e05713840c84588bd1" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "N9bzikC7YBOAFQJKEBmF4RX4SsbD6n+NmYzYbUVqVUBu8MDzawnauBlrHVFvpLbrV3fIJzCWQXrt1JTrqyyzEy3xa8ijQTNTXYK1a/C3ZJ09e7jox/Shn3mlOOIR42dbHH1GHpg3i/+0/Zm1g/TOpMguDqsW+oPEPmeNrZXcYI4BxeVxCJD/qizrc7erLy+374q7agln+o7DeZwOa7ACkrQceL8cIcyiuVbEyjq9iwGL7/A5cFNmbQUJvKSaOThIi0UQqDQEXwvnvlDpEo5ahIlbBWYfxHDhwPorQvsSsmLmL3s68vVVB39qrbabUGBjnbp16HcyPF24F/cDrZr8Rw==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-01-21T10:11:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "9964"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/redirects.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/dataid.ttl#redirects_transitive.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "transitive" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/redirects/2021.01.01/redirects_transitive.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2021-01-20T17:32:21Z\n<http://wikidata.dbpedia.org/resource/Template:Soft_redirect> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Softredirect> .\n<http://wikidata.dbpedia.org/resource/Template:Dot> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:·> .\n<http://wikidata.dbpedia.org/resource/Template:Nobots> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Bots> .\n<http://wikidata.dbpedia.org/resource/Template:Smile> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Smiley> .\n<http://wikidata.dbpedia.org/resource/Template:Clr> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Clear> .\n<http://wikidata.dbpedia.org/resource/Template:Template> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Tl> .\n<http://wikidata.dbpedia.org/resource/Template:Benvenuto> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Welcome/text/it> .\n<http://wikidata.dbpedia.org/resource/Template:-)> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Smiley> .\n<http://wikidata.dbpedia.org/resource/Template:U> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Template:Reply> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "ad996cb9e782f698105d415e6c504de73f36c075581723f445a74a903640bed6" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "UhetRV/0YC39ZYu1GxN6e5pwx7TSrgEFyLVNxJad+gtTON5msn0LpkJElSnkHoDLeuXwGWNgrPt1r/nlE/ZQQQc1Uq7GyUuKnRIFZ4z8qiGK7t4UoSFpkca+KQHZODHXibdeY7noP5kOmI4fw0VRr09igsz2IdDzaerFjnaXZot+Ar+6eGNYo3JQzJ6SII26btZLhhdzdTh/6gmW6hgRe9fpjAYlhBnNT/mjn+d7Xl6uFphUWHNDDCxWHF99SqO3bfU5inTWbnom2msHmXPYfT2Nq5JnK6PjM+6iblU1Q7EAzxkxSvRA/ksOdIsglZpEyNRsTe6Du+GlBZCkxxFiEg==" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "transitive" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-01-21T10:11:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "9968"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/redirects/2021.01.01/redirects_transitive.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/marvin/wikidata/redirects>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/marvin/wikidata/redirects/2021.01.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
