<http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/dataid.ttl#Dataset> {
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software, commit: https://github.com/dbpedia/extraction-framework/commit/f7babab6b592d0a1a4af8b618e668222959a8cf9 . After its releases, data is cleaned and persisted under the DBpedia account. ) Contains triples of the form $object rdf:type $class"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata Instance Types"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/instance-types> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/instance-types/2020.03.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Using P31 as rdf:type . \nIf the typed class is in in the DBpedia Ontology, it will be used, otherwise the type is discarded. \nE.g. \n\n```\n<QXYZ> <P31> <Q5> . # Q5 is Person\n<Q5> owl:equivalentClass dbo:Person .\n------------------------\n<QXYZ> rdf:type dbo:Person .\n```\nFunction used:\n```\n \"P31\": [\n        {\n            \"rdf:type\": \"$getDBpediaClass\"\n        }\n],\n\n```\n\nThe extractor uses the data from ontology-subclass-of artifact to optimize the hierarchy and enrich equivalent classes.\n\nThe mappings between Wikidata Items and classes can be edited in the [Mappings Wiki](http://mappings.dbpedia.org/index.php/OntologyClass:Person)\n\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L92\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L87" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata Instance Types"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/dataid.ttl#instance-types_transitive.ttl.bz2> , <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/dataid.ttl#instance-types_specific.ttl.bz2> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/dataid.ttl#instance-types_transitive.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "transitive" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/instance-types/2020.03.01/instance-types_transitive.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2020-04-01T07:29:41Z\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Country> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.wikidata.org/entity/Q6256> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/PopulatedPlace> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Place> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Place> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Location> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Country> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.wikidata.org/entity/Q6256> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "1fde913465f1477f6a5225657c90d34729d4d158905f64f98d50f83cbd123098" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "awziiDonCa6Qhn1y98reWcQCinnUnhZu14H9uBctLMTigPmUr+Kt1bM4sV0Np4p7gQn/F7OWGIwvL1Wqaw3N8mr1rcfdbMWC5rVw9k3dfSmGCzqzEC3H7MPx4g3trULUZEeO1gDdW0Ot7RdUmorV04Se1Nv6LoYONFPSmv3rXHBC1qxfiqTrYK+VXqLkLL+0XABKZD28V13MZiCLzXctTbo0PIeArxSedFZZLtfrMxO+zTbtn5hk8sqtxTpcuYG+obMJv5C4Ot+GIuJhDG4pbbAO7tW9Lm0fE4Ima8CMuNAo1lslREWS/qtNZWSogGd36IVNZHt402aoqeF7DO7vxA==" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "transitive" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-16T16:52:10Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "278420024"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/instance-types_transitive.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/dataid.ttl#instance-types_specific.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "specific" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/instance-types/2020.03.01/instance-types_specific.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2020-04-01T07:29:41Z\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q20> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q30> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q42> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q1868> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q80> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q2001> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q35> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/State> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "89c464008082be6f7d86eb3419f0e9a7a2ad5d5350f3fe694d72c4287419fe35" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "LcKbMXA6VO1zoicZGSTKyMd57w8YcBHPEIaO5sY8MGD42t6qD4+Q3MklsHuNZ34UUV0o7RWyZr+JnRfYL2/BDQ07O43hLb6pVOF5TtgUXMIAVUy/XUKK0xMbpZlwvpxhJNung7BjodpTbnkFKqk2LlrcBIM3PQLLtqE7PzJSj3hW/+J43avaHNYN4NjM7l3iE2XzIigeJCTB+FORDHyUlCUAlsVy5SOIrQ6mNE/PUDUbB9ibs/6THgQKTpd0IbDyv1VgbdimLj2kfOUQOYAn4cU4wiU2lrT+STy2EJbZfK6ROEnDI89dw+d2dg45MGQm2MzTb0ym8Ttbdn6wWYpQXg==" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "specific" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-16T16:52:10Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "48703194"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/instance-types/2020.03.01/instance-types_specific.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/marvin/wikidata/instance-types/2020.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <https://databus.dbpedia.org/marvin/wikidata/instance-types>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
}
