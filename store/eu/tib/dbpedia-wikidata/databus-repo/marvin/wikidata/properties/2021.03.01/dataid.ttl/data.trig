<http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2021.03.01/dataid.ttl#Dataset> {
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2021.03.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software, commit: https://github.com/dbpedia/extraction-framework/commit/6ce5e19acd2b2051ee1283a1409bcffadde365f3 . After its releases, data is cleaned and persisted under the DBpedia account. ) Wikidata property URIs as subject and dbo:alias to their labels"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata properties with labels"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/properties> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/properties/2021.03.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "A dump of all Wikidata properties. Some of them were used to generate mappings in R2R, e.g. \n```\n<http://www.wikidata.org/entity/P957> <http://www.wikidata.org/entity/P1630> <https://www.wikidata.org/wiki/Special:BookSources/$1> .\n```\n\n## Issue\n* The R2R mapping derived from this dataset, would need to be updated, but the update is manual atm, automation possible" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata properties with labels"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2021.03.01/dataid.ttl#properties.ttl.bz2> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2021.03.01/dataid.ttl#properties.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/properties/2021.03.01/properties.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2021.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/alias> \"détient le record\"@fr .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/alias> \"record deținut\"@ro .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/alias> \"紀錄\"@zh .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/description> \"Rekordhalter in\"@de .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/description> \"bir kişi veya kuruluş tarafından elde edilen kayda değer kayıt, tutulan tarihler için niteleyiciler (P580 gibi) ile desteklenir\"@tr .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/description> \"csúcstartó ebben\"@hu .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/description> \"henkilön tai kohteen saavuttama ennätys\"@fi .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/description> \"lu suggettu stabbilìu stu record\"@scn .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/description> \"notable record achieved by a person or entity, include qualifiers for dates held\"@en .\n<http://www.wikidata.org/entity/P1000> <http://dbpedia.org/ontology/description> \"record détenu par une personne ou une entité\"@fr ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "125e7569f41a8737760725e32811867d6610974b1c9db0b9ddbd55e7cba6874e" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "SGaHBJ6xo67Pde3/Ev0+HKju+vnLcIysE87ncBn/6NNNXlEJ2UQqLi1koz7uPHQLBgYw/bUpTpAIdp6UDz9wU+/FNmhLfTwRPQQwL34nfMqcDi1oyIfGBd6fpsy3VFnVOnS0dSimOPXsxQQqvTIjJ5jGql7uCtbBli3tVfOBvklDggxRFlpH0iEonki6mXcNjHcYXdkojOb1HkqWM4VyQjU7j5vpytV5Kv/orgm3XwxmesVy2N9edn5NDlYLz9iX5edu0v2IE97FNS7PRbJldJq6So8xr5G2VWiRQsvjWSijGGkooN8NFDMfIGcZHeO9vIRj9mBzJGrg5jW6R0C6rg==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-03-22T12:56:49Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "5272660"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2021.03.01/properties.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/marvin/wikidata/properties>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/marvin/wikidata/properties/2021.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
