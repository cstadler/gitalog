<http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2020.05.01/dataid.ttl#Dataset> {
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2020.05.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software, commit: https://github.com/dbpedia/extraction-framework/commit/f7babab6b592d0a1a4af8b618e668222959a8cf9 . After its releases, data is cleaned and persisted under the DBpedia account. ) Wikidata property URIs as subject and dbo:alias to their labels"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata properties with labels"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/properties> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/properties/2020.05.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "A dump of all Wikidata properties. Some of them were used to generate mappings in R2R, e.g. \n```\n<http://www.wikidata.org/entity/P957> <http://www.wikidata.org/entity/P1630> <https://www.wikidata.org/wiki/Special:BookSources/$1> .\n```\n\n## Issue\n* The R2R mapping derived from this dataset, would need to be updated, but the update is manual atm, automation possible" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.05.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-05-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata properties with labels"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2020.05.01/dataid.ttl#properties.ttl.bz2> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <https://databus.dbpedia.org/marvin/wikidata/properties/2020.05.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/marvin/wikidata/properties>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2020.05.01/dataid.ttl#properties.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/properties/2020.05.01/properties.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2020.05.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2020-05-16T14:51:48Z\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"Ministerpräsident , Bürgermeister , Oberbürgermeister , Ratsvorsitzender , Ortsbürgermeister , Regierungschef , Premierminister , Landrat , Regierungspräsident\"@de .\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"primer ministru , presidente , gobernador , alcalde\"@ast .\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"prefeito , primeiro ministro , premiê , governador , presidente , primeiro-ministro , chefe de estado\"@pt .\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"քաղաքապետ , Երկրին նախագահ , Վարչապետ , Մարզպետ , շրջանային կառավարիչ , նահանգապետ , հրամանատար\"@hyw .\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"ministras pirmininkas , premjeras , pirmasis ministras , meras\"@lt .\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"gradonačelnik\"@hr .\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"mērs , valdības galva\"@lv .\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"kormányfő , polgármester , főpolgármester , miniszterelnök\"@hu .\n<http://www.wikidata.org/entity/P6> <http://dbpedia.org/ontology/alias> \"ߞߏߕߌ߯ ߝߟߐ , ߞߎ߲߬ߠߊ߬ߛߌ߮ ߞߎ߲߬ߕߌ߮\"@nqo ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "9234a3571c00138cabc418db6e8566847729202789756670ec11f5842695c769" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "WBiLVcZEUM26ipVGQr2VMOIimvAmK225SRcCEEmjEK0EwFGiNwqS0ea0QX5/fMI0+GIXHgxBgN9DbJl+B3kP79KrU//ydl+HfJCRAWq5aEWxWKA0l081z5fdKrV0oHGeTn+S+Hr4mHhPQnFPYITlSXCnxoN0DqbGlqDvFblq2V2jU7rNIfjnZiJ8haQhKDyEMQs9Us+L8RSTPPjE2tUeYLTFwL9bNrvipEe9xMmSR0bd1JUW4ASYtdT2vK8e/yI/Bg8s+wZaQjzq12Gyc1n04GC7dlgOsfulTofvJ7pPoPWyi2VBh5qVLrFFEm4CREf44BIRY+4A+MHXPurNDDZ4sQ==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.05.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-05-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-05-24T17:28:34Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "4288663"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/properties/2020.05.01/properties.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
