<http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2021.01.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals/2021.01.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2021.01.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software, commit: https://github.com/dbpedia/extraction-framework/commit/6ce5e19acd2b2051ee1283a1409bcffadde365f3 . After its releases, data is cleaned and persisted under the DBpedia account. ) High-quality data extracted using the mapping-based extraction (Literal properties only)."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata Mappingbased Literals"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals/2021.01.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "The dump contains only dbo properties.\nIf there is an owl:equivalentProperty from a Wikidata property to a DBO property, the property is replaced\n\nErrors from here are recorded in the debug artifact (object vs dataype)\n\nImprovement can be most effectively done by adding more equivalent class mappings to http://mapping.dbpedia.org\n\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/wikidata/WikidataR2RExtractor.scala\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n\n\n## Issue\n* unclear, whether there are literals in wikidata that need cleaning" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata Mappingbased Literals"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2021.01.01/dataid.ttl#mappingbased-literals.ttl.bz2> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2021.01.01/dataid.ttl#mappingbased-literals.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals/2021.01.01/mappingbased-literals.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2021.01.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2021-01-12T09:39:09Z\n<http://wikidata.dbpedia.org/resource/Q22> <http://dbpedia.org/ontology/populationTotal> \"5404700\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q22> <http://dbpedia.org/ontology/areaTotal> \"78782\"^^<http://dbpedia.org/datatype/Area> .\n<http://wikidata.dbpedia.org/resource/Q21> <http://dbpedia.org/ontology/personName> \"England\" .\n<http://wikidata.dbpedia.org/resource/Q21> <http://dbpedia.org/ontology/populationTotal> \"53012456\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q21> <http://dbpedia.org/ontology/areaTotal> \"130278.43\"^^<http://dbpedia.org/datatype/Area> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://dbpedia.org/ontology/personName> \"Wales\" .\n<http://wikidata.dbpedia.org/resource/Q25> <http://dbpedia.org/ontology/personName> \"Cymru\" .\n<http://wikidata.dbpedia.org/resource/Q25> <http://dbpedia.org/ontology/formationDate> \"500-0-0\"^^<http://www.w3.org/2001/XMLSchema#date> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://dbpedia.org/ontology/startDate> \"500-0-0\"^^<http://www.w3.org/2001/XMLSchema#date> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "5efc92d1eacacf85e9f6c87a3aa82d2cf4367fabc8db38eb6fc98eb74af9df03" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "p3sNaiupGUQgqLolyCTJOgUu3aiLYtNjOnec63Pg7Yk3XFb8O/UCZ8gXN/5nG5O4uz0BXhRYT37NAuj70gb+RCIJexxzkqp7J+/VNJNbXHxkPkxD4+dIEL9fQcnD4koExbEnlJKe9ae7On9er8x5+QcdXvvoWkTe4+lpEG6us3hTdvDkIzzGu2YVqjHHW93VFXaWwonLEk7ve3CzSsf8c0p+wbpTQ2BiPXSHhZS6q4GWH7hd/fqTZPDdM7P3BlY6pBhYYSCjlen4DcHUrcc/Ns6wWSmFi52foIxX1h4lZeSgokuNCOLeqftXEtOXpqjDnp7AQZdkEmkaf2TpmliaIQ==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-01-21T10:10:39Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "290267877"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2021.01.01/mappingbased-literals.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
