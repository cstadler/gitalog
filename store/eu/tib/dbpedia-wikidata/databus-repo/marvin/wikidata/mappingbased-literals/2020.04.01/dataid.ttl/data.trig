<http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2020.04.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2020.04.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software, commit: https://github.com/dbpedia/extraction-framework/commit/f7babab6b592d0a1a4af8b618e668222959a8cf9 . After its releases, data is cleaned and persisted under the DBpedia account. ) High-quality data extracted using the mapping-based extraction (Literal properties only)."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata Mappingbased Literals"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals/2020.04.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "The dump contains only dbo properties.\nIf there is an owl:equivalentProperty from a Wikidata property to a DBO property, the property is replaced\n\nErrors from here are recorded in the debug artifact (object vs dataype)\n\nImprovement can be most effectively done by adding more equivalent class mappings to http://mapping.dbpedia.org\n\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/wikidata/WikidataR2RExtractor.scala\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n\n\n## Issue\n* unclear, whether there are literals in wikidata that need cleaning" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata Mappingbased Literals"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2020.04.01/dataid.ttl#mappingbased-literals.ttl.bz2> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2020.04.01/dataid.ttl#mappingbased-literals.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals/2020.04.01/mappingbased-literals.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2020.04.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q1000003> <http://dbpedia.org/ontology/populationTotal> \"847\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q1000003> <http://dbpedia.org/ontology/zipCode> \"62380\" .\n<http://wikidata.dbpedia.org/resource/Q1000004> <http://dbpedia.org/ontology/length> \"154.771\"^^<http://dbpedia.org/datatype/Length> .\n<http://wikidata.dbpedia.org/resource/Q1000008> <http://dbpedia.org/ontology/populationTotal> \"574\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q1000008> <http://dbpedia.org/ontology/zipCode> \"62580\" .\n<http://wikidata.dbpedia.org/resource/Q1000009> <http://dbpedia.org/ontology/populationTotal> \"1526\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q1000009> <http://dbpedia.org/ontology/zipCode> \"62580\" .\n<http://wikidata.dbpedia.org/resource/Q100000> <http://dbpedia.org/ontology/populationTotal> \"3667\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q100000> <http://dbpedia.org/ontology/zipCode> \"6267\" .\n<http://wikidata.dbpedia.org/resource/Q1000011> <http://dbpedia.org/ontology/populationTotal> \"650\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "5ce61a7a1d6ced116c8310092d908d62e1de3283a2993e61511154b9b80d974e" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "H9PSuig/0e776yXH8S5zJ2zSfOJ3BzTXO52Et6EBFlCp7+TRJgKiupvjlam+oZOkgp2MnEZxEeyQ+cemiEE3hn9heSRNcKrR+ge1KHB0i7xfmee0iw/S4cPWzAzIpkHGEOWVkWNp5mDMZxhLAH+2Lzikpo907nciQVgQoiJ0EAwiGYIWbNtfLdPgIDsK4y5GNqSLrb/3sAp583jNzJB4xLcyzeCYJ4jn6KFE+G83LA4njBOS7HHnJPGkaPWpA6LMsLp7ppZZuh7cWTorEmQpbXV7GD39OvvUO9TEQZMwex/5e6lfb4ymFC2PtLjLnvnFhlw5E6Gv//nTbicHdDPOEA==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-07-08T10:10:33Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "75123186"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/mappingbased-literals/2020.04.01/mappingbased-literals.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/marvin/wikidata/mappingbased-literals/2020.04.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
