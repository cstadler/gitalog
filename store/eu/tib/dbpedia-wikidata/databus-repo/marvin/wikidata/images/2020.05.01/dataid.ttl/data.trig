<http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/images/2020.05.01/dataid.ttl#Dataset> {
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/images/2020.05.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software, commit: https://github.com/dbpedia/extraction-framework/commit/f7babab6b592d0a1a4af8b618e668222959a8cf9 . After its releases, data is cleaned and persisted under the DBpedia account. ) Images from Wikidata item"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata Images"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/images> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/images/2020.05.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Some properties from Wikidata, notably P18, are transformed to foaf:depiction and dbo:thumbnail. \nThe transformation is described here:  \nhttps://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L111" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.05.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-05-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata Images"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/images/2020.05.01/dataid.ttl#images.ttl.bz2> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <https://databus.dbpedia.org/marvin/wikidata/images/2020.05.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/images/2020.05.01/dataid.ttl#images.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/images/2020.05.01/images.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/images/2020.05.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2020-05-16T14:51:48Z\n<http://wikidata.dbpedia.org/resource/Q15> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/Africa_satellite_orthographic.jpg?width=300> .\n<http://wikidata.dbpedia.org/resource/Q15> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/Africa_satellite_orthographic.jpg> .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/South_America_satellite_orthographic.jpg?width=300> .\n<http://wikidata.dbpedia.org/resource/Q18> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/South_America_satellite_orthographic.jpg> .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/South_america_tr.jpg?width=300> .\n<http://wikidata.dbpedia.org/resource/Q18> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/South_america_tr.jpg> .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/Mapa_político_América_do_Sul_(english).svg?width=300> .\n<http://wikidata.dbpedia.org/resource/Q18> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/Mapa_político_América_do_Sul_(english).svg> .\n<http://wikidata.dbpedia.org/resource/Q26> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/Northern_Ireland_by_Sentinel-2.jpg?width=300> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "a05bc4240d2b91108de7996e413db36b713b7ea46a206f6d7de3615278e06b9b" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "KJiCJd5jXj68o2WTq/hl+PmGbUvYiWNqZa9Pmzkv07g5bAc7Nfs2pf82NlARf6riu2ff+6k1v34jR3IWXBbGdVdhnMeEISKJLlrzPjSokQus2LOx1DIX0DcN4rUi8lFm+VntXRpZHWfk27fEl0qrbC3JMuoDTSBUdtk2VfK27uJ4RX8xv29uE6xCAiM13MyTBTuvslhz+ZTfIpf8recIqP+kyi98n0+r40aL+UbJgtBcuNFH4Rj55Ipm0zIdddrkuMOSI3JEBC1Yg+XpIL6sxP93TcMwx+aaKvbS4FPxudiuPXhYcmXw8Ix/g4WEkixFaSEIr0YuM+TTyMIA/NbwCw==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.05.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-05-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-05-24T17:28:24Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "81666727"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/images/2020.05.01/images.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/marvin/wikidata/images>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
}
