<http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/marvin/wikidata/alias>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software, commit: https://github.com/dbpedia/extraction-framework/commit/6ce5e19acd2b2051ee1283a1409bcffadde365f3 . After its releases, data is cleaned and persisted under the DBpedia account. ) Wikidata-specific dataset containing aliases for languages in the mappings wiki. Aliases for languages not in the mapping wiki are found in the _nmw variant."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata Alias"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/alias> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/alias/2021.03.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Aliases are alternative names for items that are placed in the Also known as column of the table on top of every Wikidata item page.\nFrom https://www.wikidata.org/wiki/Help:Aliases\nThere can be several aliases for each item, but only one label (other dataset).\n\n## Issues\nCurrently data is grouped in two categories:\n* aliases from mapping wiki languages (around 40)\n* aliases from all other languages\nIn the future, we might separate these into one file per language, which will increase the number of files from 2 (nmw as content variant) now, to many (basically using iso codes as content variants, beyond the 120 wikipedia versions)\nThis is under discussion however." ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata Alias"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/dataid.ttl#alias.ttl.bz2> , <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/dataid.ttl#alias_nmw.ttl.bz2> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/dataid.ttl#alias.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/alias/2021.03.01/alias.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"Franklin School , Franklin ECS\"@en .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"Franklin School\"@ca .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"Franklin School\"@es .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"ফ্রাঙ্কলিন স্কুল , ফ্রাঙ্কলিন বিদ্যালয়\"@bn .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"フランクリン・アーリー・チャイルドフッド・スクール\"@ja .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://dbpedia.org/ontology/alias> \"Kategori:Panagyurishte\"@sv .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://dbpedia.org/ontology/alias> \"Tatyana Serafimovna Kolotilshchikova\"@en .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://dbpedia.org/ontology/alias> \"Колотильщикова Татьяна Серафимовна\"@ru .\n<http://wikidata.dbpedia.org/resource/Q100000026> <http://dbpedia.org/ontology/alias> \"Witley Park underwater conservatory\"@en .\n<http://wikidata.dbpedia.org/resource/Q100000040> <http://dbpedia.org/ontology/alias> \"Pikmin 3 Déluxe\"@fr ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "c3ba3563f401c4dd09c4cba7fc3803b50287c0dce4a66e4db05e8b399be00156" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "X+JrQrAcUvKcAzWcet+tBO91QPEezluBgsbrz5AiiXcldz82w3Kw4/6bRi+vmJ4yiRAdOzLIiciIvcJLqs2do1BTfH6ywFiboxJB0mVpcYZs5r4lYJXq+e1mFM/8FqxyvYRykiU4Qv2Au9WouIwYewsQ8QoxFnJUzBKdjipRGblvsoxxjQvrSv2cygUsCZ4Sn3e9JNwK/vUSXDWVJ1AEqIuL4BvdwheOymlhOPEqhECR3BRDZ1kAX0k5wfnLbbCB5GYN+JrG1uzQV+W+SL+5CnWZXdA5yvhs0caXryoke0eSRF1OAmOoW//RoSePfwMGaJaOFpJs6VjD8UjGpaKpvA==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-03-22T12:56:25Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "185196585"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/alias.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/dataid.ttl#alias_nmw.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "nmw" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/alias/2021.03.01/alias_nmw.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"Franklin School\"@tl .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"ഫ്രാങ്ക്ലിൻ സ്കൂൾ\"@ml .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://dbpedia.org/ontology/alias> \"Panagjurishte\"@fi .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://dbpedia.org/ontology/alias> \"டாடியானா\"@ta .\n<http://wikidata.dbpedia.org/resource/Q1000001> <http://dbpedia.org/ontology/alias> \"黃金眼鏡蛇 (專輯)\"@zh .\n<http://wikidata.dbpedia.org/resource/Q100000694> <http://dbpedia.org/ontology/alias> \"വെമ്പായം ഗ്രാമപഞ്ചായത്ത്\"@ml .\n<http://wikidata.dbpedia.org/resource/Q100000> <http://dbpedia.org/ontology/alias> \"கீர்\"@ta .\n<http://wikidata.dbpedia.org/resource/Q10000164> <http://dbpedia.org/ontology/alias> \"Cirelli\"@ab .\n<http://wikidata.dbpedia.org/resource/Q10000164> <http://dbpedia.org/ontology/alias> \"Cirelli\"@ady .\n<http://wikidata.dbpedia.org/resource/Q10000164> <http://dbpedia.org/ontology/alias> \"Cirelli\"@am ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "fb0c3efa755001f270f1be34deb5a3144b845fb4a1dd6765de091ca988a79f4c" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "W1IywyXA9tnYSN1SdsQ29WqvT9ZMGNU/S7lXktDukLcLcJwu0Xx2rpVoMHl6diL5PJ/gS+24sPp1BUVAcJZhHFutjluer3kbM8OMfc2BI2hmu4PvCmCKulXhhR6NsorKr3H0VWGyd2r1pEm/EdwHG8DKaM+PvD3iZYbQ5tVg+skdMU/S7SfVrZhW6cH29y2+y23yP+Z/YBijfuMsjcX2LvzpaUAaSiolBq6mRSmbkNwKHsXY/pEmWx8LO+rChGXeiBU38YzMz1XUY4IMrSX6OZeRL6Ib4fOpKvLo5wflxIKNMO4iXbyRMpp2TBrKbQa0KEZt9iT2p3fKSgjwoQr1vQ==" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "nmw" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-03-22T12:56:25Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "103652771"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/databus-repo/marvin/wikidata/alias/2021.03.01/alias_nmw.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/marvin/wikidata/alias/2021.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
