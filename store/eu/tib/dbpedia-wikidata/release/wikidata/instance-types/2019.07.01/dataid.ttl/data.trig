<http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/dataid.ttl#Dataset> {
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <https://databus.dbpedia.org/marvin/wikidata/instance-types>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/marvin/wikidata/instance-types/2019.07.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Contains triples of the form $object rdf:type $class"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Instance Types"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/instance-types> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/instance-types/2019.07.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Using P31 as rdf:type . \nIf the typed class is in in the DBpedia Ontology, it will be used, otherwise the type is discarded. \nE.g. \n\n```\n<QXYZ> <P31> <Q5> . # Q5 is Person\n<Q5> owl:equivalentClass dbo:Person .\n------------------------\n<QXYZ> rdf:type dbo:Person .\n```\nFunction used:\n```\n \"P31\": [\n        {\n            \"rdf:type\": \"$getDBpediaClass\"\n        }\n],\n\n```\n\nThe extractor uses the data from ontology-subclass-of artifact to optimize the hierarchy and enrich equivalent classes.\n\nThe mappings between Wikidata Items and classes can be edited in the [Mappings Wiki](http://mappings.dbpedia.org/index.php/OntologyClass:Person)\n\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L92\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L87\n\n# Group Documentation\n## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.07.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-07-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata Instance Types"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/dataid.ttl#instance-types.ttl.bz2> , <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/dataid.ttl#instance-types_transitive.ttl.bz2> .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/dataid.ttl#instance-types.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Contains triples of the form $object rdf:type $class"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Instance Types"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "16380306"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2019-07-18T07:00:04Z\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q20> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q30> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q23> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q42> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q1971> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Single> .\n<http://wikidata.dbpedia.org/resource/Q1868> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q80> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "7c950922a264271a926f61d9c5e8381e5ecb6d5ffa97474650064a9d2895c9f1" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "nKC9KSTobX7eTvRcp9oaXVdkfgrsTda8Rp/HPvlkO73rXl2Ib8rQCvUWa+N3JzgZB+I49PfU4fhSqPBJZFJpqGAowYgqdFM82mjnmPTweSt6C8HldlLxCWA4miGax/oLq1yu47+Xp6SfwilDTzUguqLVV4ij0swn6hNt6rC2c48LyXHefl53ClblI7uNXiIBChXYqDYtros8nHsSYxzAAleW7mUSCmiP+k8Kc3KndL0MaAUnTv4f3XR9ed8ldX9Gl6pEZ2ydNKvO3PZFP3R9ZJgMVPpSTUaCDZQ1Ztp9tnzYaH764h6Q0Esn5eR6uolSFu+uDPCRnLw2JbMohxqBKg==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "-2025992931"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.07.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-07-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-07-28T16:33:06Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata Instance Types"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "39617549"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/instance-types.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/dataid.ttl#instance-types_transitive.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Contains triples of the form $object rdf:type $class"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Instance Types"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "transitive" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "109791048"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2019-07-18T07:00:04Z\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Country> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.wikidata.org/entity/Q6256> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/PopulatedPlace> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Place> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Place> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Location> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Country> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.wikidata.org/entity/Q6256> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "1424b9030b032787900d50362304e75fe7b9e85daa5b12bd26728d804c70ffd4" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "Z+knVCE3++O7tQhos9a2uyFOX3Idc3lyPCSBlyKtKtQiqpiHgj7WrdXlY/iwRzgdmi+rZJzDVNvJUhH2iSrkcDozA1op2Tlh8HNGBMMWMIa8rNyUtGeCfAiMJIxNf6Cr3FzKwN2xJ9x0DD0/g6iMctuo6mf85svsQNYB8QQmR2v692gHJVOo/Wo8IKho9zn3Ersg8jir2yyoZqNobY/tsSADo+KgXeLAGCOcpPknCC5K4shkX5cLLJ9MY9X0ixRKIBIwIDEto+Zao8OGgkel0+HE/m5fjXdly62/N+GYTDW95HSwreYsvYIa5YMwtoMphsAuEKnsiDTasug/dfYBlA==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "-1577335404"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "transitive" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.07.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-07-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-07-28T16:33:06Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata Instance Types"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "228505610"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/instance-types/2019.07.01/instance-types_transitive.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
