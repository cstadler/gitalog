<http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/marvin/wikidata/revision/2019.12.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software. After its releases, data is cleaned and persisted under the dbpedia account. Commit: ) The revision information of the Wikidata article the information was extracted from."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata Revision"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/revision> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/revision/2019.12.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.12.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-12-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata Revision"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/dataid.ttl#revision_ids.ttl.bz2> , <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/dataid.ttl#revision_uris.ttl.bz2> .
    
    <https://databus.dbpedia.org/marvin/wikidata/revision>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/dataid.ttl#revision_ids.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "ids" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/revision/2019.12.01/revision_ids.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "68977253"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2019-12-13T23:27:03Z\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1059191203\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Q15> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1066641031\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1065390508\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1052621180\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Q20> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1062738717\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Q26> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1055376416\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Q22> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1038792377\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Q21> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1062914530\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Q27> <http://dbpedia.org/ontology/wikiPageRevisionID> \"1060240450\"^^<http://www.w3.org/2001/XMLSchema#integer> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "a2a962b7c708476b118d7fe06324ac3b3eb36647ad75bafc8f05d45158270889" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "MT6Of3QyKrguxYhVsde+e7/1myT5obJATq4Ra/Lr6SOiTPu2Nic17yX0JciFf8qSRO9q7+G20hfmR74FzjC67QHfhdHNOKRk/d/LQeU/gCBF+R3rvRUy4+hfTIVpKvduXAO5f28U19bmH6bJABlkLfqUHdFaFoctL1xEj9JZ4gJC1K7sKeGUAlgcMcp+Z3DEWNCBj+Lq+8Inv5scUz3/Agt3KzzdqkXiw78n99X7J3ya28NsqzwkTaJ9Arhn0P1gtAz/CluOXhNSwc0CFDo7YViCkzomheuvNZ74xQhFDOq7EMxJZKMAAlnIQVJnComB//u1WRPKE3RNEs0udZFlkw==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "10779932366"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "ids" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.12.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-12-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-01-03T12:09:47Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "400013724"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/revision_ids.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/dataid.ttl#revision_uris.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "uris" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/revision/2019.12.01/revision_uris.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "68977253"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2019-12-13T23:27:03Z\n<http://wikidata.dbpedia.org/resource/Q18> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q18?oldid=1059191203&ns=0> .\n<http://wikidata.dbpedia.org/resource/Q15> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q15?oldid=1066641031&ns=0> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q17?oldid=1065390508&ns=0> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q25?oldid=1052621180&ns=0> .\n<http://wikidata.dbpedia.org/resource/Q20> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q20?oldid=1062738717&ns=0> .\n<http://wikidata.dbpedia.org/resource/Q26> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q26?oldid=1055376416&ns=0> .\n<http://wikidata.dbpedia.org/resource/Q22> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q22?oldid=1038792377&ns=0> .\n<http://wikidata.dbpedia.org/resource/Q21> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q21?oldid=1062914530&ns=0> .\n<http://wikidata.dbpedia.org/resource/Q27> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://www.wikidata.org/wiki/Q27?oldid=1060240450&ns=0> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "a4697a1e999dedfaa26e382b0681e9c55645f5dc96ed78a3cfa01d7893ea7492" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "H+7DxtKJnLPOz/YXda9gt076vmhGqB4lQzxniH8MZALSlYbLB5EpSVguywuVTRy+IgRwP98JY5jdMR9NfGCVUrrBT66mCaIbZkjxKDaDH13wqYEYNQX7N79hKtN85tKzuz5yc0o9hYPfkfkfGHktHOJYH3lyNQW9JdKlIY4/7CigbSg6md2WtjFwNCoN4QH/T5ctVITiTRD4TenNG5GalIu7P4Eu/YoQaSL4HdFyiEDCSPDMqpPs/vTsM4ebb+uzmd1y1trhrKZIhsbitjJ9Legnj9rvL7XoEr8ORWYWfjcYgp2Vp2P7OtAoOD01Y5BguGgr+m/55eFBJWUizHuBtA==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "10770443895"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "uris" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.12.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-12-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-01-03T12:09:48Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "522397329"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/revision/2019.12.01/revision_uris.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
