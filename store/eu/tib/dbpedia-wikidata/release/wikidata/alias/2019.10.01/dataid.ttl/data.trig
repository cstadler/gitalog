<http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/marvin/wikidata/alias>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases, i.e. unparsed, unsorted for debugging the software. After its releases, data is cleaned and persisted under the dbpedia account.)Wikidata-specific dataset containing aliases for languages in the mappings wiki. Aliases for languages not in the mapping wiki are found in the _nmw variant."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Wikidata Alias"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/alias> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/alias/2019.10.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Aliases are alternative names for items that are placed in the Also known as column of the table on top of every Wikidata item page.\nFrom https://www.wikidata.org/wiki/Help:Aliases\nThere can be several aliases for each item, but only one label (other dataset).\n\n## Issues\nCurrently data is grouped in two categories:\n* aliases from mapping wiki languages (around 40)\n* aliases from all other languages\nIn the future, we might separate these into one file per language, which will increase the number of files from 2 (nmw as content variant) now, to many (basically using iso codes as content variants, beyond the 120 wikipedia versions)\nThis is under discussion however." ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.10.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-10-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Wikidata Alias"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/dataid.ttl#alias.ttl.bz2> , <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/dataid.ttl#alias_nmw.ttl.bz2> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/dataid.ttl#alias.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/alias/2019.10.01/alias.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "14330369"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2019-10-08T21:22:40Z\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Süd-Amerika\"@de .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"južnoamerički kontinent\"@hr .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Sud Amèrica , Amèrica del sud , Sudamèrica , Sud-Amèrica , Sud-amèrica\"@ca .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Јужноамерички континент\"@sr .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"América del Sur , América do Sul\"@sv .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"남미\"@ko .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Sudamérica\"@gl .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"América del Sur , O seu cu América do Sul\"@en .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Sud-Ameriko\"@eo ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "94c5dee7c9ae3528e3f70b2867abb2155a3af46ee18379f5976153dd6d1e12ad" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "aLw7rg5b0MwmWVcrE9DiJVAbTxOUuDI9cxQiWJLF7q4bY6U+5lvUUauRQsv2b4CZdY2vAUcU8N8IiXYC6/k5DbrimzQB5qU7CTTuzkqkoIH2qTsB9/Tv1OvnJrT2GFwDemEN/OFLhl9BQm175XDDvFwcPI0MvrWx3xo5/Z2NeQ9zzhAw77KFtfbk2x4QGv3vYgixQijBjHeE3KDfhFETM66usKyRndZ8fI9FUlSYY9UqNB1BRf1dyvN5fmuY6cuFqSVWDDkwHgWQ7rf7Z/ZMiJSH5ye73dx6xonJQbbljaZOzJLW9MP4Pqo6aUM8IK1sAhl+xXnTnNyT+mpJKNVZNg==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "1750543603"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.10.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-10-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-10-14T11:20:37Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "147997461"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/alias.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/dataid.ttl#alias_nmw.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "nmw" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/alias/2019.10.01/alias_nmw.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "26572940"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2019-10-08T21:22:40Z\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Syd-Amerika\"@nn .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"América del Sur , América do Sul\"@fi .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"南美 , 南阿美利加洲\"@yue .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"جنوبی آمریکا\"@azb .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"America do Sud\"@lij .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Siadamerika\"@bar .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Mauling America\"@pam .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"ಸೌಥ್ ಅಮೇರಿಕ\"@kn .\n<http://wikidata.dbpedia.org/resource/Q18> <http://dbpedia.org/ontology/alias> \"Amerika Kusini\"@sw ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "9a59a134bc07e6a55c2833fa9882fcb2fbb1570cf1ecf5b87241d742e6f48a9a" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "FK1ljycqdsd863DvBfwxpuPBNwlz9Go6+h3bHy5LzZEHdubX67CCa5yCbNjh6roiMspCYUdDt84slqKjcg4RYnsnDyzpCVGEiHyS6XSOtFlSxwjyvIKRwzkD41WaiV7H6T8AlXC0ImRc9np3Q4BeHQvL7iXdMRgGvxBDUaY8cvUYhjT+GC9M/erAVhzihkN9uhIRwq0nPC8riZ/4XCWckMqDpVRL0zpqF0ANjjV0NpBGMSRrLakS94aZnNauHWiligFp/PEK/QAbpdGnOmxOexHKVcf1eHjoGOvGzzIJBvCuq7IxDCIT2E76+ZHgwRTSMjbcBcn8hz9JlK0FiQ7/jA==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "2753884015"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "nmw" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.10.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-10-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-10-14T11:20:36Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "72633390"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/alias/2019.10.01/alias_nmw.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/marvin/wikidata/alias/2019.10.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
