<http://dbpedia-wikidata.tib.eu/release/wikidata/geo-coordinates/2019.12.01/dataid.ttl#Dataset> {
    <http://dbpedia-wikidata.tib.eu/release/wikidata/geo-coordinates/2019.12.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "(MARVIN is the DBpedia bot for monthly raw releases (unparsed, unsorted) for debugging the DIEF software. After its releases, data is cleaned and persisted under the dbpedia account. Commit: ) Geographic coordinates extracted from Wikidata"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "(pre-release) Geo Coordinates"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/marvin> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/marvin/wikidata/geo-coordinates> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/marvin/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/marvin/wikidata/geo-coordinates/2019.12.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Some properties from Wikidata, notably P625, are transformed to wgs84 and geo. \nThe transformation is described here:   \nhttps://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L92" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.12.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-12-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "(pre-release) Geo Coordinates"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/geo-coordinates/2019.12.01/dataid.ttl#geo-coordinates.ttl.bz2> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/marvin/wikidata/geo-coordinates>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <http://dbpedia-wikidata.tib.eu/release/wikidata/geo-coordinates/2019.12.01/dataid.ttl#geo-coordinates.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/marvin/wikidata/geo-coordinates/2019.12.01/geo-coordinates.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/geo-coordinates/2019.12.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "30876816"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2019-12-13T23:27:03Z\n<http://wikidata.dbpedia.org/resource/Q18> <http://www.georss.org/georss/point> \"-21.0 -59.0\" .\n<http://wikidata.dbpedia.org/resource/Q18> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing> .\n<http://wikidata.dbpedia.org/resource/Q18> <http://www.w3.org/2003/01/geo/wgs84_pos#lat> \"-21.0\"^^<http://www.w3.org/2001/XMLSchema#float> .\n<http://wikidata.dbpedia.org/resource/Q18> <http://www.w3.org/2003/01/geo/wgs84_pos#long> \"-59.0\"^^<http://www.w3.org/2001/XMLSchema#float> .\n<http://wikidata.dbpedia.org/resource/Q15> <http://www.georss.org/georss/point> \"1.0 17.0\" .\n<http://wikidata.dbpedia.org/resource/Q15> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing> .\n<http://wikidata.dbpedia.org/resource/Q15> <http://www.w3.org/2003/01/geo/wgs84_pos#lat> \"1.0\"^^<http://www.w3.org/2001/XMLSchema#float> .\n<http://wikidata.dbpedia.org/resource/Q15> <http://www.w3.org/2003/01/geo/wgs84_pos#long> \"17.0\"^^<http://www.w3.org/2001/XMLSchema#float> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.georss.org/georss/point> \"35.0 136.0\" ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "f608975dc63f81dd4402367ea336764bbfaa5e9150e53b80dcd06eb9080ab217" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "rUNNRSO2e5t2pXRYKEXwULSnxdq9itjuvlcCLBYK5USknWBWTSQQevJIYIczPYJfCVoFyLr3WAaSZh2yxTiwmB7hlW0SXHtc1PpmdRHgG6fhdbXcsjUa63eqVtoM42qfGgGD98cBDH8JTV0gNvEWvO3FXaN5JQ48Mi6OXgRV8H5heWJH6lh9jcvaueXvKqu9I+baefcEHqXzEV3NRWFu+Bnp+LSt2D2RDKPY9vtokoT4vuq2ukXYEUZfxIHSt8120j897JoeKXyG3teaf2oMWxaMBDfiJ6ghZQEwOu7DitVE8itsAr8J4wgqhJVA6k9lBuOxpYFMv5gDz9EBCiqrmQ==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "4422736669"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.12.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-12-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-01-03T12:09:31Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "164917722"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://dbpedia-wikidata.tib.eu/release/wikidata/geo-coordinates/2019.12.01/geo-coordinates.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/marvin/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Raw (unparsed, uncleaned) Wikidata Extraction" .
    
    <https://databus.dbpedia.org/marvin/wikidata/geo-coordinates/2019.12.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
