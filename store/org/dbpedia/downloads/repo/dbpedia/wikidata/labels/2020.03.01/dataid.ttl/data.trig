<https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/dataid.ttl#Dataset> {
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Contains the name in all the languages in wikidata."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Labels"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/wikidata/labels> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/wikidata/labels/2020.03.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "For each language only one label is used, more labels in the alias artifact.\nNames in the languages not available in the mappings wiki are found in the _nmw variant." ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata Labels"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/dataid.ttl#labels_nmw.ttl.bz2> , <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/dataid.ttl#labels.ttl.bz2> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikidata extracted and mapped by the WikidataExtractor from the DBpedia Information Extraction Framework (DIEF)" ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Extraction" .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/dataid.ttl#labels_nmw.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "nmw" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/labels/2020.03.01/labels_nmw.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Categurìa:Panagyurishte\"@eml .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Kategori:Panagyurishte\"@da .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Kategori:Panagyurishte\"@nn .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Luokka:Panagjurište\"@fi .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"קטגוריה:פנגיורישטה\"@iw .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"رده:پاناگیوریشته\"@fa .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"پۆل:پاناگیوریشتێ\"@ckb .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"பகுப்பு:பனக்யுரிஷ்டெ\"@ta .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://www.w3.org/2000/01/rdf-schema#label> \"4\"@ji .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://www.w3.org/2000/01/rdf-schema#label> \"Tatyana Kolotilshchikova\"@ast ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "1899282442e0119eb607d9c602b68c72123a09e0ec6357e000ec3ca0cc3cb500" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "AH7NfD5i+snY85FzysJjwQrpDP68/+46AEHxqObur+JngfkuDj+ErS7hnAMBi2fTLBZftZPDSdRzv7hNIkLQ4dmN29Rx01TbOfi4l1RNuVA/qpHG8LkF8xNqC4u/MbVlrvpx3dqRStZxbtG2tED47zDTcLSuRi0hsriq01z/ciOUpn92PRcrnAHDsqM+tQ774yN8f6Gv97fRJfsMnsSDqxMp0b+ZHrVu4Nd2wMwg2rYAtv3AJSEt5nY7gS844FGP3/YS4M27gnf1s/JrT36q8pWjEaCVesC8wUrk8DE91WvJEcyYzLuR4e/xUt0t3b9tP1a5J3QwACzC13JXa97Hvg==" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "nmw" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-22T16:16:39Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "660380264"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/labels_nmw.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/labels>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/labels/2020.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/dataid.ttl#labels.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/labels/2020.03.01/labels.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Categoria:Panagyurishte\"@it .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Categorie:Panagyurishte\"@nl .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Category:Panagyurishte\"@en .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Category:パナギュリシテ\"@ja .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Categoría:Panaguiúrishte\"@es .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Catégorie:Panagyurichté\"@fr .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Kategori:Panagjurisjte\"@sv .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Kategorie:Panagjurischte\"@de .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Kategorie:Panagyurishte\"@cs .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2000/01/rdf-schema#label> \"Kategória:Panagyurishte\"@sk ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "289a31e2e73f0d48fb51ad5cb4c911ee57c86fccb044a1421448c8eabfbbe16f" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "i871qfIWjpJszXHYnSwK2ACDok8/qpSix02DZ0PvazySY2PUcUMXoEsjFH9taiiFDS2bJtV4wVN3llrLjc0oNOYEbX3gzwM1+qfPpltoBbtrgHqpa6XkiqcfJcWLphN+pXgFG58EVyEnOwy6DsrWBRBKA1DQZidW24YPFZUqg2PRnSAbuP2yzn2J3awOgyUFQyiqrgBs8+ZkOfEeeWmInnRonBAC7DEU5nlOAxbdJ0lH3eGkLcDG8NW7ehupod5/lPbip28ziag25vVgw49Q4Nd/v6mO5E9r2MCDyNQvHeg8HMG7TYNTBFUpirvz7dKK67HFWbNlzK4W3bGirAlqLA==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-22T16:02:46Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "2274104840"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/labels/2020.03.01/labels.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
