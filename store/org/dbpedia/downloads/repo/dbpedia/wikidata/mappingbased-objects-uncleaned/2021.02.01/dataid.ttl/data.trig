<https://downloads.dbpedia.org/repo/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/dbpedia/wikidata/mappingbased-objects-uncleaned>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikidata extracted and mapped by the WikidataExtractor from the DBpedia Information Extraction Framework (DIEF)" ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Extraction" .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "High-quality data extracted using the mapping-based extraction (Object properties only)."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Mappingbased Objects uncleaned"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/wikidata/mappingbased-objects-uncleaned> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "The dump contains only dbo properties, which are mapped by this json file, using patterns:\nhttps://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n\n\nIf there is an owl:equivalentProperty from a Wikidata property to a DBO property, the property is replaced.\nErrors from here are recorded in the debug artifact (object vs dataype)\n\nImprovement can be most effectively done by adding more equivalent property mappings to http://mapping.dbpedia.org\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/wikidata/WikidataR2RExtractor.scala" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.02.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-02-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Mappingbased Objects uncleaned"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01/dataid.ttl#mappingbased-objects-uncleaned.ttl.bz2> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01/dataid.ttl#mappingbased-objects-uncleaned.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01/mappingbased-objects-uncleaned.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/architect> <http://wikidata.dbpedia.org/resource/Q100228067> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/city> <http://wikidata.dbpedia.org/resource/Q3258> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/country> <http://wikidata.dbpedia.org/resource/Q408> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/namedAfter> <http://wikidata.dbpedia.org/resource/Q1472221> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/namedAfter> <http://wikidata.dbpedia.org/resource/Q3914> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/namedAfter> <http://wikidata.dbpedia.org/resource/Q5491137> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/owner> <http://wikidata.dbpedia.org/resource/Q4763563> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <https://commons.wikimedia.org/wiki/Category:Franklin_Early_Childhood_School> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <https://maps.google.com/?cid=12008206822243988114> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <https://www.facebook.com/114720590219407> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "36e7d59644223e30505e79e608e199d1eff9ef297ee55aec7e72284c759b1987" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "g0/pP00ocoZMUObUA9iHTHmHkKEaCoRIjbSpYYbb62IOYdea+fiqrPsZJlae+Z71gA2cocZq6YSiX0wV6BzcfoY/4HObkDHD/evrK9zHQSLbTh6ZtTMRf7L/m4qgqGyqBZi5L4bNzaFrRZLsVvg5tvAGuNkRQcCc+PJ03VhTO1wG/t7Sqt1afJr0sdGmrpGa85JKi0mzKcPpRi4aB1jXtHyDgPXI+UGNliTeojhzKepZBCLWbTZXEaK3qp8UwcS14oXpAgcPkRfsL5uFG89Pcdt83vXrSj+DNH7MqmDnOJPF0ut8e/CeDFasB5JGGdb1+gUKpXAHpgmwcdhY3ZCt8A==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.02.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-02-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-03-09T09:25:22Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "1190714971"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/mappingbased-objects-uncleaned/2021.02.01/mappingbased-objects-uncleaned.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
