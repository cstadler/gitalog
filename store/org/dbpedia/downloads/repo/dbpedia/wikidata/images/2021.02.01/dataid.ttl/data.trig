<https://downloads.dbpedia.org/repo/dbpedia/wikidata/images/2021.02.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/dbpedia/wikidata/images>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikidata extracted and mapped by the WikidataExtractor from the DBpedia Information Extraction Framework (DIEF)" ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Extraction" .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/images/2021.02.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Images from Wikidata item"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Images"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/wikidata/images> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/wikidata/images/2021.02.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Some properties from Wikidata, notably P18, are transformed to foaf:depiction and dbo:thumbnail. \nThe transformation is described here:  \nhttps://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L111" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.02.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-02-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata Images"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/images/2021.02.01/dataid.ttl#images.ttl.bz2> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/images/2021.02.01/dataid.ttl#images.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/images/2021.02.01/images.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/images/2021.02.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/Franklin_Early_Childhood_School_interior_play_area.jpg?width=300> .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/Franklin_Early_Childhood_School_interior_play_area.jpg> .\n<http://wikidata.dbpedia.org/resource/Q100000030> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/Brückentisch.jpg?width=300> .\n<http://wikidata.dbpedia.org/resource/Q100000030> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/Brückentisch.jpg> .\n<http://wikidata.dbpedia.org/resource/Q100000034> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/Fano,_palazzo_del_cassero.JPG?width=300> .\n<http://wikidata.dbpedia.org/resource/Q100000034> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/Fano,_palazzo_del_cassero.JPG> .\n<http://wikidata.dbpedia.org/resource/Q1000003> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/MairiedeNielleslesblequin.JPG?width=300> .\n<http://wikidata.dbpedia.org/resource/Q1000003> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/MairiedeNielleslesblequin.JPG> .\n<http://wikidata.dbpedia.org/resource/Q1000005> <http://dbpedia.org/ontology/thumbnail> <http://commons.wikimedia.org/wiki/Special:FilePath/Čapek-Chod.jpg?width=300> .\n<http://wikidata.dbpedia.org/resource/Q1000005> <http://xmlns.com/foaf/0.1/depiction> <http://commons.wikimedia.org/wiki/Special:FilePath/Čapek-Chod.jpg> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "50743b4c6c58c59aa4ee41004d31b0860f9575346c9765509bfa5a9656e56652" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "FRJCuEQZ+aTdrqEhKLU4fLb93ZO7fN3j+e8vGzSD3/eZBxmqFtVVh8CCzO04zCBV6KGLEnoTSSc+zxDvsCo/OrhNc1wHSw/pYGs8oS94drTa8i8/fZtf2rcmflszlpWHO25VQ0FzDWkP9XlfyOKqfuccsbkWYUMF++dKsuiNzKTrsO64lL1DJMMFZZkzgtG/g3LaiK5VfwG2XvVFactLcd9ocFV1Ei8aOfLe+5LFoDUk+AhxQN9ZBjFaTHEcjzdufIxsUSBS3LLLU1gnov3i/fM+G3U/3LxREbdkrXDRbi/Oc/T4gVstVka4mne5LMcAN1pkHzAPM1bys63Aww2XNA==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.02.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-02-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-03-09T09:39:03Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "91089911"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/images/2021.02.01/images.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/images/2021.02.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
