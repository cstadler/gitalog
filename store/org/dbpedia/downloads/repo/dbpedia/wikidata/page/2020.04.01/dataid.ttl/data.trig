<https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/dataid.ttl#Dataset> {
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/page>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikidata extracted and mapped by the WikidataExtractor from the DBpedia Information Extraction Framework (DIEF)" ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Extraction" .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Numbers of characters contained in a Wikidata article's source and ids"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Page Length and Ids"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/wikidata/page> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/wikidata/page/2020.04.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Numbers of characters contained in a Wikidata article's source and ids" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Page Length and Ids"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/dataid.ttl#page_length.ttl.bz2> , <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/dataid.ttl#page_ids.ttl.bz2> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/dataid.ttl#page_length.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "length" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/page/2020.04.01/page_length.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q10000000> <http://dbpedia.org/ontology/wikiPageLength> \"16346\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://dbpedia.org/ontology/wikiPageLength> \"7414\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000002> <http://dbpedia.org/ontology/wikiPageLength> \"12988\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000003> <http://dbpedia.org/ontology/wikiPageLength> \"44\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000004> <http://dbpedia.org/ontology/wikiPageLength> \"44\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000005> <http://dbpedia.org/ontology/wikiPageLength> \"12266\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000006> <http://dbpedia.org/ontology/wikiPageLength> \"12846\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000007> <http://dbpedia.org/ontology/wikiPageLength> \"13595\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000008> <http://dbpedia.org/ontology/wikiPageLength> \"44\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> .\n<http://wikidata.dbpedia.org/resource/Q10000009> <http://dbpedia.org/ontology/wikiPageLength> \"44\"^^<http://www.w3.org/2001/XMLSchema#nonNegativeInteger> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "d3a7ffe99a21d4fd5160a5081dced33f55993fbd8a65926c900d6ab79b5a9647" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "XN0TKlM1sXzTdu7ACvQDqqn1cer4av9PFwf/lrMEon+BjGw5lG89yh61JKIe24chHEkofu2A7RsM6NWAH5D9FyIpganYzbwPyvNQo6NbYxfGjOJw0TZQYDu2JiIAxeqWH2RtryWkrUeq9hVgDf7dfZYh6u0kVs282sjMShyabUciS9iXX05CdRai73Vbtx91BWj9eDY/b1KijVcSCZipVeNRlipSSj3PD+F+pA3sAv6Adl1drJM1yKF3quQvyhwSWtE9wIyJwpTGiCRLjQEsN33z21M4fMk1ggWi9NUnf226e4ULOtbozQ9o0WqOjHv1ipOr6Fdq3lrIu66v/jRH0g==" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "length" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-07-08T10:52:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "340852391"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/page_length.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/page/2020.04.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/dataid.ttl#page_ids.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "ids" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/page/2020.04.01/page_ids.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Category:API_dataset> <http://dbpedia.org/ontology/wikiPageID> \"52295690\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:API_datasets> <http://dbpedia.org/ontology/wikiPageID> \"52295331\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:Admin_user_templates> <http://dbpedia.org/ontology/wikiPageID> \"52241\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:Administration_templates> <http://dbpedia.org/ontology/wikiPageID> \"4556313\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:Administrators'_noticeboard_archive> <http://dbpedia.org/ontology/wikiPageID> \"3691101\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:All_Properties> <http://dbpedia.org/ontology/wikiPageID> \"18014910\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:Alternative_Wikidata_account_templates> <http://dbpedia.org/ontology/wikiPageID> \"12346230\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:Alternative_Wikidata_accounts> <http://dbpedia.org/ontology/wikiPageID> \"12345629\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:Amit_B_Jadhav> <http://dbpedia.org/ontology/wikiPageID> \"31175075\"^^<http://www.w3.org/2001/XMLSchema#integer> .\n<http://wikidata.dbpedia.org/resource/Category:Anatomy_properties> <http://dbpedia.org/ontology/wikiPageID> \"45533360\"^^<http://www.w3.org/2001/XMLSchema#integer> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "6fe0481cd5abb5c28928f62f64d5f9d54de39cfcb56da6a50df92c244f79d271" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "nA+WxUoaydrSUSVInYZFRfDOWdPUftYPMpIPIsR4rOUY8nDdKUGyUUEBiL9RtBallgy5KptWGCV68oQ7QBOtH0GoapMs6DolHPQ2EsH3Ic11kvR+GaqaMhuicqPN0ybFTgrT1iGHXJ8/7RkuEUmKPMTDD8CNwrvjAysGtc8PsZ98RcsaMao1TMt+CdFX5oIzfFYW/prSMZOLo82M0N3SRgh+ZKqmsXpTqoK0AoQI0rf05MXMr0PTNVcFzly5zmWsWtnRqq/WyWOwNLHsUyqVnLLrdQVMvoQQS0/ijElSFw0ioQfnrxa78ZYL4t8wyzn9es4KZ80tRhasSRQuGZLuZA==" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "ids" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-07-08T10:39:53Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "238960189"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/page/2020.04.01/page_ids.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
