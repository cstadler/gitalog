<https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/dataid.ttl#Dataset> {
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Wikidata-specific dataset containing aliases for languages in the mappings wiki. Aliases for languages not in the mapping wiki are found in the _nmw variant."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Alias"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/wikidata/alias> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/wikidata/alias/2021.01.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Aliases are alternative names for items that are placed in the Also known as column of the table on top of every Wikidata item page.\nFrom https://www.wikidata.org/wiki/Help:Aliases\nThere can be several aliases for each item, but only one label (other dataset).\n\n## Issues\nCurrently data is grouped in two categories:\n* aliases from mapping wiki languages (around 40)\n* aliases from all other languages\nIn the future, we might separate these into one file per language, which will increase the number of files from 2 (nmw as content variant) now, to many (basically using iso codes as content variants, beyond the 120 wikipedia versions)\nThis is under discussion however." ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata Alias"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/dataid.ttl#alias.ttl.bz2> , <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/dataid.ttl#alias_nmw.ttl.bz2> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikidata extracted and mapped by the WikidataExtractor from the DBpedia Information Extraction Framework (DIEF)" ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Extraction" .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/dataid.ttl#alias.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/alias/2021.01.01/alias.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"Franklin School , Franklin ECS\"@en .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"Franklin School\"@ca .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"Franklin School\"@es .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"ফ্রাঙ্কলিন স্কুল , ফ্রাঙ্কলিন বিদ্যালয়\"@bn .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"フランクリン・アーリー・チャイルドフッド・スクール\"@ja .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://dbpedia.org/ontology/alias> \"Kategori:Panagyurishte\"@sv .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://dbpedia.org/ontology/alias> \"Tatyana Serafimovna Kolotilshchikova\"@en .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://dbpedia.org/ontology/alias> \"Колотильщикова Татьяна Серафимовна\"@ru .\n<http://wikidata.dbpedia.org/resource/Q100000026> <http://dbpedia.org/ontology/alias> \"Witley Park underwater conservatory\"@en .\n<http://wikidata.dbpedia.org/resource/Q100000040> <http://dbpedia.org/ontology/alias> \"Pikmin 3 Déluxe\"@fr ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "0416af10e40af57ae2e58c2b76959b560aaee6906918db7ee2fb9e222b4196d4" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "Ecn0zWAxsHYGuDrWWkOSlWg7QtiYihOCuaMU2xOcvmP3nEQa4r7sxvR4XDwdXA5S13hZeMfRCBGQ1G7ML4zfhFZCzrPYdsHXUc/IClMFGFmewOLW0GxUqLGWkAVUBKXWgwf1DkL+GohqNMdu48VhxoEbvjSRo+gv3TPRE1ulAZ5PQZWURbKFBJXcpaJRWeU7BxgTzdFpPaCeqj29p+44OknOpIiADVt1rO/edwH9xsp1MjfZRbPMdBeVEe8k0++giIIn2hK/GUmIljek6IdUy2y9f5KUWBvzA0K7RXHnsbKe7DRW4NU//hk/lBW+k91rN9PpSnrXLMLG8GDmp2IYFQ==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-01-21T10:59:55Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "183006831"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/alias.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/alias>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/dataid.ttl#alias_nmw.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "nmw" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/alias/2021.01.01/alias_nmw.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"Franklin School\"@tl .\n<http://wikidata.dbpedia.org/resource/Q100000001> <http://dbpedia.org/ontology/alias> \"ഫ്രാങ്ക്ലിൻ സ്കൂൾ\"@ml .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://dbpedia.org/ontology/alias> \"Panagjurishte\"@fi .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://dbpedia.org/ontology/alias> \"டாடியானா\"@ta .\n<http://wikidata.dbpedia.org/resource/Q1000001> <http://dbpedia.org/ontology/alias> \"黃金眼鏡蛇 (專輯)\"@zh .\n<http://wikidata.dbpedia.org/resource/Q100000694> <http://dbpedia.org/ontology/alias> \"വെമ്പായം ഗ്രാമപഞ്ചായത്ത്\"@ml .\n<http://wikidata.dbpedia.org/resource/Q100000> <http://dbpedia.org/ontology/alias> \"கீர்\"@ta .\n<http://wikidata.dbpedia.org/resource/Q10000164> <http://dbpedia.org/ontology/alias> \"Cirelli\"@ab .\n<http://wikidata.dbpedia.org/resource/Q10000164> <http://dbpedia.org/ontology/alias> \"Cirelli\"@ady .\n<http://wikidata.dbpedia.org/resource/Q10000164> <http://dbpedia.org/ontology/alias> \"Cirelli\"@am ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "89005d8faa923a8ff7773f6b15ab46f6eab5787dab0a3ebc7ecc4243a91a7493" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "m+yWJd8LmKyNRke9e/y9BsM5L0kot6XGaj5sSdYC26GOgka8lRj3jaGqVnTZZ8Zmk4mkj8oEL178UcLgJKdKDfb3hrWNcSuOqd8wWWY/hQR0614/hM4H7KXIqgjQd0QGAH9PsdtlpDTeSFsNRrcZX6BHVAF2q07KivouXj33ommZonGsLhSEdZjfazHU+aGgjzx0WMIQPCGpocOsmMV17aVArdprD/J6u959iI70j33hYi7VVV3TBxbqsnUjsAKhj6HqecjYf+DMJHbVfrjXGcGWuH0DuWgUlz1nu2NgiTvkHe7//xbcKzKWUwijvPmpyL9yPbWzfYZ8Dj8zrFh4Cw==" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "nmw" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-01-21T10:32:24Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "103299561"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/wikidata/alias/2021.01.01/alias_nmw.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/alias/2021.01.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
