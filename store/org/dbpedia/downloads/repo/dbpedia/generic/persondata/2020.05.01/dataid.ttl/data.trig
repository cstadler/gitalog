<https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/dataid.ttl#Dataset> {
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/generic>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikipedia extracted by the several Generic Extractors from the DBpedia Information Extraction Framework (DIEF)." ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikipedia Extraction with Generic Parsers" .
    
    <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "help needed"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "persondata dataset"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/generic/persondata> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/generic> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Attribution fulfilled by\n* (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/generic/persondata/persondata/2020.05.01\n* on your website:\n  * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia\n  * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin\n* in academic publications cite: DBpedia - A Large-scale, Multilingual Knowledge Base Extracted from Wikipedia, J. Lehmann, R. Isele, M. Jakob, A. Jentzsch, D. Kontokostas, P. Mendes, S. Hellmann, M. Morsey, P. van Kleef, S. Auer, and C. Bizer. Semantic Web Journal 6 (2): 167--195 (2015)\n\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* improve the software at: https://github.com/dbpedia/extraction-framework\n* improve this documentation in the pom.xml at\nhttps://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/generic/pom.xml\n\n## Origin\nThis dataset was extracted using the wikipedia-dumps available on https://dumps.wikimedia.org/\nusing the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nFor more technical information on how these datasets were generated, please visit http://dev.dbpedia.org\n\n## Changelog\n**2020.03.01**\n* full size, including dbr/dbr/dbr triples\n**2020.02.01**\n* a bug was discovered that had reomevd all dbr/dbr/dbr properties, this has been fixed for this release, but only alf of the data was extracted \n**2019.09.01** \n* fixed URI encoding and produce more valid triples\n**2019.08.01 -- skipped**\n* no release, downtime due to maintenance and integration of fixes\n**2018.12.14 - 2019.07.01**\n* Debugging period, acii2uni form before messed up URI encoding\n* Implemented [new ci test approach](https://forum.dbpedia.org/t/new-ci-tests-on-dbpedia-releases/77) \n**2018.08.15 - 2018.12.14**\n* were created as new modular releases, some issues remain:\n* language normalisation to iso codes, zh-min-nan to nan, zh-yue to yue, bat-smg to batsmg (no iso code available)\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/generic/persondata/2020.05.01> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#codeReference>
                    <https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#documentationLocation>
                    <https://git.informatik.uni-leipzig.de/dbpedia-assoc/marvin-config/-/tree/master/databus-poms/dbpedia/generic/persondata> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#feedbackChannel>
                    <https://forum.dbpedia.org/c/data/databus/14> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#issueTracker>
                    <https://github.com/dbpedia/extraction-framework/issues> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.05.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-05-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "persondata dataset"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/dataid.ttl#persondata_lang=en.ttl.bz2> , <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/dataid.ttl#persondata_lang=de.ttl.bz2> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/dataid.ttl#persondata_lang=en.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "en" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/generic/persondata/2020.05.01/persondata_lang=en.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://dbpedia.org/resource/Jim_Pewter> <http://purl.org/dc/elements/1.1/description> \"American radio personality\"@en .\n<http://dbpedia.org/resource/Jim_Pewter> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> .\n<http://dbpedia.org/resource/Jim_Pewter> <http://xmlns.com/foaf/0.1/givenName> \"Jim\"@en .\n<http://dbpedia.org/resource/Jim_Pewter> <http://xmlns.com/foaf/0.1/name> \"Jim Pewter\"@en .\n<http://dbpedia.org/resource/Jim_Pewter> <http://xmlns.com/foaf/0.1/surname> \"Pewter\"@en ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "36892613f37b1be830a66f9f7ea004a2d6e9e8a462d50d37ca57ac258a162aba" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "JJViu2y8qKdKdpKjsXGaf3SfEpvB3Q4s7tfOoe/ESJcGb3dPiTfGegxisB53K0kFs0xNL1MnI5telndG0KmQX3jt1les1k7p25qKhjAgRZgIFPWCV05GGgP5gn7Aw/Wg2bUVl87gLUR32/Szm3ckXIHEMkzSEbmHQbP1qiq+WPLJ6AGqZmGJvGjo7Yeq8+AY8xLUP1w67/WRETA7yD9wNUN2eBWf5Wu7avkGIre3q0rBslDO91Kg4Lc1XO/cXcrA0jqot+jCvkVMrjWbpUUFG127tg4rRQM9bOtAF5Uo8F9lcpvKpxc2wvNiaC9dUdIOToP4Q6v7ugqCWnmNzh8Ssg==" ;
            <http://dataid.dbpedia.org/ns/cv#lang>
                    "en" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.05.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-05-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-06-14T16:11:23Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "250"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/persondata_lang=en.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/dataid.ttl#persondata_lang=de.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "de" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/generic/persondata/2020.05.01/persondata_lang=de.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://de.dbpedia.org/resource/$ick> <http://purl.org/dc/elements/1.1/description> \"deutscher Webvideoproduzent und Autor\"@de .\n<http://de.dbpedia.org/resource/$ick> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> .\n<http://de.dbpedia.org/resource/$ick> <http://xmlns.com/foaf/0.1/name> \"$ick\"@de .\n<http://de.dbpedia.org/resource/&Me> <http://purl.org/dc/elements/1.1/description> \"deutscher DJ und Musikproduzent\"@de .\n<http://de.dbpedia.org/resource/&Me> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> .\n<http://de.dbpedia.org/resource/&Me> <http://xmlns.com/foaf/0.1/name> \"&Me\"@de .\n<http://de.dbpedia.org/resource/(.)p(...)nin> <http://purl.org/dc/elements/1.1/description> \"nubischer König\"@de .\n<http://de.dbpedia.org/resource/(.)p(...)nin> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> .\n<http://de.dbpedia.org/resource/(.)p(...)nin> <http://xmlns.com/foaf/0.1/name> \"(.)p(...)nin\"@de .\n<http://de.dbpedia.org/resource/-minu> <http://purl.org/dc/elements/1.1/description> \"Schweizer Schriftsteller, Kolumnist und Moderator\"@de ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "76ccb6ab52014d1a9bd1caebd4d85469369f2a21996ccdf8e255e886b9f94686" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "MsGfxjEu47SvKIegx9aeMVliaFOeuAasm+P2neYgd6ntJLV/62k0TPjCjHecIJsQPfILEuVoxFA2xTftP0phaFUv1mx3BbSBETzHrEv/k3M3mMXrcLkreZqEbyPqj2NEq8VLxcORWNa/axzYqrUfiyhAR9nMK5q9qZ1H7BRakAeWf0ETGg6uwd8/oLNZ01F66963fPEtoO9/J36bh7w1QzOQH6xk+yMSBRjBRiORWh4T9udWgIxKhphzL/NlclqWD4OnhYAxtuqmvviGGgNSVFyVRot/XwggVoCr8IKjeU6VtrMXlyDLBOCZOJS95DCTEl+gaMAyvpXMqvRZPyF+tQ==" ;
            <http://dataid.dbpedia.org/ns/cv#lang>
                    "de" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.05.01" ;
            <http://purl.org/dc/terms/issued>
                    "2020-05-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-06-14T16:11:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "23287460"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/generic/persondata/2020.05.01/persondata_lang=de.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/dbpedia/generic/persondata>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/dbpedia/generic/persondata/2020.05.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
