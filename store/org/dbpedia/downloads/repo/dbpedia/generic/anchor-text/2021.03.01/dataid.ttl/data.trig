<https://downloads.dbpedia.org/repo/dbpedia/generic/anchor-text/2021.03.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/dbpedia/generic/anchor-text/2021.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/generic>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikipedia extracted by the several Generic Extractors from the DBpedia Information Extraction Framework (DIEF)." ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikipedia Extraction with Generic Parsers" .
    
    <https://downloads.dbpedia.org/repo/dbpedia/generic/anchor-text/2021.03.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Human readable text (anchor text) of Wikilinks to refer to other Wikipedia articles"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Anchor text of Wikilinks"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/generic/anchor-text> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/generic> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Attribution fulfilled by\n* (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/generic/anchor-text/anchor-text/2021.03.01\n* on your website:\n  * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia\n  * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin\n* in academic publications cite: DBpedia - A Large-scale, Multilingual Knowledge Base Extracted from Wikipedia, J. Lehmann, R. Isele, M. Jakob, A. Jentzsch, D. Kontokostas, P. Mendes, S. Hellmann, M. Morsey, P. van Kleef, S. Auer, and C. Bizer. Semantic Web Journal 6 (2): 167--195 (2015)\n\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* improve the software at: https://github.com/dbpedia/extraction-framework\n* improve this documentation in the pom.xml at\nhttps://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/generic/pom.xml\n\n## Origin\nThis dataset was extracted using the wikipedia-dumps available on https://dumps.wikimedia.org/\nusing the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nFor more technical information on how these datasets were generated, please visit http://dev.dbpedia.org\n\n## Changelog\n**2020.03.01**\n* full size, including dbr/dbr/dbr triples\n**2020.02.01**\n* a bug was discovered that had reomevd all dbr/dbr/dbr properties, this has been fixed for this release, but only alf of the data was extracted \n**2019.09.01** \n* fixed URI encoding and produce more valid triples\n**2019.08.01 -- skipped**\n* no release, downtime due to maintenance and integration of fixes\n**2018.12.14 - 2019.07.01**\n* Debugging period, acii2uni form before messed up URI encoding\n* Implemented [new ci test approach](https://forum.dbpedia.org/t/new-ci-tests-on-dbpedia-releases/77) \n**2018.08.15 - 2018.12.14**\n* were created as new modular releases, some issues remain:\n* language normalisation to iso codes, zh-min-nan to nan, zh-yue to yue, bat-smg to batsmg (no iso code available)\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/generic/anchor-text/2021.03.01> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#codeReference>
                    <https://github.com/dbpedia/extraction-framework/blob/master/core/src/org/dbpedia/extraction/mappings/AnchorTextExtractor.scala> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#documentationLocation>
                    <https://git.informatik.uni-leipzig.de/dbpedia-assoc/marvin-config/-/tree/master/databus-poms/dbpedia/generic/anchor-text> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#feedbackChannel>
                    <https://forum.dbpedia.org/c/data/databus/14> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#issueTracker>
                    <https://github.com/dbpedia/extraction-framework/issues> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Anchor text of Wikilinks"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/dbpedia/generic/anchor-text/2021.03.01/dataid.ttl#anchor-text_lang=en.ttl.bz2> .
    
    <https://databus.dbpedia.org/dbpedia/generic/anchor-text>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://downloads.dbpedia.org/repo/dbpedia/generic/anchor-text/2021.03.01/dataid.ttl#anchor-text_lang=en.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "en" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/generic/anchor-text/2021.03.01/anchor-text_lang=en.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/dbpedia/generic/anchor-text/2021.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "# started 2021-02-26T16:18:15Z\n<http://dbpedia.org/resource/Constructed_language> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Constructed language\"@en .\n<http://dbpedia.org/resource/Afghan_Armed_Forces> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Afghan Armed Forces\"@en .\n<http://dbpedia.org/resource/Constructed_language> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Constructed language\"@en .\n<http://dbpedia.org/resource/Atlas_Shrugged> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Atlas Shrugged\"@en .\n<http://dbpedia.org/resource/Abbey> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Abbey\"@en .\n<http://dbpedia.org/resource/Economy_of_Albania> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Economy of Albania\"@en .\n<http://dbpedia.org/resource/Aberdeen,_South_Dakota> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Aberdeen, South Dakota\"@en .\n<http://dbpedia.org/resource/Cain_and_Abel> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Cain and Abel\"@en .\n<http://dbpedia.org/resource/Arthur_Koestler> <http://dbpedia.org/ontology/wikiPageWikiLinkText> \"Arthur Koestler\"@en ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "db4b2b0047407c06e7cabbd496e5a58b2cd49c324523e4976c6562eb557743e1" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "WXau6mijNI8UlJS/eSAUzuNsTPDWRTrP85Ze6AfBVT1Gy89hZKkSFpyNiQaIy9vWn88+N4yXc5tHR0lzK1kVNHxgcWNb5vlazxcr18uebcmOP0aHfVGtwZAHppW1MK2srq7nNTIt+qFt6qU/ER3fM/psjUReWcmmUKQcjT788BH7/ztQRD6TW+03HcoiqxrXJQg7RpjESB1BeIii+aZj1ILM5WNWxRoD6moFdS51Qp4zjF7fcLDL9/hkkrocpTAAxNBKsjTsg1XOsX2sZnqrtnR7rM6kbu3IPpXzlSNLv56dhQyqCNhxyCFCCBpHpRqAKTKNNK/SHFXQGOO39xua5g==" ;
            <http://dataid.dbpedia.org/ns/cv#lang>
                    "en" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-03-21T15:53:04Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "2389910385"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/dbpedia/generic/anchor-text/2021.03.01/anchor-text_lang=en.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
