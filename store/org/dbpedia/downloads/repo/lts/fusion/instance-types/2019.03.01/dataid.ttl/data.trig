<https://downloads.dbpedia.org/repo/lts/fusion/instance-types/2019.03.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/dbpedia/prefusion/instance-types/2019.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/dbpedia/fusion>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "FlexiFused data" .
    
    <https://downloads.dbpedia.org/repo/lts/fusion/instance-types/2019.03.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia Fusion instance types"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia Fusion instance types"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/fusion/instance-types> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/fusion> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/fusion/instance-types/2019.03.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "\n\n# Group Documentation\n# Fusion Dataset\nThe DBpedia Fusion dataset is a new addition to the modular DBpedia releases combining DBpedia data from over 140 Wikipedia language editions and Wikidata. As an intermediate step in the FlexiFusion workflow [1], a global and unified preFused view is provided on the essence of DBpedia releases (see origin section).\nBased on this PreFusion dump a fused RDF dataset is derived, that provides improved data quality.\n\n## Description\n* contains all entities (with DBpedia Global Identifiers if exist) from PreFusion Dump based on 140 DBpedia chapters \n* conflicting values are resolved by FlexiFusion resolve function:\n  * predicate-median-outdegree calculated for every property (see [1] for more details) to classify \"functional properties\" is calculated and stored as tsv file\n  * in case of conflicts for a \"functional property\" a source preference list (EN, Wikidata, DE, FR, NL, SV) is used to select one value from multiple options \n  * union of values for non-functional properties and rdfs:label\n\n## Attribution fulfilled by\n* (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/fusion/instance-types/instance-types/2019.03.01\n* on your website:\n  * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia\n  * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin\n* in academic publications cite [1]\n\n## How to contribute\nDBpedia is a community project, help us with:\n\n- Improving the input datasets (see docu for DBpedia and Wikidata extractions in origin section)\n- upload datasets using DBpedia ontology and providing sameAs links to DBpedia entities on the databus and contact the maintainer so that it can be included.\n\n## Origin\nThis dataset is based on\n- https://databus.dbpedia.org/dbpedia/prefusion\n\nFor more specific information see the provenance information of the artifact version of interest.\n\n## References\n\n- [ 1]  Frey J, Hofer M. Hellmann S, Obraczka D, *DBpedia FlexiFusion Best of Wikipedia > Wikidata > Your Data*. ISWC Ressource Track 2019 (submitted). Available at: https://svn.aksw.org/papers/2019/ISWC_FlexiFusion/public.pdf\n\n# Changelog\n## 2019.03.01\n* first official release containing up to 140 DBpedia chapters and Wikidata extraction\n* introduced new artifacts structure\n* switched to new HTTPS Global IDs\n* usage of new FlexiFusion Workflow (reduce & resolve and new preference list)\n\n## 2018.10.01initeval\n* initial prototype for evaluation containing 5 DBpedia's + Wikidata" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia Fusion instance types"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/lts/fusion/instance-types/2019.03.01/dataid.ttl#instance-types_reduce=dbpw_resolve=union.ttl.bz2> ;
            <http://www.w3.org/ns/prov#wasDerivedFrom>
                    <https://databus.dbpedia.org/dbpedia/prefusion/instance-types/2019.03.01> .
    
    <https://databus.dbpedia.org/dbpedia/fusion/instance-types>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/dbpedia/fusion/instance-types/2019.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://downloads.dbpedia.org/repo/lts/fusion/instance-types/2019.03.01/dataid.ttl#instance-types_reduce=dbpw_resolve=union.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia Fusion instance types"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia Fusion instance types"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "union" , "dbpw" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/fusion/instance-types/2019.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "1497738"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://ru.dbpedia.org/resource/Нерсисян,_Мкртич_Гегамович> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Agent> .\n<https://global.dbpedia.org/id/56sRa> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://global.dbpedia.org/id/4dTEM> .\n<https://global.dbpedia.org/id/4bcNZ> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#NaturalPerson> .\n<https://global.dbpedia.org/id/2Zogq> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Place> .\n<https://global.dbpedia.org/id/2bw3k> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://global.dbpedia.org/id/12HY1p> .\n<http://de.dbpedia.org/resource/Oreocereus__Edward_F._Anderson__1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Work> .\n<https://global.dbpedia.org/id/4oS5N> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person> .\n<https://global.dbpedia.org/id/1ty1q> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Settlement> .\n<http://it.dbpedia.org/resource/Como_Foot-Ball_Club_1913-1914__16_novembre__1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/SportsEvent> .\n<https://global.dbpedia.org/id/4hjzN> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Insect> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "d8e6114aa494030001ef3ea7f0ba0d2343dcb1e38de4dcaa8f4d1895adbbe93c" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "PinyMcz/qRnuliPLu7wP99kiB7pgPu9VtEA1Ny6U7Jyxpatdf1ZZ8d0kEXIrQz3s76vN2PWUEpm5QuQtKNF1/nQaJQxsn+Ee8zdIezdRZTer2TfBy5/uf0/SsmaRSpnuoxkSJpR4l2/LNnqLH8BT1f8PC369OndqdIj/bi5Gv/4cs937FiOOAtzdfY83Qj6I/174da1C8nNIFgtrBXOT40JpaOdi5eJTn70MyNp01DpzCi2UhGMwmoESlmfAR/EA7exEmSqxutbvu14QHcXwbf5MyM327yLVFP5stxFgktAzWGSoKitpx4GpcIYm9wTU+asLnhhWxnc7WWj5P2cGRA==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "203617199"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#reduce>
                    "dbpw" ;
            <http://dataid.dbpedia.org/ns/cv#resolve>
                    "union" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-04-17T11:07:18Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia Fusion instance types"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "1617927111"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/fusion/instance-types/2019.03.01/instance-types_reduce=dbpw_resolve=union.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
