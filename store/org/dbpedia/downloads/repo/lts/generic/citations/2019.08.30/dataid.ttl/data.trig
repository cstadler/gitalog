<https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/dataid.ttl#Dataset> {
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/generic>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikipedia extracted by the several Generic Extractors from the DBpedia Information Extraction Framework (DIEF)." ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikipedia Extraction with Generic Parsers" .
    
    <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Contains citations from articles and bibliographic information of the references"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Citations and References"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/generic/citations> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/generic> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Attribution fulfilled by\n* (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/generic/citations/citations/2019.08.30\n* on your website:\n  * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia\n  * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin\n* in academic publications cite: DBpedia - A Large-scale, Multilingual Knowledge Base Extracted from Wikipedia, J. Lehmann, R. Isele, M. Jakob, A. Jentzsch, D. Kontokostas, P. Mendes, S. Hellmann, M. Morsey, P. van Kleef, S. Auer, and C. Bizer. Semantic Web Journal 6 (2): 167--195 (2015)\n\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* improve the software at: https://github.com/dbpedia/extraction-framework\n* improve this documentation in the pom.xml at\nhttps://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/generic/pom.xml\n\n## Origin\nThis dataset was extracted using the wikipedia-dumps available on https://dumps.wikimedia.org/\nusing the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nFor more technical information on how these datasets were generated, please visit http://dev.dbpedia.org\n\n## Changelog\n**2019.09.01** \n* fixed URI encoding and produce more valid triples\n**2019.08.01 -- skipped**\n* no release, downtime due to maintenance and integration of fixes\n**2018.12.14 - 2019.07.01**\n* Debugging period, acii2uni form before messed up URI encoding\n* Implemented [new ci test approach](https://forum.dbpedia.org/t/new-ci-tests-on-dbpedia-releases/77) \n**2018.08.15 - 2018.12.14**\n* were created as new modular releases, some issues remain:\n* language normalisation to iso codes, zh-min-nan to nan, zh-yue to yue, bat-smg to batsmg (no iso code available)\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/generic/citations/2019.08.30> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#codeReference>
                    <https://github.com/dbpedia/extraction-framework/blob/master/core/src/org/dbpedia/extraction/mappings/CitationExtractor.scala> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "The `_links` files contain the citations of one article by linking the citation URL via `isCitedBy` property. The `_data` files contain bibliographic metadata (e.g. format, title, author, website, accessdate, etc. if available) for the citations." ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.08.30" ;
            <http://purl.org/dc/terms/issued>
                    "2019-08-30T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Citations and References"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/dataid.ttl#citations_lang=en_data.ttl.bz2> , <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/dataid.ttl#citations_lang=en_links.ttl.bz2> .
    
    <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/dataid.ttl#citations_lang=en_data.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "data" , "en" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "255940"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/generic/citations/2019.08.30/citations_lang=en_data.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "94394712"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://nyphil.org/~/media/pdfs/program-notes/1314/gershwin-an%20american%20in%20paris.pdf> <http://dbpedia.org/property/format> \"PDF\" .\n<http://nyphil.org/~/media/pdfs/program-notes/1314/gershwin-an%20american%20in%20paris.pdf> <http://dbpedia.org/property/title> \"Rhapsody in Blue for Piano and Orchestra : An American in Paris\" .\n<http://nyphil.org/~/media/pdfs/program-notes/1314/gershwin-an%20american%20in%20paris.pdf> <http://dbpedia.org/property/author> \"George Gershwin\" .\n<http://nyphil.org/~/media/pdfs/program-notes/1314/gershwin-an%20american%20in%20paris.pdf> <http://dbpedia.org/property/website> \"Nyphil.org\" .\n<http://nyphil.org/~/media/pdfs/program-notes/1314/gershwin-an%20american%20in%20paris.pdf> <http://dbpedia.org/property/accessdate> \"2016-04-06\"^^<http://www.w3.org/2001/XMLSchema#date> .\n<http://www.kennedy-center.org/calendar/?fuseaction=composition&composition_id=2983> <http://dbpedia.org/property/title> \"An American in Paris: About the Work\" .\n<http://www.kennedy-center.org/calendar/?fuseaction=composition&composition_id=2983> <http://dbpedia.org/property/author> \"Richard Freed\" .\n<http://www.kennedy-center.org/calendar/?fuseaction=composition&composition_id=2983> <http://dbpedia.org/property/accessdate> \"2012-12-05\"^^<http://www.w3.org/2001/XMLSchema#date> .\n<https://books.google.com/books/about/gershwin_his_life_and_music.html?id=ekofaaaamaaj> <http://dbpedia.org/property/title> \"George Gershwin: His Life and Music\" .\n<https://books.google.com/books/about/gershwin_his_life_and_music.html?id=ekofaaaamaaj> <http://dbpedia.org/property/last> \"Schwartz\" ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "07bc499178104f3c1e981ee7fde20fb3cb00ee7385f75e1f8e6f04e7ac6a333c" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "Y3SmuO/TuoUBkThUZ0q6hfRlbm4Wptb0YNuKgCva9toQ6ATqWOOBO5BxAVMBl2C90nDdhI2uBjvaGQS7R4d7or27evWUULhrDfXHa3KhMONygz6sv98gnCQIbx4c8KHBhgiZqYcQjwTFfF5A1uRI8Ci7ca4JT5gVYfiHHvLBLI03ubZy6IVaFbSYSwLlnneSY/Zb/Do5+2cXhodkgl4fwSU9d4cmW66SviXMlPrw9QgZOFWK/lILTmjQrx7xjpw0L7Og/jiL9s36RH/R1VED5Ve10p9ZGabyDnAEa9+E5TT0EwTbSJ6ljiHckO4ZivJEKN8VdJKuoNs/A5z5XthXzQ==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "12927101734"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#lang>
                    "en" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "data" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.08.30" ;
            <http://purl.org/dc/terms/issued>
                    "2019-08-30T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-09-09T21:16:09Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "1500011579"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/citations_lang=en_data.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/dataid.ttl#citations_lang=en_links.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "en" , "links" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "446333"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/generic/citations/2019.08.30/citations_lang=en_links.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "17861611"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://nyphil.org/~/media/pdfs/program-notes/1314/gershwin-an%20american%20in%20paris.pdf> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<http://www.kennedy-center.org/calendar/?fuseaction=composition&composition_id=2983> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<https://books.google.com/books/about/gershwin_his_life_and_music.html?id=ekofaaaamaaj> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<https://www.theatlantic.com/magazine/archive/1998/10/misunderstanding-gershwin/377252/> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<http://gershwin.com/publications/an-american-in-paris-concert-work/> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<http://books.google.com/books?vid=ISBN978-0-306-80739-8> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<https://archive.org/details/bolerolifeofmaur1940goss/page/216> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<http://citation.dbpedia.org/hash/f0ef59790760b01e4a58e7ce3f1ad8365e12ab046821be29e603d371584c2c91> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<https://csosoundsandstories.org/fascinatin-rhythm-when-ravel-met-gershwin-in-jazz-age-new-york/> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> .\n<http://books.google.com/books?vid=ISBN978-0-945193-38-8> <http://dbpedia.org/property/isCitedBy> <http://dbpedia.org/resource/An_American_in_Paris> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "51879824e46c39b44ecf09b231f48c6723fbdcc989a64594148630ee0b345007" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "SVJIoH657Ayn+/Azj6gdwQalaHfZJvZVAneWwapoL71LDhyTFCIAJffFGX/fQvWXtxdt2KJ/c2Quf9YK1XBPVXitbNU/DOj3DudLf943m00M251EQQoPScDroZCi+wBGmhxzwvlfBQ8buraLX67bfu8OUdiJdq0W9dIpgyub5UfmZZ2bKBcz2nBEvYKfFeUu7CGMhjF0TX3vbI72nHpDd8eowQ8vyFK4QpiVGCv/Qh1eFtJl3rAGcd4GVDduOm7yS6JXLTE87gnWIuzZxsedHhjFUx9nv8c6xsBLZhjFDuyazYCwj8KPjaTwEytyXHBqcKbgyPRC0UXitZje1GnmZw==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "3050537392"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#lang>
                    "en" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "links" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.08.30" ;
            <http://purl.org/dc/terms/issued>
                    "2019-08-30T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-09-09T21:32:50Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "469386738"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/generic/citations/2019.08.30/citations_lang=en_links.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/dbpedia/generic/citations/2019.08.30>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/dbpedia/generic/citations>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
}
