<https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/dataid.ttl#Dataset> {
    <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Contains triples of the form $object rdf:type $class"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Instance Types"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/wikidata/instance-types> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/wikidata/instance-types/2019.08.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Using P31 as rdf:type . \nIf the typed class is in in the DBpedia Ontology, it will be used, otherwise the type is discarded. \nE.g. \n\n```\n<QXYZ> <P31> <Q5> . # Q5 is Person\n<Q5> owl:equivalentClass dbo:Person .\n------------------------\n<QXYZ> rdf:type dbo:Person .\n```\nFunction used:\n```\n \"P31\": [\n        {\n            \"rdf:type\": \"$getDBpediaClass\"\n        }\n],\n\n```\n\nThe extractor uses the data from ontology-subclass-of artifact to optimize the hierarchy and enrich equivalent classes.\n\nThe mappings between Wikidata Items and classes can be edited in the [Mappings Wiki](http://mappings.dbpedia.org/index.php/OntologyClass:Person)\n\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L92\n* https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json#L87" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.08.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-08-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata Instance Types"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/dataid.ttl#instance-types_transitive.ttl.bz2> , <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/dataid.ttl#instance-types.ttl.bz2> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikidata extracted and mapped by the WikidataExtractor from the DBpedia Information Extraction Framework (DIEF)" ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Extraction" .
    
    <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/dataid.ttl#instance-types_transitive.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "transitive" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/instance-types/2019.08.01/instance-types_transitive.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "111475758"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Country> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.wikidata.org/entity/Q6256> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/PopulatedPlace> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Place> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Place> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Location> .\n<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing> .\n<http://wikidata.dbpedia.org/resource/Q20> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Country> .\n<http://wikidata.dbpedia.org/resource/Q20> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.wikidata.org/entity/Q6256> .\n<http://wikidata.dbpedia.org/resource/Q20> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/PopulatedPlace> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "47377d951b56a13d0745364a1fa30ee9f57433d73a83a65d1cd2358dced336a2" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "WHAXFUsy311QJmPoqPP4FkN6R5w5lcQ+GuPCYHwFcOWSmWe00uFAsKYjm+MU32GafJXvk9/urb8dG43VDGc4JlaMYQWdx/6MtpF36EgmAVUKFk1RNlzS9ckegzvH+XFwFBzOhxgQ5Ge3Ippo/8XDOTYutXLoQUa4yRI+Jovx2Ab856Kxwp52fH1f3IUZiKVvm14PUP7oJS2VbqTsDdQXbH65+bXqRFnzTDTguqH+YJ0dun4ykjFNBpzsMCFtOSRVyigMoOx97lKHxXB6J8gYik51Y0f4fwvIVFJG03Aol7SBwXCFUyQUiX0Uz3y96EFn+gndq0lhbNDfrIQXVCcf2Q==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "15845810852"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "transitive" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.08.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-08-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-09-09T20:39:57Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "229308276"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/instance-types_transitive.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/instance-types/2019.08.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/instance-types>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/dataid.ttl#instance-types.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/dbpedia/wikidata/instance-types/2019.08.01/instance-types.ttl.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "16546732"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q17> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q20> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q25> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q30> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q31> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Country> .\n<http://wikidata.dbpedia.org/resource/Q23> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q42> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q1971> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Single> .\n<http://wikidata.dbpedia.org/resource/Q1868> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> .\n<http://wikidata.dbpedia.org/resource/Q80> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Person> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "768d5fbfcd6258dc3dffa14b1c603d779d57841e5932ef263effd70abf31752a" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "kDlYAZkPmGfT2hJCPyxoxYjH1fkxfvOGu61m8cpUolfw/2cbZSTHYOBSlayU7b5mciBFGovt42i2MSNulff0zlnOj7fDhpq7oriOmwjOq3dso+LBFKKWY7nbz7h7Wk6+HeVNv+Xpvvipd8Z1Z4nE/I1wDva+alvHnGQHGlNffhbDD12w64XAg2mscXeR2/2fjCXFpyIO5SNeTDEQRMsFG+YmhNr7skc8ffC02WfGwE00G9P/kpiu48HUOO4nV3MnxGyxCa3wVLZxUYxSOqfg3PgV9J9ROF3sRh2j5f7+V3Y0aCl6sBRWrPu1SPW92MZdhx6uaPETepQNhAXx43BN0Q==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "2292037155"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.08.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-08-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-09-09T20:27:48Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "39721417"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/instance-types/2019.08.01/instance-types.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
