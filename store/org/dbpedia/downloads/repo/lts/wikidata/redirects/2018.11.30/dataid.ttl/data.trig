<https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/dataid.ttl#Dataset> {
    <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Dataset containing redirects between articles in Wikidata."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Redirects"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/wikidata/redirects> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/wikidata/redirects/2018.11.30> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "The extractor responsible for this dataset is:\nhttps://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/RedirectExtractor.scala\n\n## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2018.11.30" ;
            <http://purl.org/dc/terms/issued>
                    "2018-11-30T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Redirects"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/dataid.ttl#redirects_transitive.ttl.bz2> , <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/dataid.ttl#redirects.ttl.bz2> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/redirects>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikidata extracted and mapped by the WikidataExtractor from the DBpedia Information Extraction Framework (DIEF)" ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Extraction" .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/redirects/2018.11.30>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/dataid.ttl#redirects_transitive.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Dataset containing redirects between articles in Wikidata."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Redirects"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "transitive" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "543"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Category:API_dataset> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:API_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Data_dump_dataset> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Data_dump_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Dataset_Import_Hub_categories> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Dataset_Imports_categories> .\n<http://wikidata.dbpedia.org/resource/Category:Historical_Place> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:WikiProject_Historical_Place> .\n<http://wikidata.dbpedia.org/resource/Category:List_datasets> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Text_document_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Navigational_modules> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Navigation_modules> .\n<http://wikidata.dbpedia.org/resource/Category:Text_document_dataset> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Text_document_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Unknown_datasets> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Unknown_update_type_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Webpage_dataset> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Web_page_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Wikidata:Requests_for_comment> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Wikidata:Requests_for_Comment> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "dd43fd14a1f39dd61b1312c9881a127ea150e6d975248e39e4706aa01aaada30" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "aZKE2StG8Do5ovt8EnKcRJzeIQND0PiJUdU4X1zaOWr4+taJ1up8BhiIOV8r/kMUpplMaq6f+53UiwtJO5xKlK9SEvVjOdUzJ3m3J6TkzLFehUilyXo2zjhDBEv46TUNC5ekF4xINQZKZgXu69D+p88qYDSQO6L913GYzXpGJRPusQ+3DWxAnoBUkSFXtdfsd/tS78lE9HM3dZfplFhaeEkS5wsOqrt/j9SmQ/0I847nLDzeSxN27MICPX0nxzKSFwynxuEYgCkao0m8xQAaGyDjm+Bdy2SIolti1ukVb/FeOVMjoqBJdVg0NB8WLemS85t2fquLI1LB+GeA0fbJKQ==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    true ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "91940"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "transitive" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2018.11.30" ;
            <http://purl.org/dc/terms/issued>
                    "2018-11-30T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-02-12T00:38:15Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Redirects"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "8518"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/redirects_transitive.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/dataid.ttl#redirects.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Dataset containing redirects between articles in Wikidata."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Redirects"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "543"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Category:API_dataset> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:API_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Data_dump_dataset> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Data_dump_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Dataset_Import_Hub_categories> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Dataset_Imports_categories> .\n<http://wikidata.dbpedia.org/resource/Category:Historical_Place> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:WikiProject_Historical_Place> .\n<http://wikidata.dbpedia.org/resource/Category:List_datasets> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Text_document_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Navigational_modules> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Navigation_modules> .\n<http://wikidata.dbpedia.org/resource/Category:Text_document_dataset> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Text_document_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Unknown_datasets> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Unknown_update_type_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Webpage_dataset> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Web_page_datasets> .\n<http://wikidata.dbpedia.org/resource/Category:Wikidata:Requests_for_comment> <http://dbpedia.org/ontology/wikiPageRedirects> <http://wikidata.dbpedia.org/resource/Category:Wikidata:Requests_for_Comment> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "dd43fd14a1f39dd61b1312c9881a127ea150e6d975248e39e4706aa01aaada30" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "aZKE2StG8Do5ovt8EnKcRJzeIQND0PiJUdU4X1zaOWr4+taJ1up8BhiIOV8r/kMUpplMaq6f+53UiwtJO5xKlK9SEvVjOdUzJ3m3J6TkzLFehUilyXo2zjhDBEv46TUNC5ekF4xINQZKZgXu69D+p88qYDSQO6L913GYzXpGJRPusQ+3DWxAnoBUkSFXtdfsd/tS78lE9HM3dZfplFhaeEkS5wsOqrt/j9SmQ/0I847nLDzeSxN27MICPX0nxzKSFwynxuEYgCkao0m8xQAaGyDjm+Bdy2SIolti1ukVb/FeOVMjoqBJdVg0NB8WLemS85t2fquLI1LB+GeA0fbJKQ==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    true ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "91940"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2018.11.30" ;
            <http://purl.org/dc/terms/issued>
                    "2018-11-30T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-02-12T00:38:15Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Redirects"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "8518"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/redirects/2018.11.30/redirects.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
