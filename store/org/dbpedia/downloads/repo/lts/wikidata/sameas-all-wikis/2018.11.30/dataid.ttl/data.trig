<https://downloads.dbpedia.org/repo/lts/wikidata/sameas-all-wikis/2018.11.30/dataid.ttl#Dataset> {
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Structured data from Wikidata extracted and mapped by the WikidataExtractor from the DBpedia Information Extraction Framework (DIEF)" ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata Extraction" .
    
    <https://downloads.dbpedia.org/repo/lts/wikidata/sameas-all-wikis/2018.11.30/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Sameas for all wikis"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata SameAs All Wikis"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/wikidata/sameas-all-wikis> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/wikidata> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/wikidata/sameas-all-wikis/2018.11.30> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "Contains the interlanguage links\n\n## Origin\nThis dataset was extracted using the XML wikidata-dumps available on https://www.wikidata.org/wiki/Wikidata:Database_download\nThe extraction is performed using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework\nYou may find more details on the Wikidata extractions at \"Wikidata through the eyes of DBpedia\" by Ali Ismayilov, Dimitris Kontokostas, Sören Auer, Jens Lehmann and Sebastian Hellmann.\nYou may download the article from http://www.semantic-web-journal.net/content/wikidata-through-eyes-dbpedia-1\n\n## How to contribute\nDBpedia is a community project, help us with this dataset:\n* Extend and/or improve the Wikidata class & ontology mappings at the DBpedia mappings wiki (http://mappings.dbpedia.org)\n* Extend and/or improve the Wikidata json mappings at https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/resources/wikidatar2r.json\n* Improve this documentation in the pom.xml at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/wikidata/pom.xml\n* Improve the software at: https://github.com/dbpedia/extraction-framework\n\n## Known Issues\n* RDF parsing and sorting failed for artifacts rediects, references, raw and sameas*\n* we are attempting to switch from rapper to Sansa-Stack: http://sansa-stack.net/sansa-parser-performance-improved/\n\n## Group Changelog\n### 2018.07.20 to 2018.11.30\n* were created as new modular releases, some issues remain\n* removed wikidata-sameas\n* removed raw for now\n* mappingbased objects are not cleaned\n* we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe\ncharacters\n* link to Wikimedia dump version is missing\n## 2018.10.01\n* deleted due to bzip2 non-recoverable bad blocks" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2018.11.30" ;
            <http://purl.org/dc/terms/issued>
                    "2018-11-30T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata SameAs All Wikis"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/sameas-all-wikis/2018.11.30/dataid.ttl#sameas-all-wikis.ttl.bz2> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/sameas-all-wikis>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/dbpedia/wikidata/sameas-all-wikis/2018.11.30>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://downloads.dbpedia.org/repo/lts/wikidata/sameas-all-wikis/2018.11.30/dataid.ttl#sameas-all-wikis.ttl.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Sameas for all wikis"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Wikidata SameAs All Wikis"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/sameas-all-wikis/2018.11.30/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://alismayilov.github.io/webid-ali.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "6473"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2002/07/owl#sameAs> <http://bg.dbpedia.org/resource/Категория:Панагюрище> .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2002/07/owl#sameAs> <http://ckb.dbpedia.org/resource/پۆل:پاناگیوریشتێ> .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2002/07/owl#sameAs> <http://commons.dbpedia.org/resource/Category:Panagyurishte> .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2002/07/owl#sameAs> <http://dbpedia.org/resource/Category:Panagyurishte> .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2002/07/owl#sameAs> <http://de.dbpedia.org/resource/Kategorie:Panagjurischte> .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2002/07/owl#sameAs> <http://fa.dbpedia.org/resource/رده:پاناگیوریشته> .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2002/07/owl#sameAs> <http://ja.dbpedia.org/resource/Category:パナギュリシテ> .\n<http://wikidata.dbpedia.org/resource/Q10000000> <http://www.w3.org/2002/07/owl#sameAs> <http://mk.dbpedia.org/resource/Категорија:Панаѓуриште> .\n<http://wikidata.dbpedia.org/resource/Q10000001> <http://www.w3.org/2002/07/owl#sameAs> <http://ru.dbpedia.org/resource/Колотильщикова,_Татьяна_Серафимовна> .\n<http://wikidata.dbpedia.org/resource/Q10000002> <http://www.w3.org/2002/07/owl#sameAs> <http://ceb.dbpedia.org/resource/Kategoriya:Graphostroma> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "6f313d270870c3915fd666126ab4f74fdf2af9af2d5010bab66f21277eb82de1" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "h7pD8vDVPDREg+HahZZB7wW628+VxEJ+T46TNSPRM3ze8Hcj95U0X4TfVxZ28XynUyPDdjbi6nlsItaw8t5H1V27z3Bu4CH8SiplO9F0LTJp3dpp22rQQReQfHXYlccP5p6ZAqfdehgxAapNZ9RopUZ/Em6mm5j1uj529mwfBIpgm/ahwPfPJ5fzHhY5awL+xTFsEpRtTtcub6zlxmMaGgZG93tX27qIEdCREheTpc2ycmKSaoFVD7qJYWA9CI4ZeoPjIyuZb5xwl9wa69gIgcXrzlTbbCrZnSW/e3erauMP6Qd6kt/1f7x24zVsY4KeWzZFx4iG6mZAcvUQJhPM6Q==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    true ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "940898"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2018.11.30" ;
            <http://purl.org/dc/terms/issued>
                    "2018-11-30T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-02-11T09:31:34Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "Wikidata SameAs All Wikis"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "564585191"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/wikidata/sameas-all-wikis/2018.11.30/sameas-all-wikis.ttl.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
