<https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/dbpedia/prefusion/instance-types/2019.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/dbpedia/prefusion>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Aggregated data from other groups in prefusion state" .
    
    <https://databus.dbpedia.org/dbpedia/prefusion/instance-types>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia PreFusion instance types"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia PreFusion instance types"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/prefusion/instance-types> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/prefusion> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/prefusion/instance-types/2019.03.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "\n\n# Group Documentation\n# PreFusion Dataset\nThe DBpedia PreFusion dataset is a new addition to the modular DBpedia releases combining DBpedia data from over 140 Wikipedia language editions and Wikidata. As an intermediate step in the FlexiFusion workflow [1], a global and unified preFused view is provided on the essence of DBpedia releases (see origin section).\nThe facts are harvested as RDF triples and aggregated using a new serialization format to track statement-level provenance.\nUnified access to knowledge from different sources is achieved by exploiting previously existing mappings of the DBpedia Ontology as well as merged, normalized entity identifiers (DBpedia Global IDs).\nThe ontology defines a comprehensive class hierarchy and properties, which are modelling common entities described in Wikipedia and Wikidata, and also reuses prominent vocabularies like FOAF and PROV. The dataset offers knowledge about very broad domains (like persons, organizations) but also for very specific domains (e.g. nutrition facts or animal classifications).\n\n## Description\nThe PreFusion dataset is stored as JSON-LD using a custom scheme optimized for an efficient representation of entities with overlapping object values and statement-level provenance. Thus, the dataset can be loaded both into JSON document/NoSQL stores, in case simple lookups are required – and triple stores – in case joins are required. Each PreFusion document describes a normalized subject-predicate pair (*SP−pair*) to aggregate all different object and literal values from the input sources as shown in the example below. The provenance record(s) are referencing the corresponding input file(s) of the object value and iHash value(s) which can be used to determine the original (non-normalized) IRI(s) of the triple(s) with hashing the result of the Global ID Resolution service. For more details see [1]\n\n    {\n      \"@id\": \"9336c15aed0392448b9cdb9d46b8f82e9942\",\n      \"@context\": \"https://databus.dbpedia.org/dbpedia/fusion/ fused-geo-coordinates/2018.11.25/fusion=prov-context.jsonld\",\n      \"subject\": {\n        \"@id\": \"https://global.dbpedia.org/id/J1kSq\" },\n      \"predicate\": {\n        \"@id\": \"http://www.w3.org/2003/01/geo/wgs84_pos#long\" },\n      \"objects\": [\n        {\n          \"object\": {\n            \"@value\": \"-2.3094\",\n            \"@type\": \"http://www.w3.org/2001/XMLSchema#float\" },\n          \"source\": [ {\n              \"@id\": \"d0:lang=commonswiki\",\n              \"iHash\": \"44473\" } ]\n        }\n      ]\n    }\n\n## Attribution fulfilled by\n* (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/prefusion/instance-types/2019.03.01\n* on your website:\n  * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia\n  * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin\n* in academic publications cite [1]\n\n## How to contribute\nDBpedia is a community project, help us with:\n\n - Improving the input datasets (see docu for DBpedia and Wikidata extractions in origin section)\n - upload datasets using DBpedia ontology and providing sameAs links to DBpedia entities on the databus and contact the maintainer so that it can be included.\n\n## Origin\nThis dataset is based on\nDBpedia extractions\n - https://databus.dbpedia.org/dbpedia/mappings\n - https://databus.dbpedia.org/dbpedia/generic\n\nWikidata extractions\n - https://databus.dbpedia.org/dbpedia/wikidata\n\nFor more specific information see the provenance information of the artifact version of interest.\n\n## References\n\n - [ 1]  Frey J, Hofer M. Hellmann S, Obraczka D, *DBpedia FlexiFusion Best of Wikipedia > Wikidata > Your Data*. ISWC Ressource Track 2019 (submitted). Available at: https://svn.aksw.org/papers/2019/ISWC_FlexiFusion/public.pdf\n\n# Changelog\n## 2019.03.01\n* first official release containing up to 140 DBpedia chapters and Wikidata extraction\n* changed JSON-LD provenance format\n\t* removed *selected for fusion* tag\n\t* added iHash to source information\n* introduced new artifacts structure\n* switched to new HTTPS Global IDs\n\n## 2018.10.01initeval\n* initial prototype for evaluation containing 5 DBpedia's + Wikidata" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia PreFusion instance types"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#instance-types_sources=dbpw_median.tsv.bz2> , <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#instance-types_sources=dbpw.jsonld.bz2> , <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#instance-types_sources=dbpw_context.jsonld> .
    
    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#instance-types_sources=dbpw_median.tsv.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia PreFusion instance types"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia PreFusion instance types"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "dbpw" , "median" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "tsv" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "0c02190c67edd230f40fba171787170a75db0845dc9bbc3343793e8bb109afd9" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "T2a82TT43f0yA/yL2eqmXnPwiAQGtrxY9UuArQflFxtIz8lEYKdHD73uE+lq4ayLcr93hB8Vei4JaFnrR+mvrfKOL/ppAPg4RfXnQy9AF719pcS1UDxjlzGjTwm/WZtwdvw4Sf6VzBV7mPGLwy29L3I0/B0ohwS9jehdr4KR6/DpceOS0HdKXqQkODyfGl+rXt8OVvJKMqZ90ykn33hOiS2SmHfgiD8T5QqZYGWPnBUbjs1BMXRySWzTTvGvv1z5iMLma7nJFRyPdlO6R5n2ag0MsiG7BB+JGodyAzSp5ECVKcDyWSa3w8IRcwmlGQNJYy54GJQhn4APhaAUXC+F2Q==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    true ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#sources>
                    "dbpw" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "median" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-04-16T13:34:21Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia PreFusion instance types"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "101"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/instance-types_sources=dbpw_median.tsv.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#TextTabSeparatedValues> .
    
    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#instance-types_sources=dbpw.jsonld.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia PreFusion instance types"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia PreFusion instance types"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "dbpw" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "197163"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/12BRtW\"},\"objects\":[{\"object\":{\"@id\":\"https://global.dbpedia.org/id/1xhds\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"f633d\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Gene\"},\"source\":[{\"@id\":\"d1:.ttl.bz2\",\"iHash\":\"f633d\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"f633d\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Biomolecule\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"f633d\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4tpyy\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"f633d\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"e9646ffed51bb90822d5c1f8337efd9dd8a7ac70def126de5359cc2a2ab855c2\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/3S7j3\"},\"objects\":[{\"object\":{\"@id\":\"http://dbpedia.org/ontology/TopicalConcept\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55ada\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/12PHk5\"},\"source\":[{\"@id\":\"d0:transitive_lang=pt.ttl.bz2\",\"iHash\":\"b206b\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Mollusca\"},\"source\":[{\"@id\":\"d0:lang=pt.ttl.bz2\",\"iHash\":\"b206b\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/e3g7\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55ada\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Taxon\"},\"source\":[{\"@id\":\"d1:.ttl.bz2\",\"iHash\":\"55ada\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55ada\"},{\"@id\":\"d0:transitive_lang=nl.ttl.bz2\",\"iHash\":\"a44b4\"},{\"@id\":\"d0:transitive_lang=pt.ttl.bz2\",\"iHash\":\"b206b\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4u7gX\"},\"source\":[{\"@id\":\"d0:transitive_lang=pt.ttl.bz2\",\"iHash\":\"b206b\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Animal\"},\"source\":[{\"@id\":\"d0:transitive_lang=pt.ttl.bz2\",\"iHash\":\"b206b\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Species\"},\"source\":[{\"@id\":\"d0:lang=nl.ttl.bz2\",\"iHash\":\"a44b4\"},{\"@id\":\"d0:transitive_lang=pt.ttl.bz2\",\"iHash\":\"b206b\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Eukaryote\"},\"source\":[{\"@id\":\"d0:transitive_lang=pt.ttl.bz2\",\"iHash\":\"b206b\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/1pwds\"},\"source\":[{\"@id\":\"d0:transitive_lang=pt.ttl.bz2\",\"iHash\":\"b206b\"}]},{\"object\":{\"@id\":\"http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#Concept\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55ada\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"89f62f2e5878aa59cca7ec7911da3900115b244609b9ed37c67a2e98be346073\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/PZ6s\"},\"objects\":[{\"object\":{\"@id\":\"http://dbpedia.org/ontology/TopicalConcept\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"18a83\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/e3g7\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"18a83\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Taxon\"},\"source\":[{\"@id\":\"d1:.ttl.bz2\",\"iHash\":\"18a83\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Insect\"},\"source\":[{\"@id\":\"d0:lang=nl.ttl.bz2\",\"iHash\":\"040c7\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"18a83\"},{\"@id\":\"d0:transitive_lang=sv.ttl.bz2\",\"iHash\":\"69a17\"},{\"@id\":\"d0:transitive_lang=nl.ttl.bz2\",\"iHash\":\"040c7\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4u7gX\"},\"source\":[{\"@id\":\"d0:transitive_lang=nl.ttl.bz2\",\"iHash\":\"040c7\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Animal\"},\"source\":[{\"@id\":\"d0:transitive_lang=nl.ttl.bz2\",\"iHash\":\"040c7\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Species\"},\"source\":[{\"@id\":\"d0:transitive_lang=nl.ttl.bz2\",\"iHash\":\"040c7\"},{\"@id\":\"d0:lang=sv.ttl.bz2\",\"iHash\":\"69a17\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Eukaryote\"},\"source\":[{\"@id\":\"d0:transitive_lang=nl.ttl.bz2\",\"iHash\":\"040c7\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/Q7JF\"},\"source\":[{\"@id\":\"d0:transitive_lang=nl.ttl.bz2\",\"iHash\":\"040c7\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/1pwds\"},\"source\":[{\"@id\":\"d0:transitive_lang=nl.ttl.bz2\",\"iHash\":\"040c7\"}]},{\"object\":{\"@id\":\"http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#Concept\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"18a83\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"52bec37a9fbc7255ebe5ccd2a50fab31b3bb31636844e9feb4015bc30f121e05\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/2ZjxX\"},\"objects\":[{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Place\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"17b94\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/PopulatedPlace\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"17b94\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"17b94\"}]},{\"object\":{\"@id\":\"http://schema.org/Place\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"17b94\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Settlement\"},\"source\":[{\"@id\":\"d1:.ttl.bz2\",\"iHash\":\"17b94\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4WQgG\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"17b94\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Location\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"17b94\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"7856d41605827db311e3ca3a1174a56e37491945e65153f7e592239b57284fec\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/2VyNR\"},\"objects\":[{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Mountain\"},\"source\":[{\"@id\":\"d1:.ttl.bz2\",\"iHash\":\"87e5f\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Place\"},\"source\":[{\"@id\":\"d0:lang=sv.ttl.bz2\",\"iHash\":\"9fb2d\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"87e5f\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4zxWn\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"87e5f\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d0:transitive_lang=sv.ttl.bz2\",\"iHash\":\"9fb2d\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"87e5f\"}]},{\"object\":{\"@id\":\"http://schema.org/Place\"},\"source\":[{\"@id\":\"d0:transitive_lang=sv.ttl.bz2\",\"iHash\":\"9fb2d\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"87e5f\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/NaturalPlace\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"87e5f\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Location\"},\"source\":[{\"@id\":\"d0:transitive_lang=sv.ttl.bz2\",\"iHash\":\"9fb2d\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"87e5f\"}]},{\"object\":{\"@id\":\"http://schema.org/Mountain\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"87e5f\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"8080a54cd6cd69d8521b4cbf0dcf48e23b17f3a1eb082a91fd27fc766d8ff56a\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"http://fr.dbpedia.org/resource/Hlfingen\"},\"objects\":[{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Place\"},\"source\":[{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"70727\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/PopulatedPlace\"},\"source\":[{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"70727\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"70727\"}]},{\"object\":{\"@id\":\"http://schema.org/Place\"},\"source\":[{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"70727\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Settlement\"},\"source\":[{\"@id\":\"d0:lang=fr.ttl.bz2\",\"iHash\":\"70727\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4WQgG\"},\"source\":[{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"70727\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Location\"},\"source\":[{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"70727\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"938f91691948afa96021970e7218263461c9e07095d9563d6b5d6ba36024c5e6\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/1kgXp\"},\"objects\":[{\"object\":{\"@id\":\"http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#SocialPerson\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"},{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"},{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4UMtT\"},\"source\":[{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"},{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"}]},{\"object\":{\"@id\":\"http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#Agent\"},\"source\":[{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"},{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/12HY1p\"},\"source\":[{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"},{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"},{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Agent\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"},{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"},{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Organisation\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"},{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"},{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"}]},{\"object\":{\"@id\":\"http://schema.org/Organization\"},\"source\":[{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"},{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4148A\"},\"source\":[{\"@id\":\"d0:transitive_lang=de.ttl.bz2\",\"iHash\":\"7bad5\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"55e2e\"},{\"@id\":\"d0:transitive_lang=fr.ttl.bz2\",\"iHash\":\"6f371\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Company\"},\"source\":[{\"@id\":\"d0:lang=de.ttl.bz2\",\"iHash\":\"7bad5\"},{\"@id\":\"d0:lang=fr.ttl.bz2\",\"iHash\":\"6f371\"},{\"@id\":\"d1:.ttl.bz2\",\"iHash\":\"55e2e\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"610dedb7d320ddcf1c4705aeebb11229b2c7e3eac372b581d74cc2eecd6b06d3\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/12BFX1\"},\"objects\":[{\"object\":{\"@id\":\"http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#SocialPerson\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4UMtT\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#Agent\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/12HY1p\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Agent\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Organisation\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"http://schema.org/Organization\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4148A\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Company\"},\"source\":[{\"@id\":\"d0:lang=en.ttl.bz2\",\"iHash\":\"5223b\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"a81425f1e57f2ce64bf522eb6ccd48561655b9265aaafbcd6dfe00ed37dd9ca0\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/129bdE\"},\"objects\":[{\"object\":{\"@id\":\"https://global.dbpedia.org/id/B4ph\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"81521\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Unknown\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"81521\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/WikimediaTemplate\"},\"source\":[{\"@id\":\"d1:.ttl.bz2\",\"iHash\":\"81521\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"81521\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"11520438dd2768cef1e649f102fbc73cd413deaf7ae55332dc761dc4bc6b9b2a\"}\n{\"predicate\":{\"@id\":\"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/3MRWR\"},\"objects\":[{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Person\"},\"source\":[{\"@id\":\"d1:.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"},{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/123RHp\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"},{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"}]},{\"object\":{\"@id\":\"http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#Agent\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"},{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"}]},{\"object\":{\"@id\":\"http://schema.org/Person\"},\"source\":[{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/12HY1p\"},\"source\":[{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/4dTEM\"},\"source\":[{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"},{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"}]},{\"object\":{\"@id\":\"http://www.w3.org/2002/07/owl#Thing\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Agent\"},\"source\":[{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"},{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"}]},{\"object\":{\"@id\":\"https://global.dbpedia.org/id/55jcx\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"}]},{\"object\":{\"@id\":\"http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#NaturalPerson\"},\"source\":[{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"},{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/Athlete\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d0:lang=es.ttl.bz2\",\"iHash\":\"b734b\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"}]},{\"object\":{\"@id\":\"http://dbpedia.org/ontology/SoccerPlayer\"},\"source\":[{\"@id\":\"d0:lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d0:lang=it.ttl.bz2\",\"iHash\":\"245b7\"}]},{\"object\":{\"@id\":\"http://xmlns.com/foaf/0.1/Person\"},\"source\":[{\"@id\":\"d0:transitive_lang=en.ttl.bz2\",\"iHash\":\"44c96\"},{\"@id\":\"d0:transitive_lang=es.ttl.bz2\",\"iHash\":\"b734b\"},{\"@id\":\"d0:transitive_lang=it.ttl.bz2\",\"iHash\":\"245b7\"},{\"@id\":\"d1:transitive.ttl.bz2\",\"iHash\":\"270bd\"}]}],\"@context\":\"instance-types_sources=dbpw_context.jsonld\",\"@id\":\"4c646be2ea73847d386de52ec80e3495b647366bc2388d6bf8dd9dcc538f7659\"}" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "5c038aa569756e2e776e13740b4cccf4da0c7fc462a5163fe63dd75cf6c7587f" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "daM+tQ+D44LD8T9fQnQ+SXt6G0Ys0AJbgMLhpHYdlRg+Fx+QMxisVsYBuMD+9MHHwpDcVGjfolniGJ/AoOSrf9yyhS0VFKnKXrEOPBzTLyX+sXd9J7XqLIql/CfByS0wlXbpyTYk1qCblF89a8hIHAWKsMNneFB7j1YO6VLQkAOOiGWzH2yCCnsyczv3eJPZlRlVHa6u0chhsCTQrCACT4LYECZMm9/LfkWzeqLQJv7l6vGzP2IxmmS7wvbQJ6zW28L1KPRNqZQtg5MqKiZW41azxzQokHHMVyjMgngdPfyUp6O1ecb701vq/n1Sq5vqeO0wC5X71dxNJdoflWRF1A==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "277395877"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#sources>
                    "dbpw" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-04-16T13:34:21Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia PreFusion instance types"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "2221319496"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/instance-types_sources=dbpw.jsonld.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#UNKNOWN> .
    
    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#instance-types_sources=dbpw_context.jsonld>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia PreFusion instance types"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia PreFusion instance types"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "context" , "dbpw" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "1"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "{\"@context\":{\"@base\":\"http://dbpedia.org/fusion/\",\"@vocab\":\"http://dbpedia.org/fusion/\",\"d0\":\"https://databus.dbpedia.org/dbpedia/mappings/instance-types/2018.12.01/\",\"d1\":\"https://databus.dbpedia.org/dbpedia/wikidata/instance-types/2018.11.30/\"}}" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "0375e32ce0c70ffa5bd7c4b3dee8d3d81771e8a0d2eadf98f1ce015fcb209a4d" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "XdOSdw5Zo4jyzCFfbX7m3xZaXjj3NazMkhLOfBEIazWvYj21UebJYqCAWa0KOZO3e/1Ue8cYMg+uQ94krCDtSX0BX48EPLHNFFCveJuDryOeHbZQA3WYQqMmD9Wrsn/YzZsT71Cu5gF+jl1TBV2xU0Y283AUGtoxO7ME7UstgohTg7+6HhI1KkQYRJtARYQCtOys3jM9Adc0Qo9n3w05Zd8xWko7tYjw7ClVbigpHmbKOlk6+iPGJuHglEsO1WLhtiw93igxDIg+WnOEZj8IH7dHk1FDlrUtjKaUrGsezxxdgro+1QgShO62m5i9TWgrAHiCMhpYlj90a0mOm2Xe0A==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    true ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "248"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#sources>
                    "dbpw" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "context" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-04-16T13:34:21Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia PreFusion instance types"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "247"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/instance-types/2019.03.01/instance-types_sources=dbpw_context.jsonld> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#UNKNOWN> .
}
