<https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/dbpedia/prefusion>
            a       <http://dataid.dbpedia.org/ns/core#Group> ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Aggregated data from other groups in prefusion state" .
    
    <https://webid.dbpedia.org/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/dbpedia> .
    
    <https://vehnem.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/marvin> .
    
    <https://databus.dbpedia.org/dbpedia/prefusion/specific-mappingbased-properties>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/dbpedia> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/dbpedia/prefusion/specific-mappingbased-properties> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/dbpedia/prefusion> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/dbpedia/prefusion/specific-mappingbased-properties/2019.03.01> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "\n\n# Group Documentation\n# PreFusion Dataset\nThe DBpedia PreFusion dataset is a new addition to the modular DBpedia releases combining DBpedia data from over 140 Wikipedia language editions and Wikidata. As an intermediate step in the FlexiFusion workflow [1], a global and unified preFused view is provided on the essence of DBpedia releases (see origin section).\nThe facts are harvested as RDF triples and aggregated using a new serialization format to track statement-level provenance.\nUnified access to knowledge from different sources is achieved by exploiting previously existing mappings of the DBpedia Ontology as well as merged, normalized entity identifiers (DBpedia Global IDs).\nThe ontology defines a comprehensive class hierarchy and properties, which are modelling common entities described in Wikipedia and Wikidata, and also reuses prominent vocabularies like FOAF and PROV. The dataset offers knowledge about very broad domains (like persons, organizations) but also for very specific domains (e.g. nutrition facts or animal classifications).\n\n## Description\nThe PreFusion dataset is stored as JSON-LD using a custom scheme optimized for an efficient representation of entities with overlapping object values and statement-level provenance. Thus, the dataset can be loaded both into JSON document/NoSQL stores, in case simple lookups are required – and triple stores – in case joins are required. Each PreFusion document describes a normalized subject-predicate pair (*SP−pair*) to aggregate all different object and literal values from the input sources as shown in the example below. The provenance record(s) are referencing the corresponding input file(s) of the object value and iHash value(s) which can be used to determine the original (non-normalized) IRI(s) of the triple(s) with hashing the result of the Global ID Resolution service. For more details see [1]\n\n    {\n      \"@id\": \"9336c15aed0392448b9cdb9d46b8f82e9942\",\n      \"@context\": \"https://databus.dbpedia.org/dbpedia/fusion/ fused-geo-coordinates/2018.11.25/fusion=prov-context.jsonld\",\n      \"subject\": {\n        \"@id\": \"https://global.dbpedia.org/id/J1kSq\" },\n      \"predicate\": {\n        \"@id\": \"http://www.w3.org/2003/01/geo/wgs84_pos#long\" },\n      \"objects\": [\n        {\n          \"object\": {\n            \"@value\": \"-2.3094\",\n            \"@type\": \"http://www.w3.org/2001/XMLSchema#float\" },\n          \"source\": [ {\n              \"@id\": \"d0:lang=commonswiki\",\n              \"iHash\": \"44473\" } ]\n        }\n      ]\n    }\n\n## Attribution fulfilled by\n* (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/prefusion/specific-mappingbased-properties/2019.03.01\n* on your website:\n  * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia\n  * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin\n* in academic publications cite [1]\n\n## How to contribute\nDBpedia is a community project, help us with:\n\n - Improving the input datasets (see docu for DBpedia and Wikidata extractions in origin section)\n - upload datasets using DBpedia ontology and providing sameAs links to DBpedia entities on the databus and contact the maintainer so that it can be included.\n\n## Origin\nThis dataset is based on\nDBpedia extractions\n - https://databus.dbpedia.org/dbpedia/mappings\n - https://databus.dbpedia.org/dbpedia/generic\n\nWikidata extractions\n - https://databus.dbpedia.org/dbpedia/wikidata\n\nFor more specific information see the provenance information of the artifact version of interest.\n\n## References\n\n - [ 1]  Frey J, Hofer M. Hellmann S, Obraczka D, *DBpedia FlexiFusion Best of Wikipedia > Wikidata > Your Data*. ISWC Ressource Track 2019 (submitted). Available at: https://svn.aksw.org/papers/2019/ISWC_FlexiFusion/public.pdf\n\n# Changelog\n## 2019.03.01\n* first official release containing up to 140 DBpedia chapters and Wikidata extraction\n* changed JSON-LD provenance format\n\t* removed *selected for fusion* tag\n\t* added iHash to source information\n* introduced new artifacts structure\n* switched to new HTTPS Global IDs\n\n## 2018.10.01initeval\n* initial prototype for evaluation containing 5 DBpedia's + Wikidata" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#specific-mappingbased-properties_sources=dbpw_median.tsv.bz2> , <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#specific-mappingbased-properties_sources=dbpw.jsonld.bz2> , <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#specific-mappingbased-properties_sources=dbpw_context.jsonld> .
    
    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#specific-mappingbased-properties_sources=dbpw_median.tsv.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "dbpw" , "median" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "tsv" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "8c07f977567463f3875b80dd4559ed4c6b6f4cecbacac77c8453fd2dd2cacc00" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "rMXwsaB+UJ1o2jU91pur+ZgK6ESG07JV0nf3U3rIKpTje+OjB1J2fClPq1NYFREfaj1mLSx3O+Q3EJsx7p0CPc23QEYoud1nbExWFQjgwldXHP1F8PqSadkiiSyAE7r8IBd881baQONddW2oafIsfKOWxblu+754abmPfq83QUOCny8fOf0yiWvB41IfCeHXd8+NfiEpZk1HiVLdZuhZg7AX/HmUFpVnAKX/7GbKzajrC8j9Dmt3cSr1pBHMWoYwYMyK8T0YWCvYjMRgsZbQApPrBbnCGi4IOKCPxoRgD4ly3bLV1Da8VP4Oz8Fca48mdMSt8Ulla8TL1oO/VQ07UA==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    true ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#sources>
                    "dbpw" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "median" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-04-16T13:36:59Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "1771"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/specific-mappingbased-properties_sources=dbpw_median.tsv.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#TextTabSeparatedValues> .
    
    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#specific-mappingbased-properties_sources=dbpw.jsonld.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "dbpw" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "12303"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/PopulatedPlace/areaTotal\"},\"subject\":{\"@id\":\"http://pl.dbpedia.org/resource/Étrຜhy_(Marna)\"},\"objects\":[{\"object\":{\"@value\":\"6.7\",\"@type\":\"http://dbpedia.org/datatype/squareKilometre\"},\"source\":[{\"@id\":\"d0:lang=pl.ttl.bz2\",\"iHash\":\"7bbb9\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"cb022cefae51590360145e70ccc0bc411c415ad665322369e78edf365b7f9953\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/Planet/apoapsis\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/84xG\"},\"objects\":[{\"object\":{\"@value\":\"4.1980154475834E8\",\"@type\":\"http://dbpedia.org/datatype/kilometre\"},\"source\":[{\"@id\":\"d0:lang=pl.ttl.bz2\",\"iHash\":\"0b1e4\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"7f426092f0fb49ec49c14ecdf3dc9c24feb6b19bcf4d7e59c18b49bb5ab05aa0\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/Person/height\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/4tRQe\"},\"objects\":[{\"object\":{\"@value\":\"186.0\",\"@type\":\"http://dbpedia.org/datatype/centimetre\"},\"source\":[{\"@id\":\"d0:lang=en.ttl.bz2\",\"iHash\":\"24f23\"},{\"@id\":\"d0:lang=nl.ttl.bz2\",\"iHash\":\"adcb9\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"29cefb27c8d2a50d1fa05f013e6387e99cd96b13dcb3305bee1d1eb87a574ebe\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/Work/runtime\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/12U2r1\"},\"objects\":[{\"object\":{\"@value\":\"4.0\",\"@type\":\"http://dbpedia.org/datatype/minute\"},\"source\":[{\"@id\":\"d0:lang=es.ttl.bz2\",\"iHash\":\"ea417\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"c9c3aca0f30a2d3c94601fe846c49934144aa10cae9381ccf7a73e71db696498\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/Person/height\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/33XN5\"},\"objects\":[{\"object\":{\"@value\":\"181.0\",\"@type\":\"http://dbpedia.org/datatype/centimetre\"},\"source\":[{\"@id\":\"d0:lang=pl.ttl.bz2\",\"iHash\":\"afd9b\"},{\"@id\":\"d0:lang=en.ttl.bz2\",\"iHash\":\"91d40\"},{\"@id\":\"d0:lang=gl.ttl.bz2\",\"iHash\":\"89a4c\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"f24d4c4663705284b3c2aa4c0d954e2eea31d17e621ea5d54d213fbdeb9873d7\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/PopulatedPlace/area\"},\"subject\":{\"@id\":\"http://pt.dbpedia.org/resource/Gan๺ria\"},\"objects\":[{\"object\":{\"@value\":\"4.0\",\"@type\":\"http://dbpedia.org/datatype/squareKilometre\"},\"source\":[{\"@id\":\"d0:lang=pt.ttl.bz2\",\"iHash\":\"4a025\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"aed569b9c13f6712ff26f133927ad914c041b7e42f89c12c12399a3f03e3ab3f\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/PopulatedPlace/areaTotal\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/MpjH\"},\"objects\":[{\"object\":{\"@value\":\"29.0\",\"@type\":\"http://dbpedia.org/datatype/squareKilometre\"},\"source\":[{\"@id\":\"d0:lang=de.ttl.bz2\",\"iHash\":\"f1adf\"}]},{\"object\":{\"@value\":\"29.68\",\"@type\":\"http://dbpedia.org/datatype/squareKilometre\"},\"source\":[{\"@id\":\"d0:lang=en.ttl.bz2\",\"iHash\":\"91a55\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"fb4d3610a3762178f264228b45cd35d997071c5d450f9fa6c309408764b12ed6\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/PopulatedPlace/areaTotal\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/GSYW\"},\"objects\":[{\"object\":{\"@value\":\"1.9\",\"@type\":\"http://dbpedia.org/datatype/squareKilometre\"},\"source\":[{\"@id\":\"d0:lang=gl.ttl.bz2\",\"iHash\":\"b46a4\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"024710a7d11488591e064841e5eb9412138a6587fa5c16789beb81640aece8f1\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/PopulatedPlace/areaTotal\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/CmS1\"},\"objects\":[{\"object\":{\"@value\":\"0.849\",\"@type\":\"http://dbpedia.org/datatype/squareKilometre\"},\"source\":[{\"@id\":\"d0:lang=ja.ttl.bz2\",\"iHash\":\"5eb2c\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"d7e4ad35101f83dfc38662eba66cbc1d3c7f14355712a1632e5f332a5c64ce18\"}\n{\"predicate\":{\"@id\":\"http://dbpedia.org/ontology/Person/height\"},\"subject\":{\"@id\":\"https://global.dbpedia.org/id/fXWL\"},\"objects\":[{\"object\":{\"@value\":\"191.0\",\"@type\":\"http://dbpedia.org/datatype/centimetre\"},\"source\":[{\"@id\":\"d0:lang=pl.ttl.bz2\",\"iHash\":\"01fc4\"}]}],\"@context\":\"specific-mappingbased-properties_sources=dbpw_context.jsonld\",\"@id\":\"c45fa92308fe72d5d1cc1866dbd063918b685ebf87d056095379e5430ff602d4\"}" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "c53c44c4ce9ead3679625939c4400641ca0bfed3f564c99119e5031795fff1f6" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "h5pBc0TFrdCih/vsblskih5CqQy07sNp6fdqAewItlAEqvekixtnApZ+HkpFnbbu6fW5W1N0oAwu3WlIei461Hbv86YFYfK9ySfH2k6sZf2CLqjO8DAQ4melKwOs0i7O4j7s2ZsD1uM9zkmMnYXMRSQtwdW9iMpNZ9QYuRsWltLbMxeLo80ty2bgqh+niFnlc+RYO6XdabVtNLRcsdTO9rHl8oFG/6qviUEW8f5OOmNA3N4W3ZIR+UAdZxpB8JWjirWWpWyz0/R0eE5CkfJ624sP2KF7B8KGaJGAgdhtzv/epeQmEXw0ZP6XRTPvEBJ7ZtRCn0+7bkSsqzI3rXa1Lg==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "6080479"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#sources>
                    "dbpw" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-04-16T13:36:59Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "84024133"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/specific-mappingbased-properties_sources=dbpw.jsonld.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#UNKNOWN> .
    
    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#specific-mappingbased-properties_sources=dbpw_context.jsonld>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "context" , "dbpw" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://vehnem.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "1"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "{\"@context\":{\"@base\":\"http://dbpedia.org/fusion/\",\"@vocab\":\"http://dbpedia.org/fusion/\",\"d0\":\"https://databus.dbpedia.org/dbpedia/mappings/specific-mappingbased-properties/2018.12.01/\"}}" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "777e647d1fa9f2f2020522fa98c326728af5fabc307f3db8ff5070daad0c4b09" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "pW48a0h9iwfN9KsiA2owNFSI8fZeosU2haBi7PyavUTT5dxHTu8AIRQfR+OunpfF1+y/KR9bQA3D12UuNcU0fLZNy3HVf+6IIvt2GjTP/RDkLozZv7KUabxExASaDjMTxHdT+b6kpGJEd2GHiTcqu9SWCXYPcyKXOVU8iE7H7Ux0edn5MlcBbBfzhly8aiE6MlWG23EgRtnUeDdZEnBx/kSybj6hL727KkSydXoodggt4XmQwiSH4X/ssqHDuZG8cmNVsl6eSHmDBRw1Rw66cFhjV1/e0zLjZGHwy9VnjOTdwEBkTpb7jtUmovxpRdSGXIpqPNDlTzn0vJ0wcxKe1w==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    true ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "187"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/cv#sources>
                    "dbpw" ;
            <http://dataid.dbpedia.org/ns/cv#tag>
                    "context" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2019.03.01" ;
            <http://purl.org/dc/terms/issued>
                    "2019-03-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2019-04-16T13:36:59Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://webid.dbpedia.org/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "DBpedia PreFusion specific mappingbased properties"@en ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "186"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <https://downloads.dbpedia.org/repo/lts/prefusion/specific-mappingbased-properties/2019.03.01/specific-mappingbased-properties_sources=dbpw_context.jsonld> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#UNKNOWN> .
    
    <https://databus.dbpedia.org/dbpedia/prefusion/specific-mappingbased-properties/2019.03.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
}
