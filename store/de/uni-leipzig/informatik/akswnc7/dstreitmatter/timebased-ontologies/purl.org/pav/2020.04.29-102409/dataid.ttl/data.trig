<http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#Dataset> {
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "This ontology is part of the Databus Archivo - A Web-Scale OntologyInterface for Time-Based and SemanticArchiving and Developing Good Ontologies"@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "Provenance, Authoring and Versioning (PAV)"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/ontologies> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/ontologies/purl.org/pav> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/ontologies/purl.org> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "#This group is for all vocabularies hosted on purl.org\n\nAll the artifacts in this group refer to one vocabulary, deployed in different formats.\nThe ontologies are part of the Databus Archivo - A Web-Scale Ontology Interface for Time-Based and Semantic Archiving and Developing Good Ontologies." ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/ontologies/purl.org/pav/2020.04.29-102409> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "PAV supplies terms for distinguishing between the different roles of the agents contributing content in current web based systems: contributors, authors, curators and digital artifact creators. The ontology also provides terms for tracking provenance of digital entities that are published on the web and then accessed, transformed and consumed. In order to support broader interoperability, PAV specializes the general purpose W3C PROV provenance model (PROV-O).\n\nPAV distinguishes between the data related to the digital artifact - named Provenance - and those related to the actual knowledge creation and therefore to the intellectual property aspects – named Authoring. The Versioning axis describes the evolution of digital entities in time.\n\nUsing PAV, descriptions can define the authors that originate or gave existence to the work that is expressed in the digital resource (pav:authoredBy); curators (pav:curatedBy) who are content specialists responsible for shaping the expression in an appropriate format, and contributors (super-property pav:contributedBy) that provided some help in conceiving the resource or in the expressed knowledge creation/extraction.\n\nThese provenance aspects can be detailed with dates using pav:curatedOn, pav:authoredOn, etc. Further details about the creation activities, such as different authors contributing specific parts of the resource at different dates are out of scope for PAV and should be defined using vocabularies like PROV-O and additional intermediate entities to describe the different states.\n\nFor resources based on other resources, PAV allows specification of direct retrieval (pav:retrievedFrom), import through transformations (pav:importedFrom) and sources that were merely consulted (pav:sourceAccessedAt). These aspects can also define the agents responsible using pav:retrievedBy, pav:importedBy and pav:sourceAccessedBy.\n\nVersion number of a resource can be given with pav:version, the previous version of the resource with pav:previousVersion, and any other earlier versions with pav:hasEarlierVersion. Unversioned, 'mutable' resources can specify their current version as a snapshot resource using pav:hasCurrentVersion and list the earlier versions using pav:hasVersion.\n\nThe creation of the digital representation (e.g. an RDF graph or a .docx file) can in many cases be different from the authorship of the content/knowledge, and in PAV this digital creation is specified using pav:createdBy, pav:createdWith and pav:createdOn.\n\nPAV specializes terms from W3C PROV-O (prov:) and DC Terms (dcterms:), however these ontologies are not OWL imported as PAV can be used independently. The \"is defined by\" links indicate where those terms are included from. See http://www.w3.org/TR/prov-o and http://dublincore.org/documents/2012/06/14/dcmi-terms/ for more details. See http://purl.org/pav/mapping/dcterms For a comprehensive SKOS mapping to DC Terms.\n\nPAV 2 is based on PAV 1.2 but in a different namespace ( http://purl.org/pav/ ). Terms compatible with 1.2 are indicated in this ontology using owl:equivalentProperty.\n\nThe ontology IRI http://purl.org/pav/ always resolve to the latest version of PAV 2. Particular versionIRIs such as http://purl.org/pav/2.1 can be used by clients to force imports of a particular version - note however that all terms are defined directly in the http://purl.org/pav/ namespace.\n\nThe goal of PAV is to provide a lightweight, straight forward way to give the essential information about authorship, provenance and versioning, and therefore these properties are described directly on the published resource. As such, PAV does not define any classes or restrict domain/ranges, as all properties are applicable to any online resource.\n\n--\n\nCopyright 2008-2014 Massachusetts General Hospital; Harvard Medical School; Balboa Systems; University of Manchester\n\nLicensed under the Apache License, Version 2.0 (the \"License\"); you may not use this file except in compliance with the License.  You may obtain a copy of the License at\n\n    http://www.apache.org/licenses/LICENSE-2.0\n\nUnless required by applicable law or agreed to in writing, software distributed under the License is distributed on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License." ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.29-102409" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-29T14:31:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://www.apache.org/licenses/LICENSE-2.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://purl.org/dc/terms/title>
                    "Provenance, Authoring and Versioning (PAV)"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=parsed.nt> , <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=generatedDocu.html> , <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=parsed.ttl> , <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=orig.rdf> , <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=meta.json> , <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=shaclReport.ttl> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=parsed.nt>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "parsed" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/ontologies/purl.org/pav/2020.04.29-102409/pav_type=parsed.nt> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://purl.org/pav/> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology> .\n<http://purl.org/pav/> <http://www.w3.org/2000/01/rdf-schema#label> \"Provenance, Authoring and Versioning (PAV)\"@en .\n<http://purl.org/pav/> <http://www.w3.org/2002/07/owl#versionInfo> \"2.3.1\"^^<http://www.w3.org/2001/XMLSchema#string> .\n<http://purl.org/pav/> <http://purl.org/dc/terms/modified> \"2014-08-28T14:41:00Z\"^^<http://www.w3.org/2001/XMLSchema#dateTime> .\n<http://purl.org/pav/> <http://purl.org/dc/terms/issued> \"2014-08-28T15:00:00Z\"^^<http://www.w3.org/2001/XMLSchema#dateTime> .\n<http://purl.org/pav/> <http://purl.org/dc/elements/1.1/creator> \"Paolo Ciccarese\"^^<http://www.w3.org/2001/XMLSchema#string> .\n<http://purl.org/pav/> <http://purl.org/dc/elements/1.1/creator> \"Stian Soiland-Reyes\"^^<http://www.w3.org/2001/XMLSchema#string> .\n<http://purl.org/pav/> <http://purl.org/dc/elements/1.1/contributor> \"Marco Ocana\"^^<http://www.w3.org/2001/XMLSchema#string> .\n<http://purl.org/pav/> <http://purl.org/dc/elements/1.1/contributor> \"Alasdair J G Gray\"^^<http://www.w3.org/2001/XMLSchema#string> .\n<http://purl.org/pav/> <http://purl.org/dc/elements/1.1/contributor> \"Khalid Belhajjame\"^^<http://www.w3.org/2001/XMLSchema#string> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "26c68f418c51851728e04f151e08665566cbb12d10201565acb1b2458c7be8c9" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "SUDNwMK0D7KKdPAta+gNlbzgPUB1MmCiHRmzaWa7eEdBYhpXhAd6zfzozKCcUua+7qA/grxNhvdu1Gjn8EL/Zsm2Pt7gPl8jXL0p72/nHeRr7Lg6C4dE8LFPNNcFx9CAvdsGdf/xvS4TzVK62zgoLonVbufi3i78YfgaDKxmCJcwRJqZ+gszXkXU7EKXGuxgXSwE+xlJkK0S9Hx1+BHy/37qg/axpPglYDC7rB+VhYID60caGcIDNqlgFGxlvVxkBJD/75gGAFxQ3b+/0in4flBRH3cbNSvK3Iq6xwNA7mKFsrgVQs0GlVzPWQDTaSUgv6Z/ZMv1sq8kz+m71LODnQ==" ;
            <http://dataid.dbpedia.org/ns/cv#type>
                    "parsed" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.29-102409" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-29T14:31:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://www.apache.org/licenses/LICENSE-2.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-29T08:24:13Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "72563"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/pav_type=parsed.nt> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=generatedDocu.html>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "generatedDocu" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/ontologies/purl.org/pav/2020.04.29-102409/pav_type=generatedDocu.html> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><title>PAV - Provenance, Authoring and Versioning</title><title>Provenance, Authoring and Versioning (PAV)</title><title>PAV - Provenance, Authoring and Versioning</title><link href=\"http://150.146.207.114/lode/owl.css\" rel=\"stylesheet\" type=\"text/css\" /><link href=\"http://150.146.207.114/lode/Primer.css\" rel=\"stylesheet\" type=\"text/css\" /><link href=\"http://150.146.207.114/lode/rec.css\" rel=\"stylesheet\" type=\"text/css\" /><link href=\"http://150.146.207.114/lode/extra.css\" rel=\"stylesheet\" type=\"text/css\" /><link rel=\"shortcut icon\" href=\"http://150.146.207.114/lode/favicon.ico\" /><script src=\"http://150.146.207.114/lode/jquery.js\"></script><script src=\"http://150.146.207.114/lode/jquery.scrollTo.js\"></script><script src=\"http://150.146.207.114/lode/marked.min.js\"></script><script>\n                $(document).ready(\n                    function () {\n                    \tjQuery(\".markdown\").each(function(el){\n\t\t\t\t\t\t\tjQuery(this).after(marked(jQuery(this).text())).remove()});\n                        var list = $('a[name=\"http://purl.org/pav/\"]');\n                        if (list.size() != 0) {\n                        \tvar element = list.first();\n                        \t$.scrollTo(element);\n                        }" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "06d1e0752441e8a48faadc151846322e2658354867fa3ffd9acf11ea62ccbbf5" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "HTchZ9ymOkfz12t+zTKLSOjKKxVYngnea6e7lO8ytEiSNhLYPFJcC9/KB5T+b3ZdZoBA6t+e36p+GH2FHS1wovpENFxFQfqbu+vxOjGBEI3cCV07xgwkc9R/JFs6jUY01asl1mVJ4EKzuEwjrxau7SsFiA/HQyamL2a139oZ3q+jYs0DqWQKGNx/8v/4shMXbjMvu3Rf1TAR316FRJDGy+38UiVrZmT/8mAg1sl3T5vF6E5CgB4Ptgv6ZMXja3pz1Qc9NgxnaGF2Nf5EqvOpU7vDsG+3RqrUqTNHbflTeVqFal84n0TTKtzv5P4jE7VDeUimZSMijn+cl2USgAYi4g==" ;
            <http://dataid.dbpedia.org/ns/cv#type>
                    "generatedDocu" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.29-102409" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-29T14:31:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://www.apache.org/licenses/LICENSE-2.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-29T08:24:14Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "89720"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/pav_type=generatedDocu.html> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#UNKNOWN> .
    
    <https://databus.dbpedia.org/ontologies/purl.org/pav>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/ontologies/purl.org>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <https://yum-yab.github.io/webid.ttl#onto>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/ontologies> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=parsed.ttl>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "parsed" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/ontologies/purl.org/pav/2020.04.29-102409/pav_type=parsed.ttl> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "@base <http://purl.org/pav/> .\n@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n@prefix : <../pav> .\n@prefix dc: <../dc/elements/1.1/> .\n@prefix pav1: <http://swan.mindinformatics.org/ontologies/1.2/pav/> .\n@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n@prefix prov: <http://www.w3.org/ns/prov#> .\n@prefix foaf: <http://xmlns.com/foaf/0.1/> .\n@prefix pav: <> .\n@prefix owl: <http://www.w3.org/2002/07/owl#> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "98425a25b6b0cb16c3d010053590ee38b2258a00c26934afe33d9e8c2210e4b3" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "XGN6DFtB0DAqkBfxfpL2B9EhvdzMw9QuGC10PP/hPURR1mVOZ9xXiYQj9nhzRFAYp2wwZuRctfMa1njt87fD8CAHxsQTt117Mz2IOMExpFa11hcxpxSrVTq8rmdXmW0sz8lE9Rs4v1cKDtEK/Qng3r44JkhEEP9NhewafcmWv/WHmT94IIPi8yY+y6L/iNK2HpcLrx7Ddm18IUpxCTZZxXcjNHENlVINlHOCH2ACOeTF00RaCp0M5f8/9T1JvCe2P7dpkxgSqoHSzh+sMM/aFLq/tdVnKqI7VSfsKEUnGOMIKEVzNjA2N189dLAhEwQYd+eXgFqYuWSrToK7i99RhA==" ;
            <http://dataid.dbpedia.org/ns/cv#type>
                    "parsed" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.29-102409" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-29T14:31:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://www.apache.org/licenses/LICENSE-2.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-29T08:24:13Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "41949"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/pav_type=parsed.ttl> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#TextTurtle> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=orig.rdf>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "orig" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/ontologies/purl.org/pav/2020.04.29-102409/pav_type=orig.rdf> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "rdf" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<?xml version=\"1.0\"?>\n<?xml-stylesheet type=\"text/xsl\" href=\"owl2html.xslt.xml\"?>\n\n<!DOCTYPE rdf:RDF [\n    <!ENTITY pav \"http://purl.org/pav/\" >\n    <!ENTITY dcterms \"http://purl.org/dc/terms/\" >\n    <!ENTITY foaf \"http://xmlns.com/foaf/0.1/\" >\n    <!ENTITY prov \"http://www.w3.org/ns/prov#\" >\n    <!ENTITY owl \"http://www.w3.org/2002/07/owl#\" >\n    <!ENTITY dc \"http://purl.org/dc/elements/1.1/\" >" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "ed03d6a70836897b081be5d6347277ec668e948b993fc8e2d19e015d241db746" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "qs5QfHmHSOMXwnCD0bIGjGyW1lzgKzgvwG9EIkTh4Hy9mCVQeXbOm4xIigwniVUX7zD8DGH10tZ5HdGoEUfDp0h+s5RyJJHjZ2xb7IQfbJtqck3tFNM/NP1xQ3TKgyZCvcHyYL6mt5WxjghVC2jPpIt2AbnPYkg1kcUQSjrmJa+DrqBtPfp0aFirqjiU+l6IhqyHVux/hP9nDc+RcFUbBo2tkSgQzQZM5JyEDVcnxlk8DYruKIs39q8BOLdimc12tt6k3vpNkDN9T+CEC354UBxglDaJufyG1Sr3rrHzYpcZ+Fl5fb8GL0m6eD4ISWjfQSBEA4xq4DL4uE2drbuRjA==" ;
            <http://dataid.dbpedia.org/ns/cv#type>
                    "orig" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.29-102409" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-29T14:31:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://www.apache.org/licenses/LICENSE-2.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-29T08:24:13Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "58690"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/pav_type=orig.rdf> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationRDFXML> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=meta.json>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "meta" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/ontologies/purl.org/pav/2020.04.29-102409/pav_type=meta.json> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "json" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "{\n    \"E-Tag\": \"W/\\\"55b8ae6a-e541\\\"\",\n    \"NIR-header\": \"{'Server': 'nginx/1.16.1 (Ubuntu)', 'Date': 'Wed, 29 Apr 2020 08:24:12 GMT', 'Content-Type': 'text/html; charset=utf-8', 'Content-Length': '204', 'Connection': 'keep-alive', 'Location': 'http://pav-ontology.github.io/pav/pav.rdf', 'Access-Control-Allow-Origin': '*'}\",\n    \"SHACL-validated\": true,\n    \"accessed\": \"2020.04.29; 10:24:09\",\n    \"best-header\": \"application/rdf+xml\",\n    \"content-length\": \"10905\",\n    \"lastModified\": \"Wed, 29 Jul 2015 10:43:54 GMT\",\n    \"ontology-resource\": \"http://purl.org/pav/\",\n    \"rapperErrors\": \"\"," ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "a0577229b912b21f8c0bf33df10fb04d8aaf5dc26e53074aa29dcbc5550e6259" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "Dz2WrnJfND0M5ewcvn4lsTqWDF6f1gMnkp416y/4gwZqUmYJEclVkiujovP4e9jjJ7SxgWfyMxFcextGvSORdqb8XVr3IGAVv3EPMSEG3Ds7gxkvsS2IpBf+sFbiGklI1kYLpQ3EoDrxOlV83HrbVF87l1uRPaPGENwlsgH5kXH8UusuM1+b5mkvnLBiof8/2r1C7CHTjpSU4VIng9LiYNpiifwNz7Ip5Eqal+PBv281Ve36GosxiENkF5Vbti6Yx/0dPgOWQhHE0iPm4JAQ1WU94xsAYpq8sOii/L+02eBQL4tiv6SbTLH6trROgXfys8pTY5VE6VPrHKJot6O6cA==" ;
            <http://dataid.dbpedia.org/ns/cv#type>
                    "meta" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.29-102409" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-29T14:31:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://www.apache.org/licenses/LICENSE-2.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-29T08:24:13Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "1401"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/pav_type=meta.json> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationJson> .
    
    <https://databus.dbpedia.org/ontologies/purl.org/pav/2020.04.29-102409>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#pav_type=shaclReport.ttl>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#contentVariant>
                    "shaclReport" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/ontologies/purl.org/pav/2020.04.29-102409/pav_type=shaclReport.ttl> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "ttl" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "@prefix sh: <http://www.w3.org/ns/shacl#> .\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n\n[] a sh:ValidationReport ;\n    sh:conforms true .\n" ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "4a53a3befd9bcdb7273ef3d6e47a83be519927771faee49ef0d2a44ee23be2be" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "oWf/xaFuxIv8S0LrIXOT95hSn07VZtW6gPw/mfHSE0u89/i9IS7pxmn+ncHZsn7ddmWxua/vtZfD67qDYWDu1ZIVzl+/xqzedmvbFFsIF18SrbnNOh/LNyUYyyEjPjLygZdd6T5L2O2608EDnq8vLiqL0i8Hp9VVFbSK8DolAquN73J9xYhVJcmt59LIfabJhbFUxfu/33liZ6Dbh4gNoK627o4iVHyuaXp++zlrCJKiXhGb+7rcQz5efZgQR5y6u0W0z5ZrOd/OCIqU9wTteebxqPcJUoIxGxEIwTaag/YfTSyNErCMHXcgrybEP7zCPK8QuyBP/Dsan4Q/PF1/JA==" ;
            <http://dataid.dbpedia.org/ns/cv#type>
                    "shaclReport" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.04.29-102409" ;
            <http://purl.org/dc/terms/issued>
                    "2020-04-29T14:31:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://www.apache.org/licenses/LICENSE-2.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-04-29T08:24:13Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#onto> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "147"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/timebased-ontologies/purl.org/pav/2020.04.29-102409/pav_type=shaclReport.ttl> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
