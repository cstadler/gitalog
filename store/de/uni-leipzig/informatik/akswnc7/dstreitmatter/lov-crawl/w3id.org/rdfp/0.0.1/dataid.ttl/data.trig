<http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/rdfp/0.0.1/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/denis/w3id.org/rdfp>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/denis/w3id.org/rdfp/0.0.1>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/denis/w3id.org>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/rdfp/0.0.1/dataid.ttl#rdfp.nt>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/denis/w3id.org/rdfp/0.0.1/rdfp.nt> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/rdfp/0.0.1/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "115"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<https://w3id.org/rdfp/> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/vocommons/voaf#Vocabulary> .\n<https://w3id.org/rdfp/> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology> .\n<https://w3id.org/rdfp/> <http://purl.org/dc/terms/title> \"The RDF Presentation ontology\"@en .\n<https://w3id.org/rdfp/> <http://purl.org/dc/terms/description> \"## RDF Presentation and RDF Presentation Negotiation\\r\\n\\r\\n**To cite our work:**\\r\\n\\r\\n> Maxime Lefran\\u00E7ois _Interop\\u00E9rabilit\\u00E9 s\\u00E9mantique lib\\u00E9rale pour les services et les objets_, Actes de la 17\\u00E8me conf\\u00E9rence Extraction et Gestion des Connaissances, EGC, Jan 2017, Grenoble, France - ([PDF](http://www.maxime-lefrancois.info/docs/Lefrancois-EGC2017-Interoperabilite.pdf) - [BibTeX](Lefrancois-EGC2017-Interoperabilite.bib))\\r\\n\\r\\n\\r\\nAn RDF graph can be presented in several ways, using different media types. Examples of RDF media types include `application/rdf+xml`, `text/turtle`, `application/json+ld`.\\r\\n\\r\\nToday, most of the content consumed/produced/published, on the Web is not presented in RDF. \\r\\n\\r\\nIn the Web of Things, HTTP servers and clients would rather exchange lightweight documents, potentially binary. \\r\\nCurrently, most existing RDF Presentations generically apply to any RDF graph, at the cost of being heavy text-based documents.\\r\\nYet, lightweight HTTP servers/clients could be better satisfied with consuming/producing/publishing lightweight documents, may its structure be application-specific.\\r\\n\\r\\nOn the other hand, various formalisms have been developed:\\r\\n\\r\\n- to lift documents to RDF. Examples include [RML mappings](http://rml.io), [XSPARQL](http://xsparql.deri.org/), [SPARQL-Generate](http://w3id.org/sparql-generate/);\\r\\n- to lower RDF to documents. Examples include [XSPARQL](http://xsparql.deri.org/), [STTL, aka SPARQL-Template](https://ns.inria.fr/sparql-template/);\\r\\n- to validate RDF graphs. Examples include simple [SPIN](http://spinrdf.org/), [ShEx](http://shexspec.github.io), [SHACL](https://www.w3.org/TR/shacl/). \\r\\n\\r\\nFor a given range of RDF graphs and a specific media types, an RDF Presentation is a combination of lifting, lowering, and validation rules. With these rules, one can coherently interpret a representation as RDF (lift), validate the RDF graph, and generate back the representation from the RDF graph (lower).\\r\\n\\r\\nWhile sending any kind of document, potentially lightweight, an HTTP server/client may refer to the specific RDF Presentation that is used. Then, the HTTP client/server can lift the document to RDF, and validate it.\\r\\n\\r\\nSimilarly, while requesting for an RDF graph, an HTTP server/client may inform the client/server what representation it prefers. Then, the client/server can validate the RDF graph, then lower it into a document.\\r\\n\\r\\n### RDF Presentation description \\r\\n\\r\\nFollowing the Linked Data principles, RDF Presentations are given uniform identifiers (URIs), and an RDF description of these presentations can be retrieved at their URI.\\r\\n\\r\\nThe RDFP vocabulary can be used to describe RDF Presentations and the range of RDF graphs they apply to. It can be accessed at this URI: https://w3id.org/rdfp/ using content negotiation (i.e., set HTTP Header field `Accept` to one of the RDF syntaxes media types, or access it directly in [turtle](index.ttl), or in [RDF/XML](index.rdf).\\r\\n\\r\\nFor example, the RDF Presentation identified by https://w3id.org/rdfp/example/graph/xml 303 redirects to https://w3id.org/rdfp/example/description where it is described as follows:\\r\\n\\r\\n```\\r\\n@prefix rdfp: <https://w3id.org/rdfp/>.\\r\\n@base <https://w3id.org/rdfp/>.\\r\\n\\r\\n<example/graph/xml> a rdfp:Presentation ;\\r\\n  rdfp:mediaType \\\"application/xml\\\" ; \\r\\n  rdfp:liftingRule <example/graph/xml/liftingRule> ;\\r\\n  rdfp:loweringRule <example/graph/xml/loweringRule> ;\\r\\n  rdfs:isDefinedBy <example/description> .\\r\\n```\\r\\n\\r\\nA full example RDF graph that uses this vocabulary can be found at URI https://w3id.org/rdfp/example/description. Use content negotiation, or access it directly in [turtle](https://w3id.org/rdfp/example/description.ttl), or in [RDF/XML](https://w3id.org/rdfp/example/description.rdf).\\r\\n\\r\\n\\r\\nThe RDFP vocabulary describes web resources, RDF graphs, ontologies, and their representations.\\r\\n\\r\\n  The URI of a named graph is the identification of that graph in the sense of the W3C Architecture of the World Wide Web, Volume One.\\r\\n  The graph is the content, and that graph can be represented by many different documents that can then be exchanged on the Web.\\r\\n\\r\\n## Referring to an RDF Presentation\\r\\n\\r\\nThe RDF Presentation qualifies the representation type. Following the general architecture principles defined in [W3C Architecture of the World Wide Web, Volume One](https://www.w3.org/TR/2004/REC-webarch-20041215/), we keep orthogonal the identification and representation concepts. Arguably, the representation type (the media type) should be annotated with a link to the RDF Presentation used. \\r\\n\\r\\nAlthough new media types could have a parameter that refers to its presentation, such as: `application/seas;p=\\\"https://w3id.org/rdfp/example/graph/xml\\\"`. \\r\\nThis link cannot be set by a global media type parameter, as per [RFC 2045, Multipurpose Internet Mail Extensions (MIME) Part One: Format of Internet Message Bodies](https://tools.ietf.org/html/rfc2045):\\r\\n\\r\\n> There are NO globally-meaningful parameters that apply to all media types.  Truly global mechanisms are best addressed, in the MIME model, by the definition of additional Content-* header fields.\\r\\n\\r\\nWe hence introduce HTTP header field `Content-Presentation` for this purpose. The value of this field is any absolute URI, that identifies the RDF Presentation of the represented RDF graph. \\r\\n\\r\\nUsing this header properly, any existing server can be adapted to behave \\\"as if\\\" it was producing RDF: the client simply needs to dereference the presentation link, and use the associated lifting rule to interpret the retrieved document as RDF.\\r\\n\\r\\nEqually important, a lightweight client/server can send lightweight binary messages, while still letting its server/client the chance to interpret the message body as RDF.\\r\\n\\r\\n## RDF Presentation Negotiation\\r\\n\\r\\nRDF Presentation Negotiation is a way for the client to state its presentation preferences for the response message body. \\r\\n\\r\\nTo achieve this, we introduce HTTP header field `Accept-Presentation`.  The value of this field is any (absolute) URI, that identifies the RDF Presentation the clients would like the server to use to encode the response RDF graph. \\r\\n\\r\\nUsing this header properly, a lightweight client can request a compliant server to encode its responses in a specific format, hence transferring all the computation cost on the server.\\r\\n\\r\\n\\r\\n## Directly referring to the lowering rule, validation rule, or lifting rule\\r\\n\\r\\nIn some situations, it may seem unreasonable to expect the client/server to:\\r\\n\\r\\n1. dereference the RDF Presentation URI,\\r\\n1. parse the RDF document,\\r\\n1. navigate to the lifting, validation, or lowering rule of the RDF Presentation.\\r\\n\\r\\nHence, in a future version of RDFP, we may introduce additional HTTP header fields to directly refer to these rules. For example:\\r\\n\\r\\n- HTTP header field `Content-Lifting-Rule` would mean that the receiver may use this lifting to generate RDF from the content;\\r\\n- HTTP header field `Accept-Lowering-Rule` would mean that the client expects the server to use this lowering rule to generate a content it can process.\\r\\n\\r\\n## Implementation over Jersey\\r\\n\\r\\n[`rdfp-jersey-server`](get-started.html) is an extension of Jersey that eases the development of RESTful services that produce and consume RDF. It hides the RDF Presentation description and RDF Presentation Negotiation parts, and lets the end developer focus on manipulating RDF graphs as [Apache Jena](http://jena.apache.org/) models.\\r\\n\\r\\n\\r\\n## Related projects\\r\\n\\r\\nThe RDFP vocabulary is used in the following related projects:\\r\\n\\r\\n- [ONTOP - Ontology Platform](https://w3id.org/ontop/);\\r\\n\\r\\n`rdfp-jersey-server` is used in the following related projects:\\r\\n\\r\\n- [PEP - Process Execution Platform](https://w3id.org/pep/);\\r\\n- [CNR Smart Charging Provider SEAS pilot platform](http://cnr-seas.cloudapp.net/scp/).\\r\\n\\r\\n## Contact\\r\\n\\r\\nmaxime.lefrancois.86@gmail.com\\r\\n\\r\\nhttp://maxime-lefrancois.info/\\r\\n\"@en .\n<https://w3id.org/rdfp/> <http://purl.org/dc/terms/creator> <http://maxime-lefrancois.info/me#> .\n<https://w3id.org/rdfp/> <http://purl.org/dc/terms/issued> \"2016-08-31\"^^<http://www.w3.org/2001/XMLSchema#date> .\n<https://w3id.org/rdfp/> <http://purl.org/dc/terms/modified> \"2016-09-08\"^^<http://www.w3.org/2001/XMLSchema#date> .\n<https://w3id.org/rdfp/> <http://purl.org/dc/terms/license> <https://www.apache.org/licenses/LICENSE-2.0/> .\n<https://w3id.org/rdfp/> <http://purl.org/vocab/vann/preferredNamespacePrefix> \"rdfp\" .\n<https://w3id.org/rdfp/> <http://purl.org/vocab/vann/preferredNamespaceUri> <https://w3id.org/rdfp/> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "ba6af6a51dac948077b82c8b0569cdb746fb69771a2adfeddcc2b5b26654a0fa" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "uOjAH2Vrh9138mbbaw19Ov0ebmNlF4sF5ndwgps8J8xrJazSt8QJgGTfGQ0kH/vjz/UXOKS97vCM6rRBKeEyYihh/fea7ic6lT2//O+/LrfIaxB6pNfehwhjxEW5S5eZchzWK/kdzniLUda2q4yVFvworkR4kCUQk+OaxJLTOU6L8E8mTJOgJEB3WYuzEbsBdRExZkhygXRaJ5OD+QC0bcx18dgKXiubbOPn6wh6FWQDgqaLQc40S23i8EUAR/GDRh9o0Lg0fvyf5A45qjiMKuywG65CTfQASgAt9xsdzPlDabN28iG5NrYMPJF0BTtbWHuez2HfZ9qy4KrkyF2asQ==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "25466"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "0.0.1" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-24T14:48:16Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-03-24T09:56:07Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "25466"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/rdfp/0.0.1/rdfp.nt> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/rdfp/0.0.1/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "From https://lov.linkeddata.es/ automatically crawled vocabularies hosted on w3id.org. This is the vocabulary rdfp."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "w3id.org ontologies"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/denis> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/denis/w3id.org/rdfp> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/denis/w3id.org> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Attribution fulfilled by  \n       * (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/w3id.org/rdfp/rdfp/0.0.1  \n       * on your website:  \n       * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia  \n       * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin  \n       * in academic publications cite: DBpedia - A Large-scale, Multilingual Knowledge Base Extracted from Wikipedia, J. Lehmann, R. Isele, M. Jakob, A. Jentzsch, D. Kontokostas, P. Mendes, S. Hellmann, M. Morsey, P. van Kleef, S. Auer, and C. Bizer. Semantic Web Journal 6 (2): 167--195 (2015)  \n     \n       ## How to contribute  \n       DBpedia is a community project, help us with:  \n       * editing the mappings at http://mappings.dbpedia.org  \n       * improve this documentation at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/mappings/rdfp/rdfp.md  \n       * help with the software relevant for extraction:  \n       ** https://github.com/dbpedia/extraction-framework/tree/master/core/src/main/scala/org/dbpedia/extraction/mappings  \n       ** in particular https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/InfoboxMappingsExtractor.scala  \n       \n       ## Debug  \n       Parselogs are currently kept here: http://downloads.dbpedia.org/temporary/parselogs/  \n     \n       ## Origin  \n       This dataset was extracted using the wikipedia-dumps available on https://dumps.wikimedia.org/  \n       using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework  \n       For more technical information on how these datasets were generated, please visit http://dev.dbpedia.org  \n     \n       # Changelog  \n       ## since 2018.09.12  \n       * were created as new modular releases, some issues remain  \n       * we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe xcharacters  \n       * parsing removed 250k triples total, debugging pending  \n       * object-uncleaned was not transformed into objects-cleaned and is missing  \n       * link to Wikimedia dump version is missing  \n       ## 2016.10.01  \n       * was taken from the previous BIG DBpedia releases under http://downloads.dbpedia.org/2016-10/ and included for completeness" ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/denis/w3id.org/rdfp/0.0.1> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "0.0.1" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-24T14:48:16Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "w3id.org ontologies"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/rdfp/0.0.1/dataid.ttl#rdfp.nt> .
    
    <https://yum-yab.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/denis> .
}
