<http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/pep/0.0.1/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/denis/w3id.org/pep>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://databus.dbpedia.org/denis/w3id.org>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/pep/0.0.1/dataid.ttl#pep.nt>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/denis/w3id.org/pep/0.0.1/pep.nt> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/pep/0.0.1/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "132"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<https://w3id.org/pep/> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology> .\n<https://w3id.org/pep/> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/vocommons/voaf#Vocabulary> .\n<https://w3id.org/pep/> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Document> .\n<https://w3id.org/pep/> <http://purl.org/dc/terms/title> \"Procedure Execution ontology.\"@en .\n<https://w3id.org/pep/> <http://purl.org/dc/terms/description> \"\\r\\n\\r\\nThe Procedure Execution ontology (PEP) defines `pep:ProcedureExecutor`s that implement `pep:Procedure` methods, and generate `pep:ProcedureExecution` activities. Procedures may be linked to some description of the input and/or the output using object properties `pep:hasInput` and `pep:hasOutput`. Their executions may be linked to some description of the command and/or the result using object properties `pep:hasResult` and `pep:hasCommand`. If the command or the result are simple RDF literals (a typed UNICODE string), then one may use datatype properties `pep:hasSimpleResult` and `pep:hasSimpleCommand` instead. Procedure executions made by a specific procedure executor using a specific procedure can be grouped in containers. Figure below overviews the PEP ontology, and illustrates it with an example.\\r\\n\\r\\n[![Overview of the PEP Ontology](https://w3id.org/pep/pep-1.1.png)](https://w3id.org/pep/pep-1.1.png).\\r\\n\\r\\n## PEP generalizes the core concepts of SOSA/SSN\\r\\n\\r\\nThe procedure execution ontology is a simple extension of the [Semantic Sensor Network (SSN) Ontology](https://www.w3.org/TR/vocab-ssn/) and its core, called [Sensor, Observation, Sample, and Actuator (SOSA)](https://www.w3.org/TR/vocab-ssn/). SOSA describe `sosa:Sensor`s that implement `sosa:Procedure`s and make `sosa:Observation`s, which are activities. In parallel to this, it describes `sosa:Actuator`s that implement `sosa:Procedure`s and make `sosa:Actuation`s. The Procedure Execution ontology defines an ontology pattern as a generalization of these two parallel conceptual models, which accounts for at least one third use case: *Web services exposed on the web may be called to trigger the execution of some procedures*.  SOSA/SSN defines input, output, results, and PEP introduces the command. \\r\\n\\r\\nWe provide an external document to  align with the [the Semantic Sensor Network ontology](https://w3id.org/pep/SSNAlignment-1.1).\\r\\n\\r\\n## PEP to describe Web services that call lengthy algorithms\\r\\n\\r\\nPEP can be used to describe [Web containers of Web service executions](http://ci.emse.fr/pep-platform/) one can call in a RESTful way, and that consume and produce documents with a RDF data model. A set of such containers are referred  to as a **Process Execution Platform**. \\r\\n\\r\\nWe provide an external document to align PEP with the [the RDF Presentation ontology](https://w3id.org/pep/RDFPAlignment-1.1). This  alignment encourages to describe the input and output of Web services using `rdfp:GraphDescription`, and the command and result of their execution using URIs to `rdfp:RDFSource`s.\\r\\n\"@en .\n<https://w3id.org/pep/> <http://purl.org/dc/terms/issued> \"2016-07-01\"^^<http://www.w3.org/2001/XMLSchema#date> .\n<https://w3id.org/pep/> <http://purl.org/dc/terms/modified> \"2016-05-28\"^^<http://www.w3.org/2001/XMLSchema#date> .\n<https://w3id.org/pep/> <http://purl.org/dc/terms/creator> <http://www.maxime-lefrancois.info/me#> .\n<https://w3id.org/pep/> <http://purl.org/dc/terms/license> <https://www.apache.org/licenses/LICENSE-2.0> .\n<https://w3id.org/pep/> <http://purl.org/vocab/vann/preferredNamespacePrefix> \"pep\" ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "4c887d04891ca464ce15f6f265707d0c92ae6f821e6142fa99068a1f02593cb3" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "px+Pdh58kCmwh+JONTeRE4pt8ZRaBKwwAb1mCQohWKXuGPd/suWJVPn/DEOoxz4ChiFoKhsAR9WaqZrCJP53aCj2+XzukxXTiTsw2unEKdhRBrs4TlS/yZAlW2ZEx5cEJa6sVJUmsQmOFW9jS1dmh2Hi9GIIeywk1G/hAh01G8lxAowoX98u0faQTHEs3avtc9LLQAqcDIU958hsEmCsLsoMW9U+U2kt9TL7A1K0uQKJuS3ZDhaBI0+YZV+M5w+QPlF1JAhS1PrI6+JzvMS3mZuKMqb90P1+xO7vavk8sn1noKfQJOSHerQ4qeb1A5GgcqtqhSbeGOUwatsxaeH/rg==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "19408"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "0.0.1" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-24T14:54:16Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-03-24T09:54:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "19408"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/pep/0.0.1/pep.nt> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/denis/w3id.org/pep/0.0.1>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/pep/0.0.1/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "From https://lov.linkeddata.es/ automatically crawled vocabularies hosted on w3id.org. This is the vocabulary pep."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "w3id.org ontologies"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/denis> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/denis/w3id.org/pep> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/denis/w3id.org> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Attribution fulfilled by  \n       * (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/w3id.org/pep/pep/0.0.1  \n       * on your website:  \n       * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia  \n       * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin  \n       * in academic publications cite: DBpedia - A Large-scale, Multilingual Knowledge Base Extracted from Wikipedia, J. Lehmann, R. Isele, M. Jakob, A. Jentzsch, D. Kontokostas, P. Mendes, S. Hellmann, M. Morsey, P. van Kleef, S. Auer, and C. Bizer. Semantic Web Journal 6 (2): 167--195 (2015)  \n     \n       ## How to contribute  \n       DBpedia is a community project, help us with:  \n       * editing the mappings at http://mappings.dbpedia.org  \n       * improve this documentation at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/mappings/pep/pep.md  \n       * help with the software relevant for extraction:  \n       ** https://github.com/dbpedia/extraction-framework/tree/master/core/src/main/scala/org/dbpedia/extraction/mappings  \n       ** in particular https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/InfoboxMappingsExtractor.scala  \n       \n       ## Debug  \n       Parselogs are currently kept here: http://downloads.dbpedia.org/temporary/parselogs/  \n     \n       ## Origin  \n       This dataset was extracted using the wikipedia-dumps available on https://dumps.wikimedia.org/  \n       using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework  \n       For more technical information on how these datasets were generated, please visit http://dev.dbpedia.org  \n     \n       # Changelog  \n       ## since 2018.09.12  \n       * were created as new modular releases, some issues remain  \n       * we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe xcharacters  \n       * parsing removed 250k triples total, debugging pending  \n       * object-uncleaned was not transformed into objects-cleaned and is missing  \n       * link to Wikimedia dump version is missing  \n       ## 2016.10.01  \n       * was taken from the previous BIG DBpedia releases under http://downloads.dbpedia.org/2016-10/ and included for completeness" ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/denis/w3id.org/pep/0.0.1> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "0.0.1" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-24T14:54:16Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "w3id.org ontologies"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/w3id.org/pep/0.0.1/dataid.ttl#pep.nt> .
    
    <https://yum-yab.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/denis> .
}
