<http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/data.europa.eu/eli--ontology/0.0.1/dataid.ttl#Dataset> {
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/data.europa.eu/eli--ontology/0.0.1/dataid.ttl#eli--ontology.nt>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "None" ;
            <http://dataid.dbpedia.org/ns/core#duplicates>
                    "0"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/denis/data.europa.eu/eli--ontology/0.0.1/eli--ontology.nt> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/data.europa.eu/eli--ontology/0.0.1/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#nonEmptyLines>
                    "1292"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://data.europa.eu/eli/ontology#> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology> .\n<http://data.europa.eu/eli/ontology#> <http://www.w3.org/2002/07/owl#imports> <http://www.w3.org/2004/02/skos/core> .\n<http://data.europa.eu/eli/ontology#> <http://www.w3.org/2004/02/skos/core#historyNote> \"## 1.2\\n\\n### Semantic changes\\n\\n  - Modification of eli:format and introduction of eli:media_type :\\n    - Modified the semantic of eli:format so that it now corresponds to a business / application format, and not a technical format (the technical format will be captured in eli:media_type). This is intended to distinguish e.g. authentic PDF from scanned PDF from generated-on-the-fly PDF; or e.g. XML in a standard (AkomaNtoso) format vs. XML in a proprietary format.\\n      - It is now possible to use the custom URIs \\u201Capplication/pdf;type=signed\\u201D, \\u201Capplication/pdf;type=generated\\u201D, \\u201Capplication/pdf;type=archival\\u201D, \\u201Capplication/pdf;type=scanned\\u201D as values of eli:format to indicate respectively electronically signed PDFs, generated-on-the-fly PDFs, PDF for archival, or scanned PDFs;\\n      - It is now possible to use the custom URI \\u201Celi:print_format\\u201D as a value of eli:format to indicate a printed/paper version of the legislation;\\n      - It is now possible to use the URI \\u201Capplication/akn+xml\\u201D as a value of eli:format to indicate an AkomaNtoso format;\\n    - Added a new property \\u201Celi:media_type\\u201D to capture the technical file format of the file; this is intended to serve as a basis for content negociation; IANA identifiers MUST be used in eli:media_type;\\n\\n    - Existing ELI implementations using eli:format with IANA URIs remain valid; the eli:format property remains mandatory. ELI providers are however encouraged to add the \\u201Celi:media_type\\u201D property to their ELI metadata; in some situations the value of both properties might be the same (\\u201Celi:format = application/xml\\u201D and \\u201Celi:media_type = application/xml\\u201D). It is expected that in the future the \\u201Celi:media_type\\u201D property will become mandatory.\\n\\n  - Added a couple of optional properties \\u201Chas_translation\\u201D / \\u201Cis_translation_of\\u201D to indicate that a LegalExpression is translated from another one;\\n  - Added the property \\u201Ccited_by_case_law\\u201D and the corresponding literal property \\u201Ccited_by_case_law_reference\\u201D :\\n    - Cited_by_case_law : Indicates that this LegalResource or LegalExpression is being cited in a case law, identified by a suitable URI. If the case law cannot be identified by a suitable URI, the property \\\"eli:cited_by_case_law_reference\\\" can be used with a textual reference to the case law. The actual citation link is expressed from the case law to the legislation, but legal portals may use the link from a legislation to a case law to e.g. refer to representative case laws about a legislation.\\n    - Cited_by_case_law_reference : Indicates that this LegalResource or LegalExpression is being cited in a case law that cannot be identified by a suitable URI and that is indicated by textual reference. An ECLI (European Case Law Identifier) can be used here. When the case law can be identified by a suitable URI, the property eli:cited_by_case_law should be used instead.\\n\\n### Non-semantic changes\\n\\n  - Referred to FRBRoo URIs instead of RDA URIs to extend the LegalResource, LegalExpression and Format classes, along with the FRBR backbone properties. This properly connects ELI with FRBRoo;\\n  - Added equivalence declarations between ELI and the schema.org legal extension; this is provided as a separate Excel and Turtle file;\\n  - Removed xsd:string range declaration on eli:description, title, title_short, title_alternative, rightsholder, rights, publisher, published_in so that can be declared with either a datatype or a language tag;\\n  - Fixed documentation of some properties;\\n  - Added link to Eur-Lex website in ontology documentation;\\n\\n## 1.1\\n\\n  - added the following properties :\\n    - is_member_of / has_member\\n    - number\\n    - jurisdiction\\n    - date_applicability\\n    - applies / applied_by\\n    - commences / commenced_by\\n    - repeals / repealed_by\\n    - corrects / corrected_by\\n    - amends / amended_by\\n    - is_another_publication_of / has_another_publication\\n\\n  - additionnal properties were also added to accomodate with situations where a property value can be either a literal or a URI :\\n    - responsibility_of_agent vs. responsibility_of\\n    - published_in_format vs. published_in\\n    - publisher_agent vs. publisher\\n    - rightsholder_agent vs. rightsholder\\n\\n  - the relations with DublinCore properties is now declared with a custom annotation property \\\"eli:isSpecificPropertyOf\\\" instead of rdfs:subPropertyOf to be able to relate both eli:publisher/eli:publisher_agent to dcterms:publisher, and eli:rightsholder/eli:rightsholder_agent to dcterms:rightsholder.\\n\\n  - added the class LegalResourceSubdivision\\n\\n  - the propery implements / implemented_by was deprecated in favor of applies / applied_by\\n\\n  - numerous definitions were improved and clarified\\n\\n  - all ontology items are documented with :\\n    - their version of creation;\\n    - their version of latest modification;\\n    - an history note documenting the changes on the item;\\n\\nV1.1 is backward compatible with v1.0, with 1 following exception :\\n  - starting from v1.1, is_member_of / has_member should be used instead of is_part_of / has_part for conceptual inclusions;\\n\\nAdditionnally, ELI implementers are strongly encouraged to use applies / applied_by instead of implements / implemented_by; note however that implements/implemented_by currently remain in the ontology so publishers can take the time to transition smoothly.\\n\\n## 1.0\\n\\n  - added a set of values for the InForce table\\n  - added definitions to tables\\n  - minor fixes in definitions\\n  - added rdfs:seeAlso from properties to corresponding tables\\n  - cleaned the \\\"whyThisIsNeeded\\\" annotation\\n\\n## 0.6.1\\n\\n  - fixed typos in definitions\\n\\n## 0.6\\n\\n  - imported skos rather than redeclaring skos classes and properties\\n  - changed domain of in_force, first_date_entry_into_force, date_no_longer_in_force to [LegalResource or LegalExpression] instead of LegalResource\\n  - changed domain of language, title, title_short, title_alternative to LegalExpression instead of [LegalResource or LegalExpression]\\n  - reworked the tables : don't use country and resource type tables from MDR, but declare local tables.\\n  - integrated new definitions for all classes and properties\\n\\n## 0.5\\n\\n  - Renamed \\\"Interpretation\\\" into \\\"LegalExpression\\\" (and modified comments accordingly)\\n  - Renamed the structural FRBR relations in accordance with Metalex ontology :\\n    - is_legal_resource_of becomes \\\"is_relaized_by\\\"\\n    - has_legalresource becomes \\\"realizes\\\"\\n    - is_interpretation_of becomes \\\"is_embodied_by\\\"\\n    - has_interpretation becomes \\\"embodies\\\"\\n    - file becomes \\\"is_exemplified_by\\\"\\n  - file has been made a subproperty of \\\"has exemplar of manifestation\\\" instead of \\\"dc:hasPart\\\"\\n  - relevant_for is no more considered mandatory. Deleted comment that stated \\\"we are seeking a way to express that relevant_for must have at least one value for LegalResource or one of its LegalInterpretation\\\".\\n\\n## 0.4.1\\n\\n  - Added \\\"publishes\\\" property\\n  - Enriched the note on \\\"transposes\\\" and \\\"transposed by\\\" to indicate they must point to / be expressed on LegalResource\"@en .\n<http://data.europa.eu/eli/ontology#> <http://www.w3.org/2002/07/owl#versionInfo> \"1.2\"^^<http://www.w3.org/2001/XMLSchema#decimal> .\n<http://data.europa.eu/eli/ontology#> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <http://publications.europa.eu/mdr/eli/> .\n<http://data.europa.eu/eli/ontology#> <http://purl.org/vocab/vann/preferredNamespacePrefix> \"eli\" .\n<http://data.europa.eu/eli/ontology#> <http://purl.org/dc/terms/creator> \"ELI task force\"@en .\n<http://data.europa.eu/eli/ontology#> <http://www.w3.org/2000/01/rdf-schema#comment> \"An ontology to describe legislation, based on the FRBR model, that encodes the ELI metadata fields.\\nSee the classes \\\"LegalResource\\\", \\\"LegalExpression\\\" and \\\"Format\\\" as entry points.\\nInterested readers can find more information about ELI on the ELI register at http://eurlex.europa.eu/eli and on the Office of Publications vocabularies website at http://publications.europa.eu/mdr/eli/.\\n        \"@en .\n<http://data.europa.eu/eli/ontology#> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <http://data.europa.eu/eli/ontology#LegalResource> .\n<http://data.europa.eu/eli/ontology#> <http://www.w3.org/2004/02/skos/core#editorialNote> \"- The ELI ontology reuses the property names from the Metalex ontology (http://www.metalex.eu/) to express the FRBR skeleton hierarchy : is_realized_by/realizes, is_embodied_by/embodies, is_exemplified_by. This wording is also the one used in the original FRBR specifications. However, the decision has been made _not_ to align to the Metalex class names (BibliographicWork, BibliographicExpression, BibliographicManifestation), because ELI is specifically about publishing legal metadata on the web, and not structuring legal content in any document.\\n\\n- first_date_entry_in_force and date_no_longer_in_force could be aligned with dcterms:date\\n\\n- a class could be introduced for URI schemas to implement the URI schema registry\\n\\n- ELI does not use rdfs:subPropertyOf to relate its properties with DublinCore Terms, but instead a custom annotation properties \\\"eli:isSpecificPropertyOf\\\". ELI 1.1 chose to have 2 separate properties in certain situations to accomodate with cases where the value can be either a String or a reference to an identified object; this is the case for eli:publisher / eli:publisher_agent, or eli:rightsholder and eli:rightsholder_agent. This entails that this pair of properties cannot be both aligned with their respective DCTerms equivalent (dcterms:publisher and dcterms:rightsholder), since relating a property using rdfs:subPropertyOf necessary implies to declare the DCterms property as either a datatype property _or_ an objectProperty, but not both.\"@en ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "e806094b87beb36be18213610cde07ea2bf43535636d8f525d55b8ba2551c756" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "qeQJXsN56+pLjhkroUjeS/WAlZ2QdxWUbnf+mg7vr/d1FrzwDB+zaTmp3WMV5mDaUbog1JpzBA1AsxzhfzYRF8WHedywrEgZl9uLAQgjDCDDsW5U3SZY30YY/zEeRpeCzmc0afZ4/HQ6VKwy6EOe/ZUrclyt++eXxHiOwWyCAKP493lEQLLhEM/W7eas1yU8i/oh2OWSCKnfeYK5NQ1YwxvMNrL3YYdD8wqI9BRv8KOtSYGqzlsUs5KAVNvGeWr1BPHbwKSiI3LNQMCKe4tC1wnVvvrM73aQgveJtybdBSRBYgA+a3tmHy1MTcfRxH+hpzuhdT6Kx8Jdw5+g2FMP2Q==" ;
            <http://dataid.dbpedia.org/ns/core#sorted>
                    false ;
            <http://dataid.dbpedia.org/ns/core#uncompressedByteSize>
                    "192654"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "0.0.1" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-24T11:15:42Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-03-24T09:42:02Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "192654"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/data.europa.eu/eli--ontology/0.0.1/eli--ontology.nt> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/data.europa.eu/eli--ontology/0.0.1/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "From https://lov.linkeddata.es/ automatically crawled vocabularies hosted on data.europa.eu. This is the vocabulary eli--ontology."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "data.europa.eu ontologies"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/denis> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/denis/data.europa.eu/eli--ontology> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/denis/data.europa.eu> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "## Attribution fulfilled by  \n       * (when deriving another dataset and releasing to the Databus) adding the Databus link to the provenance https://databus.dbpedia.org/dbpedia/data.europa.eu/eli--ontology/eli--ontology/0.0.1  \n       * on your website:  \n       * include the DBpedia logo and mention the usage of DBpedia with this link: https://databus.dbpedia.org/dbpedia  \n       * include backlinks from your pages to the individual entities, e.g. http://dbpedia.org/resource/Berlin  \n       * in academic publications cite: DBpedia - A Large-scale, Multilingual Knowledge Base Extracted from Wikipedia, J. Lehmann, R. Isele, M. Jakob, A. Jentzsch, D. Kontokostas, P. Mendes, S. Hellmann, M. Morsey, P. van Kleef, S. Auer, and C. Bizer. Semantic Web Journal 6 (2): 167--195 (2015)  \n     \n       ## How to contribute  \n       DBpedia is a community project, help us with:  \n       * editing the mappings at http://mappings.dbpedia.org  \n       * improve this documentation at https://github.com/dbpedia/databus-maven-plugin/tree/master/dbpedia/mappings/eli--ontology/eli--ontology.md  \n       * help with the software relevant for extraction:  \n       ** https://github.com/dbpedia/extraction-framework/tree/master/core/src/main/scala/org/dbpedia/extraction/mappings  \n       ** in particular https://github.com/dbpedia/extraction-framework/blob/master/core/src/main/scala/org/dbpedia/extraction/mappings/InfoboxMappingsExtractor.scala  \n       \n       ## Debug  \n       Parselogs are currently kept here: http://downloads.dbpedia.org/temporary/parselogs/  \n     \n       ## Origin  \n       This dataset was extracted using the wikipedia-dumps available on https://dumps.wikimedia.org/  \n       using the DBpedia Extraction-Framework available at https://github.com/dbpedia/extraction-framework  \n       For more technical information on how these datasets were generated, please visit http://dev.dbpedia.org  \n     \n       # Changelog  \n       ## since 2018.09.12  \n       * were created as new modular releases, some issues remain  \n       * we used rapper 2.0.14 to parse and `LC_ALL=C sort` to sort and ascii2uni -a U to unescape unicdoe xcharacters  \n       * parsing removed 250k triples total, debugging pending  \n       * object-uncleaned was not transformed into objects-cleaned and is missing  \n       * link to Wikimedia dump version is missing  \n       ## 2016.10.01  \n       * was taken from the previous BIG DBpedia releases under http://downloads.dbpedia.org/2016-10/ and included for completeness" ;
            <http://dataid.dbpedia.org/ns/core#maintainer>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/denis/data.europa.eu/eli--ontology/0.0.1> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "0.0.1" ;
            <http://purl.org/dc/terms/issued>
                    "2020-03-24T11:15:42Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://yum-yab.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "data.europa.eu ontologies"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://akswnc7.informatik.uni-leipzig.de/dstreitmatter/lov-crawl/data.europa.eu/eli--ontology/0.0.1/dataid.ttl#eli--ontology.nt> .
    
    <https://databus.dbpedia.org/denis/data.europa.eu/eli--ontology/0.0.1>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/denis/data.europa.eu>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <https://yum-yab.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/denis> .
    
    <https://databus.dbpedia.org/denis/data.europa.eu/eli--ontology>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
}
