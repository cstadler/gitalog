<http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-relations/2021.03.22/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-relations>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <https://nheist.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/nico.heist> .
    
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-relations/2021.03.22/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Relations that exist in CaLiGraph, but not in DBpedia expressed through predicates from the DBpedia ontology. Contains triples of the form `<$dbpedia_resource> <$dbpedia_ontology_predicate> <$dbpedia_ontology_class>`."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "CaLiGraph relations in DBpedia namespace"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/nico.heist> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-relations> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "CaLiGraph: A Large Semantic Knowledge Graph from Wikipedia Categories and List Pages\nFor more information about CaLiGraph, refer to http://caligraph.org.\n\n## Changelog\n* 2021.03.22\n  * Improved entity extraction and representation\n  * Small cosmetic changes\n* 2021.03.01\n  * Entity extraction from tables and enumerations all over Wikipedia\n* 2021.02.01\n  * BERT-based recognition of subject entities and improved language models from spaCy 3.0\n* 2021.01.05\n  * Fixed minor encoding errors and improved formatting\n* 2020.12.21\n  * CaLiGraph is now based on a recent version of Wikipedia and DBpedia from November 2020\n* 2020.09.20\n  * Improvements to the CaLiGraph class hierarchy\n  * Many small bugfixes\n* 2020.08.19\n  * Add alternative labels for CaLiGraph instances\n* 2020.05.11\n  * Adapt URI encoding to DBpedia conventions\n* 2020.03.01\n  * Add dbpedia-caligraph-types and dbpedia-transitive-caligraph-types artifacts for direct typing of DBpedia resources with CaLiGraph types.\n  * The artifact ontology-dbpedia-mapping uses the 'subClassOf' predicate instead of the 'equivalentClass' predicate to link to DBpedia types.\n* 2020.01.22\n  * Initial release on DBpedia Databus" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-relations/2021.03.22> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#codeReference>
                    <https://github.com/nheist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#documentationLocation>
                    <https://github.com/nheist/CaLiGraph/blob/master/README.md> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#feedbackChannel>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#issueTracker>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.22" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-22T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "CaLiGraph relations in DBpedia namespace"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-relations/2021.03.22/dataid.ttl#dbpedia-relations.nt.bz2> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-relations/2021.03.22>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-relations/2021.03.22/dataid.ttl#dbpedia-relations.nt.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-relations/2021.03.22/dbpedia-relations.nt.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-relations/2021.03.22/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://dbpedia.org/resource/Gaylord_Herald_Times> <http://dbpedia.org/ontology/headquarter> <http://dbpedia.org/resource/Maryland> .\n<http://dbpedia.org/resource/Howard_Nusbaum> <http://dbpedia.org/ontology/birthPlace> <http://dbpedia.org/resource/New_York_(state)> .\n<http://dbpedia.org/resource/T%C3%B3mas_%C3%81rnason> <http://dbpedia.org/ontology/deathYear> \"2014\" .\n<http://dbpedia.org/resource/The_Manny_Files> <http://dbpedia.org/ontology/country> <http://dbpedia.org/resource/United_States> .\n<http://dbpedia.org/resource/Matthew_Guinan> <http://dbpedia.org/ontology/deathYear> \"1995\" .\n<http://dbpedia.org/resource/Dick_Linder> <http://dbpedia.org/ontology/birthPlace> <http://dbpedia.org/resource/Ohio> .\n<http://dbpedia.org/resource/Run_Run_Run_(band)> <http://dbpedia.org/ontology/genre> <http://dbpedia.org/resource/Rock_music> .\n<http://dbpedia.org/resource/Rajendra_Kumar_(politician)> <http://dbpedia.org/ontology/birthPlace> <http://dbpedia.org/resource/Uttar_Pradesh> .\n<http://dbpedia.org/resource/Dan_Bigras> <http://dbpedia.org/ontology/occupation> <http://dbpedia.org/resource/Singing> .\n<http://dbpedia.org/resource/Potter_Palmer> <http://dbpedia.org/ontology/birthPlace> <http://dbpedia.org/resource/Cook_County,_Illinois> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "c5eb05c1063b98599c7d339ff45f0e904725d1e4d6aa88387b7a656720db6e15" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "AlBxYbPxh4vWyV11c/DkcBUoDnFxXVN5QkH4gldjjSuZxs76SvKqB5BQwUxpWUQYPWhsIQd103NQAJlZKc9XWDCP3sVdoFP67eTj7NJYjsngO/mWFFdpS8rs/YuYy00oJZ1WTRIeQ32FGNBNaUWMNiKy/cW0B1Oy5iJfSgV/FAwnLfvEzxZgTwWxm1QqXaxE/XSPhzAnN4SUxXObdGK7vKtcdTvPQbV5QA7TT1QRGvM0fvSwgmc0bYVGChohb8fX91jnuXSOQx5ap6deVvPI2WTQf6M1eDOj43mK5/4LVs5h3rJi1yNV587S/gHrx2kw8tgJU1ZIjhVl/HsXeDqcCQ==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.03.22" ;
            <http://purl.org/dc/terms/issued>
                    "2021-03-22T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-03-22T14:18:36Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "121646193"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-relations/2021.03.22/dbpedia-relations.nt.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
