<http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/metadata/2020.09.20/dataid.ttl#Dataset> {
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/metadata/2020.09.20/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Metadata about the dataset which is described using void vocabulary."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "CaLiGraph metadata"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/nico.heist> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/metadata> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "CaLiGraph: A Large Semantic Knowledge Graph from Wikipedia Categories and List Pages\nFor more information about CaLiGraph, refer to http://caligraph.org.\n\n## Changelog\n* 2020.09.20\n  * Improvements to the CaLiGraph class hierarchy\n  * Many small bugfixes\n* 2020.08.19\n  * Add alternative labels for CaLiGraph instances\n* 2020.05.11\n  * Adapt URI encoding to DBpedia conventions\n* 2020.03.01\n  * Add dbpedia-caligraph-types and dbpedia-transitive-caligraph-types artifacts for direct typing of DBpedia resources with CaLiGraph types.\n  * The artifact ontology-dbpedia-mapping uses the 'subClassOf' predicate instead of the 'equivalentClass' predicate to link to DBpedia types.\n* 2020.01.22\n  * Initial release on DBpedia Databus" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/metadata/2020.09.20> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#codeReference>
                    <https://github.com/nheist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#documentationLocation>
                    <https://github.com/nheist/CaLiGraph/blob/master/README.md> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#feedbackChannel>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#issueTracker>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.09.20" ;
            <http://purl.org/dc/terms/issued>
                    "2020-09-20T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "CaLiGraph metadata"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/metadata/2020.09.20/dataid.ttl#metadata.nt.bz2> .
    
    <https://nheist.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/nico.heist> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/metadata/2020.09.20>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/metadata/2020.09.20/dataid.ttl#metadata.nt.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/metadata/2020.09.20/metadata.nt.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/metadata/2020.09.20/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://caligraph.org/.well-known/void> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://rdfs.org/ns/void#Dataset> .\n<http://caligraph.org/.well-known/void> <http://purl.org/dc/elements/1.1/title> \"CaLiGraph\" .\n<http://caligraph.org/.well-known/void> <http://www.w3.org/2000/01/rdf-schema#label> \"CaLiGraph\" .\n<http://caligraph.org/.well-known/void> <http://purl.org/dc/elements/1.1/description> \"The CaLiGraph is a large-scale general-purpose knowledge graph that extends DBpedia with a more fine-grained and restrictive ontology as well as additional resources extracted from Wikipedia Listpages.\" .\n<http://caligraph.org/.well-known/void> <http://purl.org/dc/terms/license> <http://www.gnu.org/copyleft/fdl.html> .\n<http://caligraph.org/.well-known/void> <http://purl.org/dc/terms/license> <http://creativecommons.org/licenses/by-sa/3.0/> .\n<http://caligraph.org/.well-known/void> <http://purl.org/dc/terms/creator> \"Nicolas Heist\" .\n<http://caligraph.org/.well-known/void> <http://purl.org/dc/terms/creator> \"Heiko Paulheim\" .\n<http://caligraph.org/.well-known/void> <http://purl.org/dc/terms/created> \"2020-09-20\"^^<http://www.w3.org/2001/XMLSchema#date> .\n<http://caligraph.org/.well-known/void> <http://purl.org/dc/terms/publisher> \"Nicolas Heist\" ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "1c96a7c13856da2a1f8b2f1718430d9132d9fa8f40fc95381063cce2e91e47bf" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "kfHtkavBdaZyjEb3+9m4MMnRgYck6hCtVF2qYd2jYx6RdQj2Tc036VHHR+uqoEiSbPST+6qerDlXSSzHzGeCYLZNHY331HpJ58UqEypItvhsLr5pbWRgdOrX/kojbOl7xe8PXo0eVsOkbZEhrKJ5/HBqMmsrQ+1AxF7ZKC/F7OXBnmOFcrFE379l7niXLksl8ocuQ4mgJ4001URamyVl7Hlcws0RcDhzgyTC7MveeYmi6DAGH8nmQ6bG1GmbaoZ8PIZLwLZvluCnTIhayCzW3klhOHLaBtbQrCtGgO8FvPJHX95b5pNmFZcnnbaigttS/uSX9hjCyEHvFgO9sDesmQ==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2020.09.20" ;
            <http://purl.org/dc/terms/issued>
                    "2020-09-20T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2020-09-28T09:50:19Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "685"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/metadata/2020.09.20/metadata.nt.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/metadata>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
}
