<http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-caligraph-types/2021.02.01/dataid.ttl#Dataset> {
    <https://nheist.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/nico.heist> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-caligraph-types/2021.02.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-caligraph-types/2021.02.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "CaLiGraph types assigned to DBpedia instances. Contains triples of the form `<$dbpedia_resource> rdf:type <$caligraph_ontology_class>`."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "CaLiGraph types for DBpedia resources"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/nico.heist> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-caligraph-types> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "CaLiGraph: A Large Semantic Knowledge Graph from Wikipedia Categories and List Pages\nFor more information about CaLiGraph, refer to http://caligraph.org.\n\n## Changelog\n# 2021.02.01\n  * BERT-based recognition of subject entities and improved language models from spaCy 3.0\n* 2021.01.05\n  * Fixed minor encoding errors and improved formatting\n* 2020.12.21\n  * CaLiGraph is now based on a recent version of Wikipedia and DBpedia from November 2020\n* 2020.09.20\n  * Improvements to the CaLiGraph class hierarchy\n  * Many small bugfixes\n* 2020.08.19\n  * Add alternative labels for CaLiGraph instances\n* 2020.05.11\n  * Adapt URI encoding to DBpedia conventions\n* 2020.03.01\n  * Add dbpedia-caligraph-types and dbpedia-transitive-caligraph-types artifacts for direct typing of DBpedia resources with CaLiGraph types.\n  * The artifact ontology-dbpedia-mapping uses the 'subClassOf' predicate instead of the 'equivalentClass' predicate to link to DBpedia types.\n* 2020.01.22\n  * Initial release on DBpedia Databus" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-caligraph-types/2021.02.01> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#codeReference>
                    <https://github.com/nheist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#documentationLocation>
                    <https://github.com/nheist/CaLiGraph/blob/master/README.md> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#feedbackChannel>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#issueTracker>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.02.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-02-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "CaLiGraph types for DBpedia resources"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-caligraph-types/2021.02.01/dataid.ttl#dbpedia-caligraph-types.nt.bz2> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-caligraph-types/2021.02.01/dataid.ttl#dbpedia-caligraph-types.nt.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-caligraph-types/2021.02.01/dbpedia-caligraph-types.nt.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-caligraph-types/2021.02.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://dbpedia.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/Military_unit_or_formation_established_in_1946> .\n<http://dbpedia.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/Military_unit_or_formation_of_the_Bosnian_War> .\n<http://dbpedia.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/1946_establishment_in_Yugoslavia> .\n<http://dbpedia.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/Military_unit> .\n<http://dbpedia.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/Regiment_of_Yugoslav_Air_Force> .\n<http://dbpedia.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/Military_unit_or_formation_of_the_Croatian_War_of_Independence> .\n<http://dbpedia.org/resource/International_Center_of_Parapsychology_and_Scientific_Research_of_the_New_Age> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/Religious_organization_established_in_1989> .\n<http://dbpedia.org/resource/International_Center_of_Parapsychology_and_Scientific_Research_of_the_New_Age> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/New_religious_movement> .\n<http://dbpedia.org/resource/Port_Mayaca_Lock_and_Dam> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/Lock_of_Florida> .\n<http://dbpedia.org/resource/Port_Mayaca_Lock_and_Dam> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://caligraph.org/ontology/Dam_in_Florida> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "db6e2f95d95135370b48a85e691ca805c289549d92a5b47220d21b98252ff06c" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "r8Cl8rZfWyqbclm0w7p4szKLQFrF5Rx15K8r24gIobrwHOJWUzZ/abspMOb+gUMO6WOEggcyTPZATo3pOtk4AqAmYUCMh6MGU1k3xXLLm2R5/soQSd//NNKic/lY/gXhGEBbmawzfMQ6+bhfrU8lMGQlNSJF4hoA0lf3QaY6dJfx6vDO2TbLjeNc7nZq0wIak+gxL3MM2bzZJG47OxkJY5Btx4NF4fgzbX5MdeHH5gnrt5uzK93SwpVBP8Jz+ea3P3MtCm/HV77eCL6FBlB0Z/qaW8CbwyMUXsBv7LxzAUC8DWnLa1c/xkKAU7OQQ3DAmAn8FmtKHgQ0RZGwm82itw==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.02.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-02-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-02-08T08:57:45Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "304014619"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/dbpedia-caligraph-types/2021.02.01/dbpedia-caligraph-types.nt.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/dbpedia-caligraph-types>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
}
