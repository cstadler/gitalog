<http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/ontology-dbpedia-mapping/2021.01.05/dataid.ttl#Dataset> {
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/ontology-dbpedia-mapping>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
    
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/ontology-dbpedia-mapping/2021.01.05/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Mapping of CaLiGraph types and properties to the DBpedia ontology."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "CaLiGraph ontology to DBpedia ontology mapping"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/nico.heist> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/ontology-dbpedia-mapping> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "CaLiGraph: A Large Semantic Knowledge Graph from Wikipedia Categories and List Pages\nFor more information about CaLiGraph, refer to http://caligraph.org.\n\n## Changelog\n* 2021.01.05\n  * Fixed minor encoding errors and improved formatting\n* 2020.12.21\n  * CaLiGraph is now based on a recent version of Wikipedia and DBpedia from November 2020\n* 2020.09.20\n  * Improvements to the CaLiGraph class hierarchy\n  * Many small bugfixes\n* 2020.08.19\n  * Add alternative labels for CaLiGraph instances\n* 2020.05.11\n  * Adapt URI encoding to DBpedia conventions\n* 2020.03.01\n  * Add dbpedia-caligraph-types and dbpedia-transitive-caligraph-types artifacts for direct typing of DBpedia resources with CaLiGraph types.\n  * The artifact ontology-dbpedia-mapping uses the 'subClassOf' predicate instead of the 'equivalentClass' predicate to link to DBpedia types.\n* 2020.01.22\n  * Initial release on DBpedia Databus" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/ontology-dbpedia-mapping/2021.01.05> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#codeReference>
                    <https://github.com/nheist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#documentationLocation>
                    <https://github.com/nheist/CaLiGraph/blob/master/README.md> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#feedbackChannel>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#issueTracker>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.05" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-05T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "CaLiGraph ontology to DBpedia ontology mapping"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/ontology-dbpedia-mapping/2021.01.05/dataid.ttl#ontology-dbpedia-mapping.nt.bz2> .
    
    <https://nheist.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/nico.heist> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/ontology-dbpedia-mapping/2021.01.05>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/ontology-dbpedia-mapping/2021.01.05/dataid.ttl#ontology-dbpedia-mapping.nt.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/ontology-dbpedia-mapping/2021.01.05/ontology-dbpedia-mapping.nt.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/ontology-dbpedia-mapping/2021.01.05/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://caligraph.org/ontology/Mean_of_transportation> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/MeanOfTransportation> .\n<http://caligraph.org/ontology/GeneLocation> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/GeneLocation> .\n<http://caligraph.org/ontology/Cipher> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/Cipher> .\n<http://caligraph.org/ontology/Gross_domestic_product_per_capita> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/GrossDomesticProductPerCapita> .\n<http://caligraph.org/ontology/Population> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/Population> .\n<http://caligraph.org/ontology/Demographic> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/Demographics> .\n<http://caligraph.org/ontology/Algorithm> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/Algorithm> .\n<http://caligraph.org/ontology/Anatomical_structure> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/AnatomicalStructure> .\n<http://caligraph.org/ontology/Food> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/Food> .\n<http://caligraph.org/ontology/Diploma> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://dbpedia.org/ontology/Diploma> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "e6a47e58d673b9af0583d3ceba7e74b2ba0192433094ceb3c38b9a171ac62763" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "Ssoza3jkDCypPk+zpIN1Vsg1dZ2dULw4qy/7WqX7gYtUdLJOzaG6NeSs14IAJm/LvhZimkfVrYxsr66DtkNZTRLbSGY+DGOVJu9g9Vihr7po1EhGfIbKErI0ylEeREJkQxk4MF6kc17VJLqE3ICtR9lovBPvfiDxuI1H+gaxkgGXFzR2hOED3DK2owhwF0lx7IYGWqPQuRnJlI91Rb7ngxgo5qB0oZntpCOeaF4GJBL8zfmSHa1rrRyof0f+9tiNpvgx7gQCe3J31NyQqxIVoILaV8JLhGD/VCcwM2HXcD+ygHp+hFAx3EhKUT5H3TPwNbWBwfl3LIhSnSyCJAm31g==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.01.05" ;
            <http://purl.org/dc/terms/issued>
                    "2021-01-05T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-01-11T08:55:05Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "9646"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/ontology-dbpedia-mapping/2021.01.05/ontology-dbpedia-mapping.nt.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
}
