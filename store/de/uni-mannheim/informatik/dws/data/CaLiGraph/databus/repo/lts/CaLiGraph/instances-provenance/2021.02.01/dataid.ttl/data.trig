<http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/instances-provenance/2021.02.01/dataid.ttl#Dataset> {
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/instances-provenance/2021.02.01/dataid.ttl#Dataset>
            a       <http://dataid.dbpedia.org/ns/core#Dataset> ;
            <http://www.w3.org/2000/01/rdf-schema#comment>
                    "Provenance information about instances (e.g. if the instance has been extracted from a Wikipedia list page)."@en ;
            <http://www.w3.org/2000/01/rdf-schema#label>
                    "CaLiGraph instance provenance data"@en ;
            <http://dataid.dbpedia.org/ns/core#account>
                    <https://databus.dbpedia.org/nico.heist> ;
            <http://dataid.dbpedia.org/ns/core#artifact>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/instances-provenance> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#group>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/core#groupdocu>
                    "CaLiGraph: A Large Semantic Knowledge Graph from Wikipedia Categories and List Pages\nFor more information about CaLiGraph, refer to http://caligraph.org.\n\n## Changelog\n# 2021.02.01\n  * BERT-based recognition of subject entities and improved language models from spaCy 3.0\n* 2021.01.05\n  * Fixed minor encoding errors and improved formatting\n* 2020.12.21\n  * CaLiGraph is now based on a recent version of Wikipedia and DBpedia from November 2020\n* 2020.09.20\n  * Improvements to the CaLiGraph class hierarchy\n  * Many small bugfixes\n* 2020.08.19\n  * Add alternative labels for CaLiGraph instances\n* 2020.05.11\n  * Adapt URI encoding to DBpedia conventions\n* 2020.03.01\n  * Add dbpedia-caligraph-types and dbpedia-transitive-caligraph-types artifacts for direct typing of DBpedia resources with CaLiGraph types.\n  * The artifact ontology-dbpedia-mapping uses the 'subClassOf' predicate instead of the 'equivalentClass' predicate to link to DBpedia types.\n* 2020.01.22\n  * Initial release on DBpedia Databus" ;
            <http://dataid.dbpedia.org/ns/core#version>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/instances-provenance/2021.02.01> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#codeReference>
                    <https://github.com/nheist/CaLiGraph> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#documentationLocation>
                    <https://github.com/nheist/CaLiGraph/blob/master/README.md> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#feedbackChannel>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://dataid.dbpedia.org/ns/debug.ttl#issueTracker>
                    <https://github.com/nheist/CaLiGraph/issues> ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/description>
                    "" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.02.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-02-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://purl.org/dc/terms/title>
                    "CaLiGraph instance provenance data"@en ;
            <http://www.w3.org/ns/dcat#distribution>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/instances-provenance/2021.02.01/dataid.ttl#instances-provenance.nt.bz2> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/instances-provenance/2021.02.01>
            a       <http://dataid.dbpedia.org/ns/core#Version> .
    
    <https://nheist.github.io/webid.ttl#this>
            <http://xmlns.com/foaf/0.1/account>
                    <https://databus.dbpedia.org/nico.heist> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph>
            a       <http://dataid.dbpedia.org/ns/core#Group> .
    
    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/instances-provenance/2021.02.01/dataid.ttl#instances-provenance.nt.bz2>
            a       <http://dataid.dbpedia.org/ns/core#SingleFile> ;
            <http://dataid.dbpedia.org/ns/core#associatedAgent>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://dataid.dbpedia.org/ns/core#compression>
                    "bzip2" ;
            <http://dataid.dbpedia.org/ns/core#file>
                    <https://databus.dbpedia.org/nico.heist/CaLiGraph/instances-provenance/2021.02.01/instances-provenance.nt.bz2> ;
            <http://dataid.dbpedia.org/ns/core#formatExtension>
                    "nt" ;
            <http://dataid.dbpedia.org/ns/core#isDistributionOf>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/instances-provenance/2021.02.01/dataid.ttl#Dataset> ;
            <http://dataid.dbpedia.org/ns/core#preview>
                    "<http://caligraph.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:Military_units_and_formations_of_the_Croatian_War_of_Independence> .\n<http://caligraph.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:Military_units_and_formations_of_the_Bosnian_War> .\n<http://caligraph.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:1946_establishments_in_Yugoslavia> .\n<http://caligraph.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:Regiments_of_Yugoslav_Air_Force> .\n<http://caligraph.org/resource/105th_Fighter-Bomber_Aviation_Regiment> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:Military_units_and_formations_established_in_1946> .\n<http://caligraph.org/resource/International_Center_of_Parapsychology_and_Scientific_Research_of_the_New_Age> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:Religious_organizations_established_in_1989> .\n<http://caligraph.org/resource/International_Center_of_Parapsychology_and_Scientific_Research_of_the_New_Age> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:New_religious_movements> .\n<http://caligraph.org/resource/Port_Mayaca_Lock_and_Dam> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:Dams_completed_in_1977> .\n<http://caligraph.org/resource/Port_Mayaca_Lock_and_Dam> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:Dams_in_Florida> .\n<http://caligraph.org/resource/Port_Mayaca_Lock_and_Dam> <http://www.w3.org/ns/prov#wasDerivedFrom> <http://en.wikipedia.org/wiki/Category:Locks_of_Florida> ." ;
            <http://dataid.dbpedia.org/ns/core#sha256sum>
                    "1037d637211ac3559732f185f9161a9bb0d0cca649b18d3fb49a4d6ad38382a4" ;
            <http://dataid.dbpedia.org/ns/core#signature>
                    "DVz4IE5F1r0oISkx0Uvciv5ZjBqXmyHPlEtZfbMTHWdTgAT4r4F5WKjbXQGjANjJeCTvfRYlPsnxfTTGcXcwxIlzz1FPjUGYMKlPj4cFAdFdlCC65WCWKy5tcAFN9LUlj7BlpJfz0o0W5Um/izhcf2soxBGsx7c91U9b40M1AhiZ+hdzq98zi2NdwhXkZ0KbUTvEge6RvcyAdueiU5qJgLWGWJtGLwSbd6wUA0ujuK7gHnU9JymLAVBHZnDQSg8RRb2xw3fDybcWqd61n1N/dmWJdnM6CZu4EOtXIFtHN5yjRqr2NX00IRQ1gbtNbJ/ptiezzc/Ozv/GKsHwrjqTMg==" ;
            <http://purl.org/dc/terms/conformsTo>
                    "http://dataid.dbpedia.org/ns/core#" ;
            <http://purl.org/dc/terms/hasVersion>
                    "2021.02.01" ;
            <http://purl.org/dc/terms/issued>
                    "2021-02-01T00:00:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/license>
                    <http://purl.oclc.org/NET/rdflicense/cc-by3.0> ;
            <http://purl.org/dc/terms/modified>
                    "2021-02-08T08:57:45Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> ;
            <http://purl.org/dc/terms/publisher>
                    <https://nheist.github.io/webid.ttl#this> ;
            <http://www.w3.org/ns/dcat#byteSize>
                    "284831925"^^<http://www.w3.org/2001/XMLSchema#decimal> ;
            <http://www.w3.org/ns/dcat#downloadURL>
                    <http://data.dws.informatik.uni-mannheim.de/CaLiGraph/databus/repo/lts/CaLiGraph/instances-provenance/2021.02.01/instances-provenance.nt.bz2> ;
            <http://www.w3.org/ns/dcat#mediaType>
                    <http://dataid.dbpedia.org/ns/mt#ApplicationNTriples> .
    
    <https://databus.dbpedia.org/nico.heist/CaLiGraph/instances-provenance>
            a       <http://dataid.dbpedia.org/ns/core#Artifact> .
}
