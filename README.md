# Gitalog

A data catalog hosted in git


Created using [FsRdfStore](https://github.com/AKSW/FsRdfStore) and [databus data](https://databus.dbpedia.org/dbpedia/databus/databus-data)

Clone this repo and use FsRdfStore in order to get a SPARQL interface.
TODO Create a WebDAV accessible clone of this repo + a public Fuseki in order to allow for online querying.
